<?php
/**
 * Template Name: Landing E Page
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */

get_header('landing'); 
   	 global $post;
    	$post_slug=$post->post_name;
	$file_name = explode('-', $post_slug, 2);
	$service_type = $file_name[1];
	$template_fname= "template-parts/".$service_type;	
?>
<br><br>


		<section class="getstarted pb-2">
			<div class="landing-banner-text desktop_fade_slider">
				<div class="container">
					<div class="row landing-service">
						<div class="col-md-8 under_line my-2 pr-md-4" id="request_proposal">
							<?php $sub_head = get_field('sub_heading');
							echo $sub_head; ?>
							<?php get_template_part( 'template-parts/use-case-fade-scroller', 'none' );?>
						</div>
						<div class="col-md-4 it-case-study-large after_banner_img text-center px-0">
							<?php while ( have_posts() ) : the_post(); ?>
							<?php $after_banner_image =  get_field('after_banner_image'); ?>
							<?php endwhile; ?>
							<img src="<?php echo $after_banner_image; ?>">
						</div>
					</div>
				</div>
			</div>
			<div class="landing-after-banner">
				<div class="container scroller">
					<div class="row landing-row mt-lg-3 px-sm-4 px-2">
						<span style="display: none" id="h-form-subject"><?php echo get_field('form_email_subject');?></span>
						<div class="col-lg-12 flat-box get-started-box px-0 mt-lg-0 mt-3">
							<div class="form-column pb-0 pt-xl-2 pt-3 px-xl-4 px-md-3 px-2">
								<div class="get-started-form-box landing py-2">
									<h6 class="mb-3 py-3 under_line"><?php the_field('form_heading'); ?></h6>
									<?php echo do_shortcode( '[contact-form-7 id="4830" title="Landing B Top form"]'); ?>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<?php get_template_part( 'template-parts/landing-page-boxes-tabs', 'none' );?>
						</div>
					</div>
				</div>
			</div>
		</section>

	<?php get_template_part( 'template-parts/how-we-can-support-you', 'none' );?>
	
	<?php get_template_part( 'template-parts/e-commerce-platforms', 'none' );?>

	<section class="text-center explore my-1 pb-md-3 bg-colour">
		<?php get_template_part( 'template-parts/use_cases', 'none' );?>
	</section>

	<?php get_template_part( 'template-parts/how_it_works', 'none' );?>

	<?php get_template_part( 'template-parts/home_150_logos', 'none' );?>

	<section class="text-center explore my-1 pt-md-3 ">
		<?php get_template_part( 'template-parts/case_studies', 'none' );?>
	</section>
	
	<section class="mt-5 mb-3 pb-2"  id="our_advantage">
		<div class="container">
			<div class="row pt-md-3 px-sm-0 px-2 mx-auto">
				<div class="col-md-8 pb-md-3 pb-0 explore_text text-center mx-auto advantage-heading">
					<h3>Our Advantage</h3>
				</div>
			</div>
			<br>
			<?php get_template_part( 'template-parts/our_advantage', 'none' );?>
		</div>
	</section>

	<section class="mb-5">
		<?php get_template_part( 'template-parts/landing-assivo-insights');?>
	</section>

	<?php get_template_part( 'template-parts/faqs_landing', 'none');?>
	
	<?php get_template_part( 'template-parts/request_consultation', 'none' );  ?>
    
	<!-- Rediction on Conusltation page after top form submtting START -->
	<input type="hidden" id="consultation_page" value="<?php the_permalink(3955); ?>" />
	<input type="hidden" id="next_step_page" value="<?php the_permalink(116); ?>" />


	<script>
	document.addEventListener( 'wpcf7mailsent', function( event ) {
	var checkedValue = document.querySelector('.check-boxes .active input:checked').value;
	var consultation = document.getElementById("consultation_page").value;
	var next_step = document.getElementById("next_step_page").value;
// 	 alert(checkBox + consultation);

	if(checkedValue != "A short-term project (4-8 weeks)" ){
		location = consultation;
	}else {
		location = next_step;
	}

	}, false );
	// Rediction on Conusltation page after top form submtting End


jQuery(document).ready(function($){
	
//   if(! $('#h_casestudies').hasClass('active')){
// 	  $('#data-entry-processing_1').addClass("active");
//   }
	
	$("#wpcf7-f3947-o1 select[name=csize] option:first").text("Choose Company Size");
	$("#wpcf7-f4830-o1 select[name=csize] option:first").text("Choose Company Size");
<?php 
	if(isset($_GET['calendy-popup'])){?>
		$("a#consultation-popup").click();
	<?php }
	?>
	
});	

</script>



<?php	get_footer();