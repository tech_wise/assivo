$(document).ready(function() {
	// *********** Left right arrows for Landing pages six boxes/tabs START ***********
	function next_pre(next_pre_selector, auto_scroll_time){
		$(next_pre_selector).on('click', function (e) {
		e.preventDefault();
	
		//Define Variables
		var current_href = $(this).attr('href');
		var current_id = current_href.substring(5);
		var last_id = 6;
	
		// Previous Arrow
		if(current_id < 2){
			pre_id = last_id;
		
		}else{
			var pre_id = parseInt(current_id) - 1;
		}
	
		// Next Arrows
		if(current_id == last_id){
			var next_id = 1;
	
		}else{
			var next_id = parseInt(current_id) + 1;
		}
	
		// Remove hide boxes
		$('.landing-box-tabs .tab-pane').removeClass('active show');
	
		// Show current box
		$('div#box-'+current_id).addClass('active show');
		
		// Assign next previous IDs
		$('a.next-box').attr('href', '#box-'+next_id);
		$('a.pre-box').attr('href', '#box-'+pre_id);
	
		// Add acive class to current tab/link
		$('#landing-boxes li').removeClass('active_tab');
		$('#landing-boxes li:nth-child('+current_id+')').addClass('active_tab');
		
			// console.log('current_href = '+ current_href + ' - current_id = ' + current_id + ' - next_id = ' + next_id );
		})	
		
	
		// Auto Scroller 
		var initList = setInterval(function(){ $('.next-box').click();}, auto_scroll_time) ;
	
	
		$('.tab-content').mouseover(function(){
			clearInterval(initList);
		 });
	
		 $('.tab-content').on("mouseout",function(){
			initList = setInterval(function(){ $('.next-box').click();}, auto_scroll_time);
		});
	}
	
	// Call function
	next_pre('ul#landing-boxes li a.tab', 10000);


// *********** Left right arrows for Landing pages six boxes/tabs END ***********

	

		$("#wpcf7-f1218-o1 select[name=csize] option:first").text("Choose Company Size");
	// 	$("#wpcf7-f1218-o2 select[name=csize] option:first").text("Choose Company Size");
		$("#wpcf7-f1219-o1 select[name=csize] option:first").text("Choose Company Size");
		$("#wpcf7-f4406-o2 select[name=csize] option:first").text("Choose Company Size");
		$("#wpcf7-f4716-o1 select[name=lsize] option:first").text("Average Monthly Loan Volume");
		$("#wpcf7-f4799-o1 select[name=source] option:first").text("How did you hear about us");
		$("#wpcf7-f3824-o1 select[name=csize] option:first").text("Choose Company Size");
		$("#wpcf7-f4716-o1 select[name=csize] option:first").text("Choose Company Size");
		$(".page-template-get-started #wpcf7-f4830-o1 select[name=csize] option:first").text("Choose Company Size");
	
	
		
	// 	For menu close icon
		$('nav.navbar button.navbar-toggler.close-icon').click(function(){
			$('nav.navbar button.navbar-toggler.close-icon').removeClass('close-icon');
		});
		
	// 	Arrows JS
		
		var counted_h_dots = $('#h_usecase .container.active .h-dots .owl-dots').children().length;
		if(counted_h_dots){
	// 		alert($('#h_usecase .container.active .h-dots.active').attr('id'));
		}
		
		
		// UC CheckBox JS
		$("input[name='uc_checkbox[]']").change(function() {
			var current_url = 'http://' + window.location.hostname + window.location.pathname, uc_checked_array=[];
			$("input[name='uc_checkbox[]']").each(function (index, obj) {
				if($(this).is(":checked")) {
					if(obj.value != 'all_use_cases'){
						uc_checked_array.push(obj.value);
					}
				}
			});
			if(uc_checked_array.length > 0){
				window.location.href = current_url+'?uc_filter='+uc_checked_array+'#h_uc_multiselect';
	// 			alert(current_url+'?uc_filter='+uc_checked_array);
				
			}else{
				window.location.href = current_url;
			}
	// 		if(document.location.search.length) {
	// 			alert();
	// 		} else {
	// 			alert();
	// 		}
			
			
			
	//        alert(uc_checked_array);
	//         $('#textbox1').val($(this).is(':checked'));     
		});
		
		
		$("button.h-dot").click(function(){
			$(this).parent().find('.h-dot.active').removeClass('active')
			$(this).parent().find('.h-dot').removeClass('new-uc')
	// 		$('.owl-dots .h-dot.active').removeClass('active');
			$(this).addClass('active');
			$(this).addClass('new-uc');
			$(this).parent().parent().parent().find('.h_explore_tabs.d-block').removeClass('d-block').addClass('d-none');
			$(this).parent().parent().parent().find('.h_explore_tabs').removeClass('n-new-uc');
			$('.h_explore_tabs.'+$(this).attr('id')).removeClass('d-none').addClass('d-block');
			$('.h_explore_tabs.'+$(this).attr('id')).addClass('n-new-uc');
	// 		alert($(this).attr('id'));
		});
		
		
		
	// 	Nayyer
	
		
		$(".uc-arrows-all button.owl-prev").click(function(){
			if($('.h-dots-all .h-dot.active').prev().parent().parent().hasClass('h-dots-all')){
				$('.h-dots-all .h-dot.active').removeClass('active');
				$('.h-dots-all .h-dot.new-uc').prev().addClass('active');
				$('.h-dots-all .h-dot.new-uc').removeClass('new-uc');
				$('.h-dots-all .h-dot.active').addClass('new-uc');
	
				$('.h_explore_tabs.uc-all.d-block').removeClass('d-block').addClass('d-none');
				$('.h_explore_tabs.uc-all.n-new-uc').prev().removeClass('d-none').addClass('d-block');
				$('.h_explore_tabs.uc-all.n-new-uc').removeClass('n-new-uc');
				$('.h_explore_tabs.uc-all.d-block').addClass('n-new-uc');
			}
	// 		else{
	// 			$('.h-dots-all .h-dot.active').removeClass('active');
	// 			$('.h-dots-all #h_tab-8.h-dot').addClass('active');
	// 			$('.h-dots-all .h-dot.new-uc').removeClass('new-uc');
	// 			$('.h-dots-all .h-dot.active').addClass('new-uc');
				
	// 			$('.h_explore_tabs.uc-all.d-block').removeClass('d-block').addClass('d-none');
	// 			$('.h_explore_tabs.uc-all.h_tab-8').removeClass('d-none').addClass('d-block');
	// 			$('.h_explore_tabs.uc-all.n-new-uc').removeClass('n-new-uc');
	// 			$('.h_explore_tabs.uc-all.d-block').addClass('n-new-uc');
	// 		}
		});
		
		$(".uc-arrows-all button.owl-next").click(function(){
			if($('.h-dots-all .h-dot.active').next().parent().parent().hasClass('h-dots-all')){
				$('.h-dots-all .h-dot.active').removeClass('active');
				$('.h-dots-all .h-dot.new-uc').next().addClass('active');
				$('.h-dots-all .h-dot.new-uc').removeClass('new-uc');
				$('.h-dots-all .h-dot.active').addClass('new-uc');
				
				
				$('.h_explore_tabs.uc-all.d-block').removeClass('d-block').addClass('d-none');
				$('.h_explore_tabs.uc-all.n-new-uc').next().removeClass('d-none').addClass('d-block');
				$('.h_explore_tabs.uc-all.n-new-uc').removeClass('n-new-uc');
				$('.h_explore_tabs.uc-all.d-block').addClass('n-new-uc');
			}
	// 		else{
	// 			$('.h-dots-all .h-dot.active').removeClass('active');
	// 			$('.h-dots-all #h_tab-1.h-dot').addClass('active');
	// 			$('.h-dots-all .h-dot.new-uc').removeClass('new-uc');
	// 			$('.h-dots-all .h-dot.active').addClass('new-uc');
				
	// 			$('.h_explore_tabs.uc-all.d-block').removeClass('d-block').addClass('d-none');
	// 			$('.h_explore_tabs.uc-all.h_tab-1').removeClass('d-none').addClass('d-block');
	// 			$('.h_explore_tabs.uc-all.n-new-uc').removeClass('n-new-uc');
	// 			$('.h_explore_tabs.uc-all.d-block').addClass('n-new-uc');
	// 		}
		});
		
		$(".uc-arrows-select button.owl-prev").click(function(){
			if($('.h-dots-select .h-dot.active').prev().parent().parent().hasClass('h-dots-select')){
				$('.h-dots-select .h-dot.active').removeClass('active');
				$('.h-dots-select .h-dot.new-uc').prev().addClass('active');
				$('.h-dots-select .h-dot.new-uc').removeClass('new-uc');
				$('.h-dots-select .h-dot.active').addClass('new-uc');
	
				$('.h_explore_tabs.uc-select.d-block').removeClass('d-block').addClass('d-none');
				$('.h_explore_tabs.uc-select.n-new-uc').prev().removeClass('d-none').addClass('d-block');
				$('.h_explore_tabs.uc-select.n-new-uc').removeClass('n-new-uc');
				$('.h_explore_tabs.uc-select.d-block').addClass('n-new-uc');
			}
		});
		
		$(".uc-arrows-inner button.owl-next").click(function(){
			if($('.h-dots-inner .h-dot.active').next().parent().parent().hasClass('h-dots-inner')){
				$('.h-dots-inner .h-dot.active').removeClass('active');
				$('.h-dots-inner .h-dot.new-uc').next().addClass('active');
				$('.h-dots-inner .h-dot.new-uc').removeClass('new-uc');
				$('.h-dots-inner .h-dot.active').addClass('new-uc');
	
				if($('.h_explore_tabs.uc-inner.d-block').prev().hasClass('.h_explore_tabs.uc-inner')){
					$('.h_explore_tabs.uc-inner.d-block').removeClass('d-block').addClass('d-none');
					$('.h_explore_tabs.uc-inner.n-new-uc').next().removeClass('d-none').addClass('d-block');
					$('.h_explore_tabs.uc-inner.n-new-uc').removeClass('n-new-uc');
					$('.h_explore_tabs.uc-inner.d-block').addClass('n-new-uc');
				}
			}
		});
		
	// 	Select tab on landing page
		if(! $('#all').hasClass('active')){
			var selected_uc_category = $('#selected-uc-category').text();
			$("#h_usecase>div.active").removeClass("active");
			$('#'+selected_uc_category).addClass("active");
		}
		if(! $('#all_1').hasClass('active')){
			var selected_cs_category = $('#selected-cs-category').text();
			$("#h_casestudies>div.active").removeClass("active");
			$('#'+selected_cs_category+'_1').addClass("active");
		}
		
		$("#form-page-subject").val($("#h-form-subject").text());
		
		$('.h_faq_check').on('click',function(){
			$('.h_faq_hide').hide();
			$('#'+$(this).attr("h_faq_id")).show();
		}); 
		
		
		$('.h_uc_link').on('click',function(){
			$("#uc_filtered").addClass("tab-pane");
			$("input[name='uc_checkbox[]']").each(function (index, obj) {
				if($(this).is(":checked")) {
					$(this).removeAttr('checked');
				}
			});
			$("#h_usecase>div.active").removeClass("active");
			$('#'+$(this).attr("h_id")).addClass("active");
		}); 
		
		$('.h_cs_link').on('click',function(){
			$('#'+$(this).attr("h_id")).addClass("active");
			$("#h_casestudies>div.active").removeClass("active");
			$('#'+$(this).attr("h_id")).addClass("active");
		});
		
	//     //start here
	//    	  var height_cal = 0 ;
	// 	  var get_height = 0 ;
	// 	  var remove_height = 0;
	// 	$(".search-menu img").click(function() {
	// 	   $(".search-box").toggle();
	// 	   $(".search-box input[type='text']").focus();
	// 	 });
		
		
	// 	//Auto Main Banner Image Height
	// 	function main_banner_height(selector){
	// 		var background = $(selector).css('background-image');
	// 		//var
	// 		// If the background image is anything other than "none"
	// 			// Find and replace "url()" to get the pure image URL
	// 			/* background = background.replace('url("', '').replace('")', ''); */
	// 				background1 = background.replace('url(', '').replace(')', '');
	// 				if (background1.indexOf('"') != -1) {
	// 				background1 = background.replace('url("', '').replace('")', '');
	
	// 				}
	// 			// Create new Image instance and set path to our background
	// 			var bg = new Image();
	// 			bg.src = background1;
	// 			// We now have serveral vars availible to pass through to the plugin
	// 			// self = the element
	// 			// background = the url
	// 			// bg.width = image width
	// 			// bg.height = image height
	
	
	// 			var orginal_img_width  = bg.width;
	// 			var orginal_img_height = bg.height;
	// 			var req_height_percentage = (orginal_img_height / orginal_img_width ) * 100;
	
	// 			var wind_widht = $( window ).width();
	// 			var calculate_height =( (wind_widht / 100) * req_height_percentage +1 ) ;
	// 			//console.log('background:' + background);
				
	// 			//console.log("orginal_img_width:"+bg.width+",orginal_img_height:"+bg.height,"req_height_percentage:"+req_height_percentage,"wind_widht:"+wind_widht,"calculate_height:"+calculate_height);
	
	// 			$(selector).height(calculate_height);
	// // 		$(selector).css('height',calculate_height+' !important');
		
	// 	return calculate_height;
	// 	}
		
	// 	//Banner Height On Load
	
	// 	$( window ).on( "load", function() {
	// 			console.log( "window loaded" );
			
	// 			if($('.assive-home-page').length){
	// 			    main_banner_height('.assive-home-page');
	// 			}
	// 			if($('.hybrid-model').length){
	// 			    main_banner_height('.hybrid-model');
	// 			}
			
		
	// 	});
				
	// 	//Banner Height On Ressise window
	//         $( window ).resize(function() {
	// 		   //Banner Height On Load
				
	// 			if($('.assive-home-page').length){
	// 			   main_banner_height('.assive-home-page');
	// 			}
	// 			if($('.hybrid-model').length){
	// 			   main_banner_height('.hybrid-model');
	// 			}
	// 		});
	
	/**********Scroll to div Function *****************************
	
	  $(".scrollTo").on('click', function(e) {
		 e.preventDefault();
		 var target = $(this).attr('href');
		 $('html, body').animate({
		   scrollTop: ($(target).offset().top)
		 }, 2000);
	  });
		height_cal =  main_banner_height('.assive-home-page');
		get_height = height_cal - 70;
		remove_height = (get_height) - 0.25;
		*/
	/*
	   $(window).on("scroll", function() {
		
			if (!$("body").hasClass("page-template-get-started")){
				if (!$("body").hasClass("page-template-landing")){
					if($(window).scrollTop() > 0.5) {
						$("header").addClass("nav_g");
					}
					else{
						$("header").removeClass("nav_g");
					}
					if($(window).scrollTop() > 1) {						
						$("header").addClass("nav-bg");
						$("header").addClass("desk_responsive_head");
						$("header.desk_responsive_head").attr("style","background:linear-gradient(to right, #4775e5,#8e53e9) !important;");
						$(".assivo-banner-text.desktop_view").css("display","none");
						if ($(window).width() > 865){
					  //      $(".use-case").attr("style","margin-top:15% !important;");
						}
						else{
					  //  	$("section.mobile_view:nth-child(3)").attr("style","margin-top:15% !important;");
						}
						
					} 
					else {
					 $("header.desk_responsive_head").attr("style","background:unet!important;");
						   $("header").removeClass("desk_responsive_head");
						   $("header").addClass("back_responsive");
						   $("header").removeClass("nav_g");
						   $(".nav-position").removeClass("nav-bg");
							
						   $("header").removeClass("nav-bg");
						   main_banner_height('header');
						   if ($(window).width() > 865){
								   $(".assivo-banner-text.desktop_view").css("display","block"); 
								$(".assivo-banner-text.desktop_view").slideDown("slow");
							   //	$(".use-case").attr("style","margin-top:5% !important;");
						   }
						else{
								  //	$("section.mobile_view:nth-child(3)").attr("style","margin-top:5% !important;");
							   }
					}
				}
			   }
		});  */
	});
	//-----------------------------search forms-------------------------
	
	$(document).ready(function(){
	  $('.wpcf7-form-control-wrap .email').hide();
		$('.placeholder-abst').click(function(){
	//       alert('hello');
			$(this).parent().parent().find('.wpcf7-email').focus();
			var myclass='';
			if($(window).width() <= 865){
			  myclass='.mobile_view';
			}
			else{
			   myclass='.desktop_view';
			}
			$(this).parent().parent().find(myclass).hide();
			
		});
		$('.wpcf7-email').focusout(function(){
			
			if( $(this).val() == ''){
			 //alert($(this).val());
			 var myclass='';
			if($(window).width() <= 865){
			  myclass='.mobile_view';
			}
			else{
			   myclass='.desktop_view';
			}
			  $(this).parent().parent().find(myclass).show(); 
		  }
		});
	});
	$(document).ready(function(){
		$('.flip-box-front h5').on('click',function(){
	//        alert('hello');
		   $(this).closest('.flip-box-front').trigger('click');
		});
		
		//case-study-collapse
		$('.case-study-link').on('click',function(){
			var c = $(this).attr('data-target');
				if ( !$( c ).hasClass( "show" ) ) {
					$('body').find(c).siblings('.card-header').find('a').trigger('click')
					$('body').find(c).siblings('.card-header').find('a').addClass('i-minus');
					$('body').find(c).siblings('.card-header').find('a').removeClass('i-plus');
				}
			setTimeout(function(){
				let h_t = $(document).scrollTop();
				$(document).scrollTop(h_t - 80);
			}, 2000);
		});
		
	});
	
	
	//----------------footer js code------------------//
	
	$(document).ready(function(){
	
		$(".toggle-i-circle").click(function () {
			$(".toggle-i-circle").each(function(){
				if($(this).attr('aria-expanded')=='true'){
					$(this).removeClass("i-minus").addClass("i-plus");	
				}
			});
			if ($(this).hasClass("collapsed")) {
				$(this).toggleClass("i-minus i-plus");
			}
		});
	
		$(".toggle-circle").click(function () {
			if(!$(this).hasClass('h-js-faq')){
				$(".toggle-circle").each(function(){
					if($(this).attr('aria-expanded')=='true'){
						if(!$(this).hasClass('h-js-faq')){
							$(this).removeClass("minus").addClass("plus");
						}
					}
				});
				if ($(this).hasClass("collapsed")) {
					$(this).toggleClass("minus plus");
				}
			}
		});
	
		$("body").click(function (e) {
			if ($(e.target).is('.link p')){
				$(e.target).parent('.link').trigger('click');
			}
			
			if($(e.target).is('.link')){
				$(".toggle-i-circle").each(function(){
					if($(this).attr('aria-expanded')=='true'){
						$(this).removeClass("i-minus");
						$(this).addClass("i-plus");
					}
	
				});
				$(e.target).parent().find('a').addClass("i-plus").removeClass("i-minus");
				if ($(e.target).parent().find('a').hasClass("collapsed")) {
					$(e.target).parent().find('a').toggleClass("i-minus i-plus");
				}
				// 					$('body').find('.toggle-i-circle').each(function(){
				// 						tr=$(this).attr('data-target');
				// 						if($(tr).hasClass('show')){
				// 						     $(this).removeClass('i-plus');
				// 							 $(this).addClass('i-minus');
				// 						   }
				// 						else{
				// 							 $(this).removeClass('i-plus');
				// 							 $(this).addClass('i-minus');
				// 						}
				// 					});
			}
			if($(e.target).is('.faq-head')){
				if(!$(e.target).parent().find('a').hasClass("h-js-faq")){
					$(".toggle-circle").each(function(){
						if($(this).attr('aria-expanded')=='true'){
							if(!$(this).hasClass('h-js-faq')){
								$(this).removeClass("minus").addClass("plus");
							}
						}
					});
					$(e.target).parent().find('a').addClass("plus").removeClass("minus");
					if ($(e.target).parent().find('a').hasClass("collapsed")) {
						$(e.target).parent().find('a').toggleClass("minus plus");
					}
				}
				
				
				if($(e.target).parent().find('a').hasClass("h-js-faq") && $(e.target).parent().find('a').hasClass("plus")){
	// 				$(".h-js-faq").each(function(){
	// 					$($(this).attr('data-target')).removeClass('show');
	// 					$(this).removeClass("minus").addClass("plus");
	// 					$(this).parent().find('button').removeClass("h-color");
	// 				});
					$(e.target).parent().find('a').removeClass("plus").addClass("minus");
					$(e.target).parent().find('button').addClass("h-color");
					
	// 				if($($(e.target).attr('data-target')).find('div').hasClass('show')){
	// 					$($(e.target).attr('data-target')+' .collapse.show').parent().find('a').removeClass('plus').addClass('minus');
	// 				}
					
				}else if($(e.target).parent().find('a').hasClass("h-js-faq") && $(e.target).parent().find('a').hasClass("minus")){
					$(e.target).parent().find('button').removeClass("h-color");
					$(e.target).parent().find('a').removeClass("minus").addClass("plus");
				}
			}
			
			if($(e.target).is('.h-js-faq')){
				if($(e.target).parent().find('a').hasClass("h-js-faq") && $(e.target).parent().find('a').hasClass("plus")){
					
	// 				$(".h-js-faq").each(function(){
	// 					$($(this).attr('data-target')).removeClass('show');
	// 					$(this).removeClass("minus").addClass("plus");
	// 					$(this).parent().find('button').removeClass("h-color");
	// 				});
					$(e.target).parent().find('a').removeClass("plus").addClass("minus");
					$(e.target).parent().find('button').addClass("h-color");
					
	// 				if($($(e.target).attr('data-target')).find('div').hasClass('show')){
	// 					$($(e.target).attr('data-target')+' .collapse.show').parent().find('a').removeClass('plus').addClass('minus');
	// 				}
					
				}else if($(e.target).parent().find('a').hasClass("h-js-faq") && $(e.target).parent().find('a').hasClass("minus")){
					$(e.target).parent().find('button').removeClass("h-color");
					$(e.target).parent().find('a').removeClass("minus").addClass("plus");
	// // 				alert('husnain');
				}
			}
			
		});
		
	
		// 			    $('.filter-check').prop('checked', true);
		$("#search-case").on("keyup", function () {
			$('.case-box').hide();
			$('.study_box').hide();
			$('.case-box').removeClass('searchresults');
			$('.study_box').removeClass('searchresults');
			var value = $(this).val().toLowerCase();
			if (value == "") {
				//                     $('.filter-check').trigger('click');
				$('.case-box').hide();
				$('.study_box').hide();
	
				if (pagename == 'usecase') {
					create_pages('case-box', '');
				}
				if (pagename == 'casestudy') {
					create_pages('study_box', '');
				}                     
				return false;
			}
				
				
			$(".case-box").filter(function () {
				//                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
				//                    $("#case-title").text('All Cases');
				if ($(this).text().toLowerCase().indexOf(value) > -1) {
					$(this).addClass('searchresults');
				}
			});
			$(".study_box").filter(function () {
				//                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
				//                    $("#case-title").text('All Studies');
				if ($(this).text().toLowerCase().indexOf(value) > -1) {
					$(this).addClass('searchresults');
				}
			});
	
	
			create_pages('searchresults', 'searched');
	
	
	
	
	
	
		});	
				
	
			function create_pages(newclass, newid) {
	
	
	
			var count = 0;
			var pagelist = '<ul class="pagination">';
			$('.' + newclass).each(function () {
				count++;
	
			});
			var pageselect = count / totalcontents;
			if (Number.isInteger(pageselect)==false) {
				// 					alert(Number.isInteger(pageselect));
				pageselect+=1;
			}
			// 				alert(pageselect);
	
			if(pageselect >=2 ){
				for (j = 1; j <= pageselect; j++) {
					acclass = '';
					if(pageselect==j){
	
					}
					if (j == 1) {
						acclass = 'active';
						
	
						// For Use Case Template Part
						if($( "div" ).hasClass( "uc-temp-part" )){
	
							pagelist += '<li class="page-item ' + acclass + '" ><a class="page-link" href="#" page="1" id="' + newid + '">Previous</a></li>';
	
						}else{
							pagelist += '<li class="page-item ' + acclass + '"><a class="page-link" href="#" page="1"  id="' + newid + '">First</a></li>';
						}
					}
					pagelist += '<li class="page-item ' + acclass + '" ><a class="page-link" href="#" page="' + j + '" id="' + newid + '">' + j + '</a></li>';
				} 
			}
			j -= 1;
			if (pagelist != '<ul class="pagination">') {
	
				// For Use Case Template Part
				if($( "div" ).hasClass( "uc-temp-part" )){
					pagelist += '<li class="page-item ' + acclass + '" ><a class="page-link" href="#" page="2" id="' + newid + '">Next</a></li>';
	
				}else{
					pagelist += '<li class="page-item " ><a class="page-link" href="#" page="' + j + '" id="' + newid + '">Last</a></li>';
				}
			}
			pagelist += '</ul>';
			$('.pagenation-list').html('');
			$('.pagenation-list').html(pagelist);
			$('.' + newclass).slice(0, totalcontents).show();
	
		}
	
	
	
	
	
	
		$(".case-filter").click(function (e) {
			e.preventDefault();
			$(this).siblings('.filter-check').trigger('click');
		});
	
		///////////Checkbox Filter code
		//            $(".filter-check").prop('checked', true);
		$(".filter-check").click(function () {
			$(".case-box").removeClass('selected-cat');
			$(".study_box").removeClass('selected-cat');
			$("#case-title").text("");
			//                                $(".all-filter-check").prop('checked',false);
			var selected = [];
			$('body').find('.filter-check').each(function () {
	
				if ($(this).prop("checked") == true) {
	
					var text = $(this).siblings('.case-filter').find('span').text();
					var type = $(this).siblings('.case-filter').data('type');
					var list = $(this).siblings('.case-filter').data(type);
					if (list == '') {
						list = type;
					}
					selected.push(list);
	
				}
	
			});
	
			$(".case-box").hide();
			$(".study_box").hide();
			$(".category").hide();
			if (selected.length === 0) {
				//                     $('.all-cat').trigger('click');
				//                     $('.all-verticals').trigger('click');
				$('.case-box').hide();
				$('.study_box').hide();
	
				if (pagename == 'usecase') {
					create_pages('case-box', '');
				}
				if (pagename == 'casestudy') {
					create_pages('study_box', '');
				}
	
	
			}
			if(selected.length != 0){
				$.each(selected, function (i, v) {
	
					$(".case-box").each(function () {
	
						if ($(this).hasClass(v)) {
	
							$(this).addClass('selected-cat');
						}
					});
					$(".category").each(function () {
						if ($(this).hasClass(v)) {
							$(this).addClass('selected-cat');
						}
					});
					$(".study_box").each(function () {
						if ($(this).hasClass(v)) {
							$(this).addClass('selected-cat');
						}
					});
				});
				create_pages('selected-cat', 'selected');
			}
	
	
		});
		
		
		// Use Case Template Part Next Previous Arrows Kamran
	function next_pre_uc(current_item){
		var last_dot = $(".uc-temp-part .page-item:nth-last-child(2) a").attr("page");
	
		// Previous Arrow
		if(current_item < 2){
			active_pre_page = 1;
		
		}else{
			active_pre_page =  current_item - 1;
		}
	
		// Next Arrows
		if(current_item == last_dot){
			active_next_page = last_dot;
		
		}else{
			active_next_page = parseInt(current_item) + 1;
		}
		
		$(".uc-temp-part .page-item:first-child a").attr("page", active_pre_page);
		$(".uc-temp-part .page-item:last-child a").attr("page", active_next_page);
	
			//Next Previous active dot record
			var active_record = parseInt(current_item) + 1;
			$(".uc-temp-part li.page-item:nth-child("+active_record+")").addClass("active");
	}	
	
	
		
		$('body').click(function (e) {
			// 				alert($(e.target).attr('class'));
			if ($(e.target).is('#selected')) {
				e.preventDefault();
				$('.case-box').hide();
				$('.study_box').hide();
				$('.page-item').removeClass('active');
				$(e.target).parent('.page-item').addClass('active');
				//                               alert($(this).find('.page-link').html());
				selpage = $(e.target).attr('page');
				select_pages(selpage);
				next = selpage * totalcontents;
				offset = next - totalcontents;
				$(".selected-cat").slice(offset, next).show();
			}
			if ($(e.target).is('#searched')) {
				e.preventDefault();
				$('.case-box').hide();
				$('.study_box').hide();
				$('.page-item').removeClass('active');
				$(e.target).parent('.page-item').addClass('active');
				//                               alert($(this).find('.page-link').html());
				selpage = $(e.target).attr('page');
				select_pages(selpage);
				next = selpage * totalcontents;
				offset = next - totalcontents;
				$(".searchresults").slice(offset, next).show();
			}
			if ($(e.target).is('.page-link')) {
				e.preventDefault();
				if($(e.target).attr('id')==''){
					$('.case-box').hide();
					$('.study_box').hide();
					$('.page-item').removeClass('active');
					$(e.target).parent('.page-item').addClass('active');
					//                               alert($(this).find('.page-link').html());
					selpage = $(e.target).attr('page');
					select_pages(selpage);
					next = selpage * totalcontents;
					offset = next - totalcontents;
					$(".case-box").slice(offset, next).show();
					$(".study_box").slice(offset, next).show();
				}
	
					// Next Previous Arrow Home Page Use Cases Kamran
					 next_pre_uc(selpage);
					//  console.log(next_item);
	
			}
	
	
		});
		$('.all-cat').click(function () {
			$('.filter-check').each(function () {
				if ($(this).attr('name') == 'category') {
					$(this).prop('checked', true);
				}
			});
			$('input[name="category"]').first().trigger('click');
			$('input[name="category"]').first().trigger('click');
			return false;
		});
		$('.all-verticals').click(function () {
			$('.filter-check').each(function () {
				if ($(this).attr('name') == 'verticles') {
					$(this).prop('checked', true);
				}
			});
			$('input[name="verticles"]').first().trigger('click');
			$('input[name="verticles"]').first().trigger('click');
			return false;
		});
	
	
		//----------------paggination-----------------
		$('.case-box').hide();
		$('.study_box').hide();
	
		if (pagename == 'usecase') {
			create_pages('case-box', '');
		}
		if (pagename == 'casestudy') {
			create_pages('study_box', '');
		}
	
	
	
	
		$(".page-item").on('click', function (e) {
			e.preventDefault();
			$('.case-box').hide();
			$('.study_box').hide();
			$('.page-item').removeClass('active');
			$(this).addClass('active');
	
			//                               alert($(this).find('.page-link').html());
			selpage = $(this).find('.page-link').attr('page');
	
			select_pages(selpage);
			next = selpage * totalcontents;
			offset = next - totalcontents;
			$('.case-box').slice(offset, next).show();
			$('.study_box').slice(offset, next).show();
	
	
		});
	
			function select_pages(selpage){
			lastval=$('.page-item').last().find('.page-link').attr('page');
			if(selpage==1){
				$('.page-item').slice(0,2).find('.page-link').css('background-color','#007bff');
				$('.page-item').slice(0,2).find('.page-link').css('color','#fff');
				$('.page-item').slice(-2).find('.page-link').css('background-color','#fff');
				$('.page-item').slice(-2).find('.page-link').css('color','#007bff');    
				
				// For Use Case Template Dot Colors
				$('.uc-temp-part .page-item').slice(0,2).find('.page-link').css('background-color','#D6D6D6');
				$('.uc-temp-part .page-item').slice(-2).find('.page-link').css('background-color','#D6D6D6');
	
			}
			else if(selpage==lastval){
				$('.page-item').slice(-2).find('.page-link').css('background-color','#007bff');
				$('.page-item').slice(-2).find('.page-link').css('color','#fff');  
				$('.page-item').slice(0,2).find('.page-link').css('background-color','#fff');
				$('.page-item').slice(0,2).find('.page-link').css('color','#007bff');
	
				// For Use Case Template Dot Colors
				$('.uc-temp-part .page-item').slice(0,2).find('.page-link').css('background-color','#D6D6D6');
				$('.uc-temp-part .page-item').slice(-2).find('.page-link').css('background-color','#D6D6D6');
	
			}
			else{
				$('.page-item').slice(0,2).find('.page-link').css('background-color','#fff');
				$('.page-item').slice(0,2).find('.page-link').css('color','#007bff');
				$('.page-item').slice(-2).find('.page-link').css('background-color','#fff');
				$('.page-item').slice(-2).find('.page-link').css('color','#007bff'); 
	
				// For Use Case Template Dot Colors
				$('.uc-temp-part .page-item').slice(0,2).find('.page-link').css('background-color','#D6D6D6');
				$('.uc-temp-part .page-item').slice(-2).find('.page-link').css('background-color','#D6D6D6');
	
			}
		}
	});
	
	
	
	$(document).ready(function(){
		var ur= window.location.href;
		if(ur.split('#')[1]!=undefined && ur.split('#')[1]!= '' ){
			csid=ur.split('#')[1];
			var btid=csid.split('-')[2];
			$('#case-study-btn-'+btid).trigger('click',function(){
				$('html, body').animate({
					scrollTop: $("#case-study-"+btid).offset().top
				});
			});
		}
		$('.data-entry-tabs .nav-link').on('click',function(){
			$('html, body').animate({
				scrollTop: $('.data-entry-tabs').offset().top-100
			});
		}); 
	});
	
	
	//Show one Category's use cases at a time (Nayyer)
		$('.filter-check').change(function(){
			if($(this).is(':checked')){
				$('input.filter-check').prop('checked', false);
			}
		});
	
	
	// For Active Category
		$(".uc-cat").click(function(){
			$(this).addClass("active");
			$('.uc-cat').not(this).removeClass('active');
		  });
		
	
		$('.cat-name').click(function(){
			$('#case-header h3').html($(this).text());
		});
	
	
	// Nayyer
	// Use Cases Template part menu
	$('.cat-cs-menu  a.h_cs_link').click(function(){
			var active_icon = $(this).find('img').attr('src');
			$('.cat-cs-menu .c-name img.icon-white').attr('src', active_icon);
			$('.cat-cs-menu .c-name h5').html($(this).text());
			$('.cat-cs-menu .c-menu nav.navbar .button').addClass('collapsed');
			$('.cat-cs-menu .c-menu nav.navbar .navbar-collapse').removeClass('show');
		});
	
	// Case Studies Template part menu
		$('.cat-uc-menu a.h_uc_link').click(function(){
			var active_icon = $(this).find('img').attr('src');
			$('.cat-uc-menu .c-name img.icon-white').attr('src', active_icon);
			$('.cat-uc-menu .c-name h5').html($(this).text());
			$('.cat-uc-menu .c-menu nav.navbar .button').addClass('collapsed');
			$('.cat-uc-menu .c-menu nav.navbar .navbar-collapse').removeClass('show');
		});
		
	// Thank you Page Check boxes
		$('.one-project').click(function(){
			$('.one-project input').prop('checked', true);
			$('.multi-project input').prop('checked', false);
			$('.multi-project-checkbox input').prop('checked', false);
			$('.calendly-inline-widget').hide();
			$('.req-summary .multi-project').removeClass('active');
			$('.req-summary .one-project').addClass('active');
			$('.req-summary .one-project-data').removeClass('d-none').addClass('d-block');
			$('.req-summary .multi-project-checkbox').removeClass('d-block').addClass('d-none');
			$('.req-summary .one-project-data .wpcf7cf-hidden').removeClass('wpcf7cf-hidden').show();
			$('.multi-project-data textarea').val('');
			$('.req-summary .multi-project-data div').hide();
		});
	
		
		$('.multi-project').click(function(){
			$('.multi-project input').prop('checked', true);
			$('.one-project input').prop('checked', false);
			$('.one-project-data input').prop('checked', false);
			$('.req-summary .one-project').removeClass('active');
			$('.req-summary .multi-project').addClass('active');
			$('.req-summary .multi-project-checkbox').removeClass('d-none').addClass('d-block');
			$('.req-summary .one-project-data').removeClass('d-block').addClass('d-none');
			$('.one-project-data textarea').val('');
			$('.req-summary .multi-project-data div').hide();
		});
	
	
		$('.one-project input').change(function(){
			if($(this).is(':checked')){
				$('.multi-project input').prop('checked', false);
				$('.multi-project-data textarea').val('');
				$('.calendly-inline-widget').hide();
				$('.req-summary .multi-project').removeClass('active');
				$('.req-summary .one-project').addClass('active');
				$('.req-summary .one-project-data').removeClass('d-none').addClass('d-block');
				 $('.req-summary .multi-project-checkbox').removeClass('d-block').addClass('d-none');
				$('.req-summary .multi-project-data').removeClass('d-block').addClass('d-none');
				$('.multi-project-checkbox .box input').prop('checked', false);
				$('.req-summary .multi-project-data div').hide();
			}
		});
		
		$('.multi-project input').change(function(){
			if($(this).is(':checked')){
				$('.one-project input').prop('checked', false);
				$('.one-project-data textarea').val('');
	// 			$('.calendly-inline-widget').hide();
				$('.req-summary .one-project').removeClass('active');
				$('.req-summary .multi-project').addClass('active');
				$('.req-summary .multi-project-checkbox').removeClass('d-none').addClass('d-block');
				 $('.req-summary .one-project-data').removeClass('d-block').addClass('d-none');
				$('.one-project-data .box input').prop('checked', false);
				$('.req-summary .multi-project-data div').hide();
			}
		});
	
	
		$('.multi-project-checkbox .multi-box-1').click(function(){
			$('.multi-project-checkbox .multi-box-1 input').prop('checked', true);
			$('.multi-project-checkbox .multi-box-2 input').prop('checked', false);
			$('.multi-project-checkbox .multi-box-3 input').prop('checked', false);
			$('.calendly-inline-widget').hide();
			$('.req-summary .multi-project-data').removeClass('d-none').addClass('d-block');
			$('.req-summary .multi-project-data .wpcf7cf-hidden').removeClass('wpcf7cf-hidden').show();
			$('.req-summary .multi-project-data div').show();
		});
		
		$('.multi-project-checkbox .multi-box-2').click(function(){
			$('.multi-project-checkbox .multi-box-2 input').prop('checked', true);
			$('.multi-project-checkbox .multi-box-1 input').prop('checked', false);
			$('.multi-project-checkbox .multi-box-3 input').prop('checked', false);
			$('.calendly-inline-widget').hide();
			$('.req-summary .multi-project-data').removeClass('d-none').addClass('d-block');
			$('.req-summary .multi-project-data .wpcf7cf-hidden').removeClass('wpcf7cf-hidden').show();
			$('.req-summary .multi-project-data div').show();
		});
	
		$('.multi-project-checkbox .multi-box-3').click(function(){
			$('.multi-project-checkbox .multi-box-3 input').prop('checked', true);
			$('.multi-project-checkbox .multi-box-2 input').prop('checked', false);
			$('.multi-project-checkbox .multi-box-1 input').prop('checked', false);
			$('.calendly-inline-widget').show();
			$('.req-summary .multi-project-data.d-block').removeClass('d-block').addClass('d-none');
		});
	
		$('.multi-project-checkbox .multi-box-1 input').change(function(){
			if($(this).is(':checked')){
				$('.calendly-inline-widget').hide();
				$('.req-summary .multi-project-data').removeClass('d-none').addClass('d-block');
			}
		});
		
		$('.multi-project-checkbox .multi-box-2 input').change(function(){
			if($(this).is(':checked')){
				$('.calendly-inline-widget').hide();
				$('.req-summary .multi-project-data').removeClass('d-none').addClass('d-block');
			}
		});
	
		$('.multi-project-checkbox .multi-box-3 input').change(function(){
			if($(this).is(':checked')){			
				$('.calendly-inline-widget').show();
				 $('.req-summary .multi-project-data.d-block').removeClass('d-block').addClass('d-none');
			}
		});
		
		$('.one-project-data .box-1').click(function(){
			$('.one-project-data .box-1 input').prop('checked', true);
			$('.one-project-data .box-2 input').prop('checked', false);
		});
	
		$('.one-project-data .box-2').click(function(){
			$('.one-project-data .box-2 input').prop('checked', true);
			$('.one-project-data .box-1 input').prop('checked', false);
		});
	
		$('.one-project-data .box input').change(function(){
			if($(this).is(':checked')){
				$('.one-project-data .box input').not(this).prop('checked', false);
			}
		});
		
		$('.multi-project-data .box input').change(function(){
			if($(this).is(':checked')){
				$('.multi-project-data .box input').not(this).prop('checked', false);
			}
		});
	
	// Landing page Top Form Checkboxes
		
		$('.top-form .check-1 input').prop('checked', true);;
		$('.top-form .check-1 div').addClass('active');

		$('.top-form .check-boxes input').change(function(){
			if($(this).is(':checked')){
				$('.top-form .check-boxes input').not(this).prop('checked', false);
				$('.top-form .check-boxes div').removeClass('active');
				$(this).parentsUntil('.check-boxes').addClass('active');
			}
		});
		$('.top-form .check-boxes').click(function(){
			$('.top-form .check-boxes div').removeClass('active');
			$(this).children('div').addClass('active');
			$('.top-form .check-boxes input').prop('checked', false);
			$(this).find('input').prop('checked', true);
		});
	
	// Footer Form Checkboxes
	
		$('.request-bg-img .check-boxes input').change(function(){
			if($(this).is(':checked')){
				$('.request-bg-img .check-boxes input').not(this).prop('checked', false);
				$('.request-bg-img .check-boxes div').removeClass('active');
				$(this).parentsUntil('.check-boxes').addClass('active');
			}
		});
		$('.request-bg-img .check-boxes').click(function(){
			$('.request-bg-img .check-boxes div').removeClass('active');
			$(this).children('div').addClass('active');
			$('.request-bg-img .check-boxes input').prop('checked', false);
			$(this).find('input').prop('checked', true);
		});


// For Common Use Cases Tabs 
// 	$('#common-uc li').addClass('list-item');
	$('#common-uc li').click(function(){
		$('#common-uc li').removeClass('active');
		$(this).addClass('active');
	});


// For Common Use Cases Tabs Next Prev

	$('.common-use-cases #next-prev-btn img.next').click(function(){
		$('#common-uc li.active').next().addClass('active').prev().removeClass('active');
		$('#common-uc-content .tab-pane.show.active').next().addClass('show active').prev().removeClass('show active');
		if($('#common-uc li:last-child').hasClass('active')) {
			if(lastElementFount){
				$('#common-uc li').removeClass('active');
				$('#common-uc li:first-child').addClass('active');
				$('#common-uc-content .tab-pane.show.active').removeClass('show active');
				$('#common-uc-content .tab-pane:first-child').addClass('show active');
			}
			 lastElementFount = true;
		} else {
			lastElementFount = false;
		}
	});

	$('.common-use-cases #next-prev-btn img.prev').click(function(){
		$('#common-uc li.active').prev().addClass('active').next().removeClass('active');
		$('#common-uc-content .tab-pane.show.active').prev().addClass('show active').next().removeClass('show active');
		if($('#common-uc li:first-child').hasClass('active')) {
			if(lastElementFount){
				$('#common-uc li').removeClass('active');
				$('#common-uc li:last-child').addClass('active');
				$('#common-uc-content .tab-pane.show.active').removeClass('show active');
				$('#common-uc-content .tab-pane:last-child').addClass('show active');
			}
			 lastElementFount = true;
		} else {
			lastElementFount = false;
		}
	});


// For FAQs tabs
	$('#question-tabs li').click(function(){
		$('#question-tabs li').removeClass('active');
		$(this).addClass('active');
	});

	$('#faq-tabs li').click(function(){
		$('#faq-tabs li').removeClass('active');
		$(this).addClass('active');
		$('#question-tabs li').removeClass('active');
		$('#question-tabs li:first-child').addClass('active');
	});


// For Services Tabs Mortgage Outsourcing Page
	$('#services-tabs li').click(function(){
		$('#services-tabs li').removeClass('active');
		$(this).addClass('active');
	});
// For Mortgage Outsourcing Page Logo Link Remove
	$(".page-template-landing-mortgage-outsourcing header .header-logo").removeAttr("href");

// For About Customer Tabs 
	$('#about-customer-tabs li').click(function(){
		$('#about-customer-tabs li').removeClass('active');
		$(this).addClass('active');
	});

// For About Customer Tabs Next Prev

	$('.about-customer #next-prev-btn img.next').click(function(){
		$('#about-customer-tabs li.active').next().addClass('active').prev().removeClass('active');
		$('#about-customer-tabs-content .tab-pane.show.active').next().addClass('show active').prev().removeClass('show active');
		if($('#about-customer-tabs li:last-child').hasClass('active')) {
			if(lastElementFound){
				$('#about-customer-tabs li').removeClass('active');
				$('#about-customer-tabs li:first-child').addClass('active');
				$('#about-customer-tabs-content .tab-pane.show.active').removeClass('show active');
				$('#about-customer-tabs-content .tab-pane:first-child').addClass('show active');
			}
			 lastElementFound = true;
		} else {
			lastElementFound = false;
		}
	});

	$('.about-customer #next-prev-btn img.prev').click(function(){
		$('#about-customer-tabs li.active').prev().addClass('active').next().removeClass('active');
		$('#about-customer-tabs-content .tab-pane.show.active').prev().addClass('show active').next().removeClass('show active');
		if($('#about-customer-tabs li:first-child').hasClass('active')) {
			if(lastElementFound){
				$('#about-customer-tabs li').removeClass('active');
				$('#about-customer-tabs li:last-child').addClass('active');
				$('#about-customer-tabs-content .tab-pane.show.active').removeClass('show active');
				$('#about-customer-tabs-content .tab-pane:last-child').addClass('show active');
			}
			 lastElementFound = true;
		} else {
			lastElementFound = false;
		}
	});


// Landing D Page Multistep Form
	$("#wpcf7-f4406-o2 .first-step button").text("Continue");
	$("#wpcf7-f4406-o1 .first-step button").text("Continue");

	$('.form-section form .project').click(function(){
		$('.form-section form .project').removeClass('active');
		$(this).addClass('active');
	});
	

	$('.form-section form fieldset .project').click(function(){
 		var checkbox_val = $(this).find('input').val();
		if(checkbox_val == 'One time project'){
			$('.form-section form .pop-up-button').hide();
			$('.form-section form .thanks-text').show();
		}else{
			$('.form-section form .pop-up-button').show();
			$('.form-section form .thanks-text').hide();
			
		}
	});


// 	For Top Banner Use Cases Slider
		var slides = document.querySelectorAll('.desktop_fade_slider #slides .slide');
		var currentSlide = 0;
		var slideInterval = setInterval(nextSlide,5000);



		function nextSlide(){
			slides[currentSlide].className = 'slide';
			currentSlide = (currentSlide+1)%slides.length;
			slides[currentSlide].className = 'slide showing';
		}

			var slides1 = document.querySelectorAll('.mobile_view_new #slides .slide');
			var currentSlide1 = 0;
			var slideInterval1 = setInterval(nextSlide1,5000);


		function nextSlide1(){
			slides1[currentSlide1].className = 'slide';
			currentSlide1 = (currentSlide1+1)%slides1.length;
			slides1[currentSlide1].className = 'slide showing';
		}

	

		$('#slides li:first-child').addClass('showing');


//Slide Home Page

// Landing Mortgage Outsourcing Page
	$('.page-id-4426 .landing-service .under_line').removeAttr('id');
/*
	$('#wpcf7-f4716-o1 input.wpcf7-submit').click(function(){
		if($('#wpcf7-f4716-o1 select#lsize option:selected').val() === '1-50 loans / month'){
		   event.preventDefault();
// 		   alert(1);
		}
	});
*/

	
$(document).ready(function(){

	// Atuo slect Use Cases  category on the landing pages only
	var active_category = $('#h_uc_multiselect').attr('uccat');
	if(active_category != ''){
		// $('.case-filter[data-uc_and_cs_categories = "data-cleansing-data-enrichment"]').hide();
		// Auto click on current Use Case Category		
		$('.case-filter[data-uc_and_cs_categories = "'+active_category+'"]').click();

		//Slect category left side
		$('.case-filter[data-uc_and_cs_categories = "'+active_category+'"]').addClass('active');

	}

	});
