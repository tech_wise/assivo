<?php
/**
 * Template Name: Landing Financial Services Page
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */

get_header('landing'); 
   	 global $post;
    	$post_slug=$post->post_name;
	$file_name = explode('-', $post_slug, 2);
	$service_type = $file_name[1];
	$template_fname= "template-parts/".$service_type;	
?>
<br><br>

	<section class="getstarted pb-2">
		<div class="landing-banner-text desktop_fade_slider">
			<div class="container">
				<div class="row landing-service">
					<div class="col-md-8 under_line my-2" id="request_proposal">
						<?php $sub_head = get_field('sub_heading');
						echo $sub_head; ?>
					</div>
					<div class="col-md-4 it-case-study-large after_banner_img text-center px-0">
						<?php while ( have_posts() ) : the_post(); ?>
						<?php $after_banner_image =  get_field('after_banner_image'); ?>
						<?php endwhile; ?>
						<img src="<?php echo $after_banner_image; ?>">
					</div>
				</div>
			</div>
		</div>
		<div class="landing-after-banner">
			<div class="container scroller">
				<div class="row landing-row mt-lg-3 px-sm-4 px-2">
					<span style="display: none" id="h-form-subject"><?php echo get_field('form_email_subject');?></span>
					<div class="col-lg-12 flat-box get-started-box px-0 mt-lg-0 mt-3">
						<div class="form-column pb-0 pt-xl-2 pt-3 px-xl-4 px-md-3 px-2">
							<div class="get-started-form-box landing py-2">
								<h6 class="mb-3 py-3 under_line"><?php the_field('form_heading'); ?></h6>
								<?php echo do_shortcode( '[contact-form-7 id="3947" title="Landing B page Form"]'); ?>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<?php get_template_part( 'template-parts/landing-page-boxes-tabs', 'none' );?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="common-use-cases">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-12 heading text-center">
					<h1>Common Use Cases</h1>
					<ul class="nav nav-tabs text-center" id="common-uc">
						<li class="active">
							<a data-toggle="tab" href="#common-uc-1" class="tab">
								<img src="<?php the_field('cuc_tab_1_icon_white'); ?>" class="white">
								<img src="<?php the_field('cuc_tab_1_icon_blue'); ?>" class="blue">
								<p><?php the_field('cuc_tab_1_text'); ?></p>
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#common-uc-2" class="tab">
								<img src="<?php the_field('cuc_tab_2_icon_white'); ?>" class="white">
								<img src="<?php the_field('cuc_tab_2_icon_blue'); ?>" class="blue">
								<p><?php the_field('cuc_tab_2_text'); ?></p>
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#common-uc-3" class="tab">
								<img src="<?php the_field('cuc_tab_3_icon_white'); ?>" class="white">
								<img src="<?php the_field('cuc_tab_3_icon_blue'); ?>" class="blue">
								<p><?php the_field('cuc_tab_3_text'); ?></p>
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#common-uc-4" class="tab">
								<img src="<?php the_field('cuc_tab_4_icon_white'); ?>" class="white">
								<img src="<?php the_field('cuc_tab_4_icon_blue'); ?>" class="blue">
								<p><?php the_field('cuc_tab_4_text'); ?></p>
							</a>
						</li>
					</ul>
					
					<div class="tab-content" id="common-uc-content">
						<div id="common-uc-1" class="tab-pane fade show active">
							<div class="row justify-content-center">
								<div class="col-xl-9 col-lg-11 heading">
									<?php the_field('cuc_tab_1_main_heading'); ?>
								</div>
								<div class="col-xl-9 col-lg-11 sub-heading">
									<h5>Documents analyzed</h5>
								</div>
							</div>
							<div class="row justify-content-center">
								<div class="col-xl-3 col-md-4 documents">
									<?php the_field('tab_1_icons_list'); ?>
								</div>
								<div class="col-xl-6 col-md-8 document-list">
									<?php the_field('tab_1_list'); ?>
								</div>
								<div class="col-12 mx-auto text-center">
									<div class="get-footer advantage-btn">
										<a class="assivo-contact-us text-center text-white border-0 font-weight-bold" href="#request_proposal">Request A Proposal</a>
										<link href="https://assets.calendly.com/assets/external/widget.css" rel="stylesheet">
										<script src="https://assets.calendly.com/assets/external/widget.js" type="text/javascript"></script>
										<a class="assivo-contact-us text-center text-white border-0 font-weight-bold blue-btn" href="" onclick="Calendly.initPopupWidget({url: 'https://calendly.com/assivo'});return false;" id="consultation-popup">Schedule A Consultation</a>
										
									</div>
								</div>
							</div>
						</div>
						
						<div id="common-uc-2" class="tab-pane fade">
							<div class="row justify-content-center">
								<div class="col-xl-9 col-lg-11 heading">
									<?php the_field('cuc_tab_2_main_heading'); ?>
								</div>
								<div class="col-xl-9 col-lg-11 sub-heading">
									<h5>Documents analyzed</h5>
								</div>
							</div>
							<div class="row justify-content-center">
								<div class="col-xl-3 col-md-4 documents">
									<?php the_field('tab_2_icon_list'); ?>
								</div>
								<div class="col-xl-6 col-md-8 document-list">
									<?php the_field('tab_2_list'); ?>
								</div>
								<div class="col-12 mx-auto text-center">
									<div class="get-footer advantage-btn">
										<a class="assivo-contact-us text-center text-white border-0 font-weight-bold" href="#request_proposal">Request A Proposal</a>
										<link href="https://assets.calendly.com/assets/external/widget.css" rel="stylesheet">
										<script src="https://assets.calendly.com/assets/external/widget.js" type="text/javascript"></script>
										<a class="assivo-contact-us text-center text-white border-0 font-weight-bold blue-btn" href="" onclick="Calendly.initPopupWidget({url: 'https://calendly.com/assivo'});return false;" id="consultation-popup">Schedule A Consultation</a>
										
									</div>
								</div>
							</div>
						</div>
						
						<div id="common-uc-3" class="tab-pane fade">
							<div class="row justify-content-center">
								<div class="col-xl-9 col-lg-11 heading">
									<?php the_field('cuc_tab_3_main_heading'); ?>
								</div>
								<div class="col-xl-9 col-lg-11 sub-heading">
									<h5>Documents analyzed</h5>
								</div>
							</div>
							<div class="row justify-content-center">
								<div class="col-xl-3 col-md-4 documents">
									<?php the_field('tab_3_icon_list'); ?>
								</div>
								<div class="col-xl-6 col-md-8 document-list">
									<?php the_field('tab_3_list'); ?>
								</div>
								<div class="col-12 mx-auto text-center">
									<div class="get-footer advantage-btn">
										<a class="assivo-contact-us text-center text-white border-0 font-weight-bold" href="#request_proposal">Request A Proposal</a>
										<link href="https://assets.calendly.com/assets/external/widget.css" rel="stylesheet">
										<script src="https://assets.calendly.com/assets/external/widget.js" type="text/javascript"></script>
										<a class="assivo-contact-us text-center text-white border-0 font-weight-bold blue-btn" href="" onclick="Calendly.initPopupWidget({url: 'https://calendly.com/assivo'});return false;" id="consultation-popup">Schedule A Consultation</a>
										
									</div>
								</div>
							</div>
						</div>
						
						<div id="common-uc-4" class="tab-pane fade">
							<div class="row justify-content-center">
								<div class="col-xl-9 col-lg-11 heading">
									<?php the_field('cuc_tab_4_main_heading'); ?>
								</div>
								<div class="col-xl-9 col-lg-11 sub-heading">
									<h5>Documents analyzed</h5>
								</div>
							</div>
							<div class="row justify-content-center">
								<div class="col-xl-3 col-md-4 documents">
									<?php the_field('tab_4_icon_list'); ?>
								</div>
								<div class="col-xl-6 col-md-8 document-list">
									<?php the_field('tab_4_list'); ?>
								</div>
								<div class="col-12 mx-auto text-center">
									<div class="get-footer advantage-btn">
										<a class="assivo-contact-us text-center text-white border-0 font-weight-bold" href="#request_proposal">Request A Proposal</a>
										
										<link href="https://assets.calendly.com/assets/external/widget.css" rel="stylesheet">
										<script src="https://assets.calendly.com/assets/external/widget.js" type="text/javascript"></script>
										<a class="assivo-contact-us text-center text-white border-0 font-weight-bold blue-btn" href="" onclick="Calendly.initPopupWidget({url: 'https://calendly.com/assivo'});return false;" id="consultation-popup">Schedule A Consultation</a>
										
									</div>
								</div>
							</div>
						</div>
						
					</div>
					<div id="next-prev-btn">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/prev-icon.png" class="prev">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/next-icon.png" class="next">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="about-customer">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-12 text-center">
					<ul class="nav nav-tabs text-center" id="about-customer-tabs">
						<li class="active">
							<a data-toggle="tab" href="#about-tab-1" class="tab">
								<img src="<?php the_field('tab_1_icon_white'); ?>" class="white">
								<img src="<?php the_field('tab_1_icon_blue'); ?>" class="blue">
								<p><?php the_field('tab_1_text'); ?></p>
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#about-tab-2" class="tab">
								<img src="<?php the_field('tab_2_icon_white'); ?>" class="white">
								<img src="<?php the_field('tab_2_icon_blue'); ?>" class="blue">
								<p><?php the_field('tab_2_text'); ?></p>
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#about-tab-3" class="tab">
								<img src="<?php the_field('tab_3_icon_white'); ?>" class="white">
								<img src="<?php the_field('tab_3_icon_blue'); ?>" class="blue">
								<p><?php the_field('tab_3_text'); ?></p>
							</a>
						</li>
					</ul>
					
					<div class="tab-content" id="about-customer-tabs-content">
						<div id="about-tab-1" class="tab-pane fade show active">
							<div class="row justify-content-center">
								<div class="col-md-12 list text-left mb-4">
									<div class="row">
										<div class="col-md-5">
											<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.svg">
											<h2><?php the_field('tab_1_bottom_box_heading'); ?></h2>
										</div>
										<div class="col-md-7">
											<?php the_field('tab_1_bottom_box_list'); ?>
										</div>
									</div>
								</div>
								<div class="col-lg-11 heading">
									<h1><?php the_field('tab_1_main_heading'); ?></h1>
									<div class="row">
										<div class="col-md-8 sub-heading text-center">
											<?php the_field('tab_1_top_box_text'); ?>
										</div>
										<div class="col-md-4 text-center graphic">
											<img src="<?php the_field('tab_1_top_box_image'); ?>">
										</div>
									</div>
								</div>
								<div class="col-md-12 details">
									<?php the_field('tab_1_hierarchy_box_heading'); ?>
									<div class="row justify-content-center">
										<div class="col-xl-10 col-md-12 text-md-right text-center">
											<ul class="box-list">
												<li>
													<p><?php the_field('tab_1_hierarchy_1_text'); ?></p>
													<img src="<?php bloginfo('stylesheet_directory'); ?>/images/Blue-line.png" class="hr-line">
													<img src="<?php bloginfo('stylesheet_directory'); ?>/images/Blue-line-1.png" class="ver-line">
													<div class="box-1 box">
														<img src="<?php bloginfo('stylesheet_directory'); ?>/images/Customer-Acquisition.png">
														<p>Customer Acquisition</p>
													</div>
												</li>
												<li>
													<p><?php the_field('tab_1_hierarchy_2_text'); ?></p>
													<img src="<?php bloginfo('stylesheet_directory'); ?>/images/Darkblue-line.png" class="hr-line">
													<img src="<?php bloginfo('stylesheet_directory'); ?>/images/Darkblue-line-1.png" class="ver-line">
													<div class="box-2 box">
														<img src="<?php bloginfo('stylesheet_directory'); ?>/images/Underwriting.png">
														<p>Underwriting</p>
													</div>
												</li>
												<li>
													<p><?php the_field('tab_1_hierarchy_3_text'); ?></p>
													<img src="<?php bloginfo('stylesheet_directory'); ?>/images/purple-line.png" class="hr-line">
													<img src="<?php bloginfo('stylesheet_directory'); ?>/images/purple-line-1.png" class="ver-line">
													<div class="box-3 box">
														<img src="<?php bloginfo('stylesheet_directory'); ?>/images/Funding.png">
														<p>Funding</p>
													</div>
												</li>
												<li>
													<p><?php the_field('tab_1_hierarchy_4_text'); ?></p>
													<img src="<?php bloginfo('stylesheet_directory'); ?>/images/orange-line.png" class="hr-line">
													<img src="<?php bloginfo('stylesheet_directory'); ?>/images/orange-line-1.png" class="ver-line">
													<div class="box-4 box">
														<img src="<?php bloginfo('stylesheet_directory'); ?>/images/Monitoring.png">
														<p>Monitoring</p>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								
							</div>
						</div>
						
						<div id="about-tab-2" class="tab-pane fade">
							<div class="row justify-content-center">
								<div class="col-lg-11 heading">
									<h1><?php the_field('tab_2_main_heading'); ?></h1>
									<div class="row">
										<div class="col-md-8 sub-heading text-center">
											<?php the_field('tab_2_top_box_text'); ?>
										</div>
										<div class="col-md-4 text-center graphic">
											<img src="<?php the_field('tab_2_top_box_image'); ?>">
										</div>
									</div>
								</div>
								<div class="col-md-12 details">
									<?php the_field('tab_2_list_box_heading'); ?>
									<div class="row invoice-lists">
										<div class="col-md-6 lists">
											<div class="heading">
												<img src="<?php the_field('tab_2_list_1_icon'); ?>">
												<h3><?php the_field('tab_2_list_1_heading'); ?></h3>	
											</div>
											<?php the_field('tab_2_list_1'); ?>
										</div>
										<div class="col-md-6 lists">
											<div class="heading">
												<img src="<?php the_field('tab_2_list_2_icon'); ?>">
												<h3><?php the_field('tab_2_list_2_heading'); ?></h3>	
											</div>
											<?php the_field('tab_2_list_2'); ?>
										</div>
										<div class="col-md-6 lists">
											<div class="heading">
												<img src="<?php the_field('tab_2_list_3_icon'); ?>">
												<h3><?php the_field('tab_2_list_3_heading'); ?></h3>	
											</div>
											<?php the_field('tab_2_list_3'); ?>
										</div>
										<div class="col-md-6 lists">
											<div class="heading">
												<img src="<?php the_field('tab_2_list_4_icon'); ?>">
												<h3><?php the_field('tab_2_list_4_heading'); ?></h3>	
											</div>
											<?php the_field('tab_2_list_4'); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div id="about-tab-3" class="tab-pane fade">
							<div class="row justify-content-center">
								<div class="col-lg-11 heading">
									<h1><?php the_field('tab_3_main_heading'); ?></h1>
									<div class="row">
										<div class="col-md-8 sub-heading text-center">
											<?php the_field('tab_3_top_box_text'); ?>
										</div>
										<div class="col-md-4 text-center graphic">
											<img src="<?php the_field('tab_3_top_box_image'); ?>">
										</div>
									</div>
								</div>
								<div class="col-md-12 details">
									<?php the_field('tab_3_list_box_heading'); ?>
									<div class="row invoice-lists">
										<div class="col-md-6 lists">
											<div class="heading">
												<img src="<?php the_field('tab_3_list_1_icon'); ?>">
												<h3><?php the_field('tab_3_list_1_heading'); ?></h3>	
											</div>
											<?php the_field('tab_3_list_1'); ?>
										</div>
										<div class="col-md-6 lists">
											<div class="heading">
												<img src="<?php the_field('tab_3_list_2_icon'); ?>">
												<h3><?php the_field('tab_3_list_2_heading'); ?></h3>	
											</div>
											<?php the_field('tab_3_list_2'); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
					<div id="next-prev-btn">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/prev-icon.png" class="prev">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/next-icon.png" class="next">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="workflow">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-12 text-center heading">
					<h1><?php the_field('section_heading'); ?></h1>
				</div>
				<div class="col-md-10 workflow-box">
					<div class="row">
						<div class="col-md-4 text-center img">
							<img src="<?php the_field('top_box_image'); ?>">
						</div>
						<div class="col-md-8 text">
							<h1><?php the_field('top_box_heading'); ?></h1>
							<?php the_field('top_box_text'); ?>
						</div>
					</div>
				</div>
				<div class="col-md-12 text-center workflow-icons">
					<h1><?php the_field('icons_heading'); ?></h1>
					<?php the_field('icons_list'); ?>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-10 text-center bottom-box">
					<h1><?php the_field('bottom_left_box_heading'); ?></h1>
					<?php the_field('bottom_left_box_text'); ?>
				</div>
				<div class="col-md-10 text-center bottom-box">
					<h1><?php the_field('bottom_right_box_heading'); ?></h1>
					<?php the_field('bottom_right_box_text'); ?>
				</div>
			</div>
		</div>
	</section>


	<?php get_template_part( 'template-parts/how_it_works', 'none' );?>

	<section class="mt-5 mb-3 pb-2"  id="our_advantage">
		<div class="container">
			<div class="row pt-md-3 px-sm-0 px-2 mx-auto">
				<div class="col-md-8 pb-md-3 pb-0 explore_text text-center mx-auto advantage-heading">
					<h3>Our Advantage</h3>
				</div>
			</div>
			<br>
			<?php get_template_part( 'template-parts/our_advantage', 'none' );?>
		</div>
	</section>

	<?php get_template_part( 'template-parts/faqs_landing', 'none');?>

	<?php get_template_part( 'template-parts/testimonials', 'none' ); ?>

	<?php get_template_part( 'template-parts/request_consultation', 'none' ); ?>


	<script>

		jQuery(document).ready(function($){
			
		  $("#wpcf7-f3947-o1 select[name=csize] option:first").text("Choose Company Size");
		<?php 
			if(isset($_GET['calendy-popup'])){?>
				$("a#consultation-popup").click();
			<?php }
			?>
			
		});

	</script>



<?php	get_footer();
