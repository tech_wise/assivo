<?php
/*
Template Name: Home Page
*/
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */


get_header(); ?>
	
	<?php get_template_part( 'template-parts/how_we_add_value', 'none' );?>

	<section class="text-center explore my-1 pb-md-3 bg-colour">
		<?php get_template_part( 'template-parts/use_cases', 'none' );?>
	</section>

	<?php get_template_part( 'template-parts/how_it_works', 'none' );?>

	<?php get_template_part( 'template-parts/home_150_logos', 'none' );?>

	<section class="text-center explore my-1 pt-md-3 ">
		<?php get_template_part( 'template-parts/case_studies', 'none' );?>
	</section>

	<section class="mb-3 pb-2"  id="our_advantage"> <!-- mt-5 -->
	    	<div class="container">
	    		<div class="row px-sm-0 px-2 mx-auto"> <!-- pt-md-3 -->
	    			<div class="col-md-8 pb-0 explore_text text-center mx-auto advantage-heading"> <!-- pb-md-3 -->
	    		      <h3>Our Advantage</h3>
	    		    </div>
	    		</div>
				<br>
	    		<?php get_template_part( 'template-parts/our_advantage', 'none' );?>
	    	</div>
	</section>

	<section>
		<?php get_template_part( 'template-parts/blog-insights', 'none' );?>
	</section>

	<?php get_template_part( 'template-parts/home_testimonials', 'none' );?>
	
<?php if(isset($_GET['dev'])){
	 if($_GET['dev']=="1"){ ?> 
		
		
		<section class="faqs-section bg-colour pb-md-5 py-md-2">
			<?php get_template_part( 'template-parts/faqs', 'none' );?>
		</section>
		
	<?php } }  ?> 	
<!-- 	<section>
		<?php // get_template_part( 'template-parts/software_tools', 'none' );?>
	</section> -->

	<?php get_template_part( 'template-parts/request_consultation', 'none' );?>

<?php get_footer();
