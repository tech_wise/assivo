<?php
/**
 * Template Name: Landing Page
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */

get_header('landing'); 
   	 global $post;
    	$post_slug=$post->post_name;
	$file_name = explode('-', $post_slug, 2);
	$service_type = $file_name[1];
	$template_fname= "template-parts/".$service_type;	
?>
<br><br>
		<section class="getstarted pb-2">
			<div class="banner-img">
				<?php the_post_thumbnail('full');?>
			</div>
			<div class="landing-banner-text">
				<div class="container-fluid">
					<div class="row landing-row mt-lg-3 px-sm-4 px-2">
						<div class="col-lg-9 landing-service pl-lg-0">
							<div class="row">
<!-- 								<div class="col-md-1 doted-img">
									<img src="https://assivo.com/dev/wp-content/uploads/2020/05/left-line.png">
								</div> -->
								<div class="col-md-12 col-12 landing-top-text">
									<div class="under_line text-center my-2 pr-md-4">
										<?php $sub_head = get_field('sub_heading');
										echo $sub_head; ?>
									</div>
									<div class="row px-lg-3 px-0">
										<div class="col-sm-7 it-case-study-large px-0">
											<div class="landing-service px-1">
												<h3> <?php the_field('after_banner_text_heading'); ?></h3>
												<?php the_field('after_banner_text'); ?>
											</div>
										</div>
										<div class="col-sm-5 it-case-study-large after_banner_img text-center px-0">
											<?php while ( have_posts() ) : the_post(); ?>
											<?php $after_banner_image =  get_field('after_banner_image'); ?>
											<?php endwhile; ?>
											<img src="<?php echo $after_banner_image; ?>">
										</div>
										
										<?php get_template_part( 'template-parts/landing-page-boxes', 'none' );?>
								</div>
							</div>
						</div>
					</div>
					<span style="display: none" id="h-form-subject"><?php echo get_field('form_email_subject');?></span>
					<div class="col-lg-3 flat-box get-started-box px-0 mt-lg-0 mt-3" id="request_proposal">
						<div class="form-column pb-0 pt-xl-2 pt-3 px-xl-4 px-md-3 px-2">
							<div class="get-started-form-box landing">
								<h6 class="mb-3 under_line"><?php the_field('form_heading'); ?></h6>
 								<?php echo do_shortcode( '[contact-form-7 id="1219" title="Get Started Form"]'); ?>
								<div class="row text-left form-footer">
									<div class="col-2 form-footer-img">
										<img src="https://assivo.com/dev/wp-content/uploads/2020/06/icon-01-7.svg">
									</div>
									<div class="col-10 form-footer-text">
										<p>444 W. Lake St. Suite 1700 Chicago, IL 60606</p>
									</div>
								</div>
								<div class="row text-left">
									<div class="col-2 form-footer-img">
										<img src="https://assivo.com/dev/wp-content/uploads/2020/06/icon-01-8.svg">
									</div>
									<div class="col-10 form-footer-text">
										<a href="mailto:hello@assivo.com">hello@assivo.com</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/5-step-process', 'none' );?>
	
	<?php get_template_part( 'template-parts/testimonials', 'none' );?>

	<section class="text-center explore my-1 pb-md-3 bg-colour">
		<?php get_template_part( 'template-parts/use_cases', 'none' );?>
	</section>
	
	<section class="text-center explore my-1 pt-md-3 ">
		<?php get_template_part( 'template-parts/case_studies', 'none' );?>
	</section>
	
	<section class="mt-5 mb-3 pb-2"  id="our_advantage">
		<div class="container">
			<div class="row pt-md-3 px-sm-0 px-2 mx-auto">
				<div class="col-md-8 pb-md-3 pb-0 explore_text text-center mx-auto advantage-heading">
					<h3>Our Advantage</h3>
				</div>
			</div>
			<br>
			<?php get_template_part( 'template-parts/our_advantage', 'none' );?>
		</div>
	</section>


<!-- 	<section class="my-1 pt-md-3 case-studies">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center main-heading">
					<h3>
						Customer Success Stories
					</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1 col-3 icon">
					<img src="https://assivo.com/dev/wp-content/uploads/2018/10/Cases-Study-Icon-01-2.svg">
				</div>
				<div class="col-sm-5 col-9 heading">
					<h2>
						Data Entry & Processing
					</h2>
				</div>
				<div class="col-sm-6 col-12 text-right client-company">
					<p>
						<i>E-Commerce Product Data Entry & Enrichment / Furniture Distributor</i>
					</p>
				</div>
			</div>
			<div class="row studies-content justify-content-between">
				<div class="col-md-5 testimonials">
					<div class="testimonial-inner">
						<h3 text-center>
							Client-Testimonial
						</h3>
						<blockquote>
							<p>
								With the management headaches of managing a product catalog team off my plate, I have been able to focus my efforts on tightening up our operations, developing stronger relationships with our current furniture manufacturers, and scouting the globe for new furniture manufacturers.  The efficiency and professionalism with which Assivo operates is second-to-none.
							</p>
						</blockquote>
						<p>
							- VP of Business Development									 
						</p>
					</div>
				</div>
				<div class="col-md-6 case-studies-data">
					<h3>
						The Client Company
					</h3>
					<p>
						Our client is an online retailer of mid to high-end home & outdoor furnishings and décor.  They carry and distribute over 500 furniture brands, and consistently make updates to a product catalog that consists of over 750,000 SKUs.
					</p>
					<h3>
						Client's Challenges
					</h3>
					<p>
						Our client had set up an in-house team of 12 data entry / product catalog specialists that primarily focused on interacting with their furniture brands, receiving relevant updates to product descriptions and data, and updating prices.  Their in-house team was plagued with high turnover, low productivity, high absenteeism, and poor accuracy due to their mundane and repetitive nature of this work.  This broken process was negatively impacting our clients’ relationships with their furniture brands as well as suppressing revenue and sales potential.
					</p>
					<h3>
						<img src="https://assivo.com/dev/wp-content/themes/assivo/images/aaassivo-blue-logo.svg"> assivo's solution
					</h3>
					<p>
						Assivo supplemented our clients’ in-house team with 5 full-time associates in our India Operations Center that worked during the U.S. overnight shift to start off our relationship.  Over the course of our first month of working together, it became apparent to our senior client contacts that Assivo’s team was producing more accurate, reliable, and consistent output vs. their in-house team.  Assivo’s team of 5 full-time associates updated approximately 10,000 SKUs per month, a level of output similar to our clients’ in-house team of 12 employees.
Our client eventually expanded the relationship with Assivo from 5 to 15 full-time associates, and retained 2 of their highest performing in-house employees to oversee and interface with the Assivo team.  We reliably and continuously process / update over 30,000 SKUs for our client per month, and they have been able to focus their efforts on building relationships with new furniture brands and manufacturers and expanding their business.
					</p>
				</div>
			</div>
		</div>
	</section> -->
	
	<?php	
// 		get_template_part($template_fname);
// 		get_template_part( 'template-parts/how_it_works', 'none' );
	
	?>
<!-- 	<section class="mt-5 mb-3"  id="our_advantage">
	    	<div class="container">
	    		<div class="row pt-md-3 px-sm-0 px-2 mx-auto">
	    			<div class="col-md-8 pb-md-3 pb-0 explore_text text-center mx-auto advantage-heading">
	    		      <h3>Our Advantage</h3>
	    		    </div>
	    		</div>
			<br>
	    		<?php get_t//emplate_part( 'template-parts/our_advantage', 'none' );?>
	    	</div>
       </section> -->
      <?php get_template_part( 'template-parts/faqs_landing', 'none');?>
	
      <?php
// 	 if($service_type != "machine-learning-ai-training" && $service_type != "content-moderation" ) { ?>
		<section>
			<?php // get_template_part( 'template-parts/software_tools', 'none' );?>
			<?php get_template_part( 'template-parts/landing-assivo-insights');?>
		</section>
	<?php // } ?>
	
    <?php 
// 		get_template_part( 'template-parts/request_consultation', 'none' );
	?>

<script>

jQuery(document).ready(function($){
	
//   if(! $('#h_casestudies').hasClass('active')){
// 	  $('#data-entry-processing_1').addClass("active");
//   }
	
  $("#wpcf7-f1219-o1 select[name=csize] option:first").text("Choose Company Size");
	
});	

</script>



<?php	get_footer();