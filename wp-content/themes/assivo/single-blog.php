<?php
/**
 * The template for displaying all single blog posts
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */

get_header(); ?>

<style>
	.assive-home-page, .mobile_view_new{
		display: none !important;
	}
	.blog-content li {
		margin-top: 20px;
	}
</style>

	<section class="blog-slider pt-5">
		<?php get_template_part( 'template-parts/blog_nav', 'none' );?>
		<div class="container">
			<div class="row after-header pt-5">
				<div class="col-md-12 mb-4 blog-slider-text">
					<?php
						$blog_slider_terms = get_the_terms( $post->ID, 'blog_categories' );
						foreach($blog_slider_terms as $blog_slider_term) {
							$blog_slider_term_icon = get_field('taxonomy_icon', $blog_slider_term->taxonomy.'_'.$blog_slider_term->term_id);
						?>
						<a class="slider-icon" href="<?php echo site_url($blog_slider_term->taxonomy.'/'.$blog_slider_term->slug); ?>">
							<img src="<?php echo $blog_slider_term_icon; ?>">
							<?php echo $blog_slider_term->name; ?>
						</a>
					<?php } ?>
				</div>
				<div class="col-md-12 main-title">
					<p>
						<?php the_title(); ?>
					</p>
				</div>
				<div class="col-md-12 after-header-bottom">	
					<div class="row">
						<div class="col-md-6 col-6">
							<img src="<?php the_field('author_image'); ?>" class="testimonial-image">
							<div class="title pt-4"><?php the_field('author_name'); ?></div>
							<div class="designation">(<?php the_field('author_designation'); ?>)</div>
						</div>
						<div class="col-md-6 col-6 mt-5 pt-3">
							<p>
								<?php echo get_the_date(); ?> &nbsp; | &nbsp; <?php the_field('blog_min_read'); ?> read
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php	
		$blog_terms = get_the_terms( $post->ID, 'blog_categories' );
		$h_check = true;
		foreach($blog_terms as $blog_term) {
			if($blog_term->slug == 'customer-success-stories'){
				$h_check = false;
			}
		}
		if($h_check == true){ ?>
			<section class="blog-content">
				<div class="container">
					<div class="row related-blog pt-4">
<!-- 						<div class="col-md-12"> -->
							<!-- 					<img src="https://assivo.com/dev/wp-content/uploads/2020/05/michael.png" class="content-image"> -->
							<div class="col-lg-6 col-md-12 content-image">
								<?php the_post_thumbnail('single-post-thumbnail'); ?>
							</div>
							<div class="col-lg-6 col-md-12 text-right-image">
								<?php the_field('text_right_to_image'); ?>
							</div>
<!-- 						</div> -->
						<div class="col-md-12">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</section>
		<?php }
		else{ ?>

			<section class="use-case colapseable-item">
				<div class="container">
					<div class="row mx-auto px-2 mt-4 h_max-content">
						<div class="col-12 contnt_div">
							<div class="study_boxes pb-2">
								<h3>The Client Company</h3>
								<p><?php echo get_field('client_company'); ?></p>
							</div>
						</div>
						<div class="col-12 contnt_div">
							<div class="study_boxes pb-2">
								<h3>Client’s Challenges</h3>
								<p><?php echo get_field('client_challenge'); ?></p>
							</div>
						</div>
						<div class="col-12 contnt_div">
							<div class="study_boxes pb-2">
								<h3><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/aaassivo-blue-logo.svg" alt=""/>assivo's solution</h3>
								<p><?php echo get_field('assivo_solution'); ?></p>
							</div>
						</div>

					</div>
				</div>
			</section>
		
		<?php }?>


	<section class="about-assivo-blog">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-10 explore_text text-center">
					<h3>
						About <img src="https://assivo.com/dev/wp-content/themes/assivo/images/blue-logo-assivo.svg"> Assivo
					</h3>
					<p>
						Assivo provides progressive and technology-enabled business process outsourcing solutions to SMBs, middle market companies, startups, and emerging growth companies across a wide variety of industries. This segment of the market is often underserved by large/legacy outsourcing firms, yet disappointed with the quality of service that they receive from small and mid-size providers. <br><br>

We utilize modern technology, automation, and talented offshore teams to accelerate our clients’ business objectives in a cost-effective manner without sacrificing quality. <br><br>

Our primary service offerings are: Data Entry & Processing, Research & Data Collection, Lead Generation & CRM Management, Data Cleansing & Data Enrichment, Data & Content Moderation, and Back Office Processing & Support. <br><br>

Assivo was founded in 2016 by Karan Vaswani and is headquartered in Chicago, with state-of-the-art operations in India.
					</p>
				</div>
			</div>
		</div>
	</section>


	<section class="blog-boxes">
		<div class="container">
			<div class="row related-blog pt-4 mt-5">
				<div class="col-md-12 related-blog-title">
					<p>
						Related Insights
					</p>
				</div>
				<?php
					$terms = get_the_terms( $post->ID , 'blog_categories', 'string');
					$term_ids = wp_list_pluck($terms,'term_id');
					  $second_query = new WP_Query( array(
						  'post_type' => 'blog',
						  'tax_query' => array(
										array(
											'taxonomy' => 'blog_categories',
											'field' => 'id',
											'terms' => $term_ids,
											'operator'=> 'IN' //Or 'AND' or 'NOT IN'
										 )),
						  'posts_per_page' => 3,
						  'ignore_sticky_posts' => 1,
						  'orderby' => 'rand',
						  'post__not_in'=>array($post->ID)
					   ) );

					//Loop through posts and display...
						if($second_query->have_posts()) {
						 while ($second_query->have_posts() ) : $second_query->the_post(); ?>

							<div class="col-md-4 b-box mt-4">
								<a href="<?php echo site_url($post->post_name); ?>">
									<img src="<?php echo get_the_post_thumbnail_url(); ?>">
								</a>
								<div class="b-box-text">
									<p>
										<?php echo get_the_date(); ?> &nbsp; | &nbsp; <?php the_field('blog_min_read'); ?> read
									</p>
									<a class="heading" href="<?php echo site_url($post->post_name); ?>">
										<?php the_title(); ?>
									</a> <br><br>
									<?php
										$blog_slider_terms = get_the_terms( $post->ID, 'blog_categories' );
										foreach($blog_slider_terms as $blog_slider_term) {
											$blog_slider_term_icon = get_field('taxonomy_icon', $blog_slider_term->taxonomy.'_'.$blog_slider_term->term_id);
										?>
										<a class="box-taxonomy" href="<?php echo site_url($blog_slider_term->taxonomy.'/'.$blog_slider_term->slug); ?>">
											<img src="<?php echo $blog_slider_term_icon; ?>">
											<?php echo $blog_slider_term->name; ?>
										</a>
									<?php } ?>
								</div>
							</div>
				
					   <?php endwhile; wp_reset_query();
					   } ?>
				
<!-- 				<div class="col-md-4 b-box mt-4">
					<img src="https://assivo.com/dev/wp-content/uploads/2020/06/blog-img-2.png">
					<div class="b-box-text">
						<p>
							April 14,2020 | 5min read
						</p>
						<p class="heading">
							We are a premier partner for all of your
						</p>
						<a href="">
							<img src="https://assivo.com/dev/wp-content/uploads/2020/06/icon_03.png">
							Service-Specific Insights
						</a>
					</div>
				</div>
				<div class="col-md-4 b-box mt-4">
					<img src="https://assivo.com/dev/wp-content/uploads/2020/06/blog-img-3.png">
					<div class="b-box-text">
						<p>
							April 14,2020 | 5min read
						</p>
						<p class="heading">
							We are a premier partner for all of your
						</p>
						<a href="">
							<img src="https://assivo.com/dev/wp-content/uploads/2020/06/icon_03.png">
							Customer Success Stories
						</a>
					</div>
				</div> -->
			</div>
		</div>
	</section>

    <?php get_template_part( 'template-parts/request_consultation', 'none' );?>

<?php get_footer();