<?php
/**
 * Template Name: Landing New Page
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */

get_header('landing');
   	 global $post;
    	$post_slug=$post->post_name;
	$file_name = explode('-', $post_slug, 2);
	$service_type = $file_name[1];
	$template_fname= "template-parts/".$service_type;	
?>
		<section class="getstarted">
			<div class="banner-img">
				<?php the_post_thumbnail('full');?>
			</div>
			<div class="landing-banner-text">
				<div class="container-fluid">
					<div class="row landing-row mt-lg-3 px-sm-4 px-2">
						<div class="col-lg-9 landing-service pl-lg-0">
							<div class="row justify-content-start">
								<div class="col-lg-2 col-3 mt-xl-2 mt-sm-1 mt-0 img-postion px-sm-3 px-0">
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.svg"></a>
								</div>
								<div class="col-lg-7 col-md-7 col-12 landing-top-text">
									<div class="under_line text-center my-2">
										<?php $sub_head = get_field('sub_heading');
										echo $sub_head; ?>
										<?php the_field('sub_heading_new_landing'); ?> 
									</div>
								</div>
								<div class="col-lg-3 col-md-2 it-case-study-large after_banner_img text-center px-0">
									<img src="<?php the_field('image_landing_new'); ?>">
								</div>
								
								<?php get_template_part( 'template-parts/landing-page-boxes', 'none' );?>
								
								<div class="col-md-12">
									<?php get_template_part( 'template-parts/testimonials', 'none' );?>
								</div>
							</div>
						</div>
						<span style="display: none" id="h-form-subject"><?php echo get_field('form_email_subject');?></span>
						<div class="col-lg-3 flat-box get-started-box px-0 mt-lg-0 mt-3" id="request_proposal">
							<div class="form-column pb-0 pt-xl-2 pt-3 px-xl-4 px-md-3 px-2">
								<div class="get-started-form-box landing">
									<h6 class="mb-3 under_line"><?php the_field('form_heading'); ?></h6>
								<?php echo do_shortcode( '[contact-form-7 id="1219" title="Get Started Form"]'); ?>
									<div class="row text-left form-footer">
										<div class="col-2 form-footer-img">
											<img src="https://assivo.com/dev/wp-content/uploads/2020/06/icon-01-7.svg">
										</div>
										<div class="col-10 form-footer-text">
											<p>444 W. Lake St. Suite 1700 Chicago, IL 60606</p>
										</div>
									</div>
									<div class="row text-left">
										<div class="col-2 form-footer-img">
											<img src="https://assivo.com/dev/wp-content/uploads/2020/06/icon-01-8.svg">
										</div>
										<div class="col-10 form-footer-text">
											<a href="mailto:hello@assivo.com">hello@assivo.com</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>


<?php get_footer();