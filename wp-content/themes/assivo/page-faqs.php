<?php
/**
 * 
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */

get_header(); ?>

	<section class="faqs-section py-md-5">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center faqs-heading mb-3">
					<h3>
						FREQUENTLY ASKED QUESTIONS
					</h3>
				</div>
			</div>
			<div class="row px-1 px-sm-3">
				<?php 
				$terms_array = array( 
					'taxonomy' => 'faqs_categories', 
					'parent'   => 0 
				);
				$services_terms = get_terms($terms_array); 
				$k=1; ?>				
				<div class="tab-content col-md-12 border px-0 accordion-section">
					<?php foreach($services_terms as $service):  ?>
					<div class="accordion tab-pane active h_faq_hide" role="tablist" aria-multiselectable="true">
						<div class="card">
							<div class="card-header py-4">
								<h5>
									<button class="btn btn-link faq-head faq-heading collapsed <?php // if($k==1){echo 'h-color';} else{echo 'collapsed';}?>" type="button" data-toggle="collapse" data-target="#collapse<?php echo $k;?>" aria-expanded="true" aria-controls="collapse"><?php echo $service->name; ?></button>
									<a class="float-right toggle-circle h-js-faq plus collapsed <?php // if($k==1){echo 'minus';}else{echo 'plus collapsed';}?>" href="#" data-toggle="collapse" data-target="#collapse<?php echo $k;?>" aria-expanded="true" aria-controls="collapse"></a> 
								</h5>
								<?php 
								$i=1; $j=1; ?>
								<div class="tab-content col-md-12 border px-0 accordion-section collapse <?php //if($k == 1) echo 'show'?>" id="collapse<?php echo $k;?>">
									<?php
									$post_args = array(
										'posts_per_page' => -1,
										'post_type' => 'frequent_asked_ques',
										'order' => 'ASC',
										'tax_query' => array(
											array(
												'taxonomy' => 'faqs_categories', 
												'field' => 'term_id', 
												'terms' => $service->term_id,
											)
										)
									);
									$myposts = get_posts($post_args);?>
									<div class="accordion tab-pane <?php if($j==1){echo 'active';}else{echo '';}?> h_faq_hide" id="<?php echo $k;?>-accordian" role="tablist" aria-multiselectable="true">
										<?php
										foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
										<div class="card">
											<div class="card-header py-4" id="heading<?php echo $k;?>-<?php echo $i;?>">
												<h5>
													<button class="btn btn-link faq-head h-faq-head <?php if($i==1){echo '';}else{echo 'collapsed';}?>" type="button" data-toggle="collapse" data-target="#collapse<?php echo $k;?>-<?php echo $i;?>" aria-expanded="true" aria-controls="collapse<?php echo $k;?>-<?php echo $i;?>"><?php the_title(); ?></button>
													<!-- 										<a class="float-right toggle-circle <?php if($i==1){echo 'minus';}else{echo 'plus collapsed';}?>" href="#" data-toggle="collapse" data-target="#collapse<?php echo $k;?>-<?php echo $i;?>" aria-expanded="true" aria-controls="collapse<?php echo $k;?>-<?php echo $i;?>"></a>  -->
												</h5>
											</div>
											<div id="collapse<?php echo $k;?>-<?php echo $i;?>" class="collapse show<?php // if($i==1){echo 'show';}else{echo '';}?>" aria-labelledby="heading<?php echo $k;?>-<?php echo $i;?>" data-parent="#<?php echo $k;?>-accordian">
												<div class="card-body border mx-4 p-md-4 p-2">
													<?php the_content();?>	
												</div>
											</div>
											<?php  
											$i++; ?>
										</div>
										<?php 
										endforeach;
										$i=1;
										$j++;  ?>
									</div>
									<?php 
									wp_reset_postdata(); ?>
								</div>
							</div>
						</div>
					</div>
					<?php $k++; endforeach; ?>
				</div>
			</div>
		</div>
	</section>
	
	<section class="text-center explore my-1 pb-md-3 bg-colour">
		<?php get_template_part( 'template-parts/use_cases', 'none' );?>
	</section>

	<section class="my-3 pt-3">
		<?php get_template_part( 'template-parts/software_tools', 'none' );?>
	</section>
	
	<?php get_template_part( 'template-parts/how_it_works', 'none' );?>
	
<?php get_footer();