<?php
/**
 * Template Name: Landing D Page
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */

get_header('landing'); 
   	 global $post;
    	$post_slug=$post->post_name;
	$file_name = explode('-', $post_slug, 2);
	$service_type = $file_name[1];
	$template_fname= "template-parts/".$service_type;	
?>
<br><br>


		<section class="getstarted pb-2">
			<div class="landing-banner-text desktop_fade_slider">
				<div class="container">
					<div class="row landing-service">
						<div class="col-lg-7 col-md-8 under_line my-2">
							<?php $sub_head = get_field('sub_heading');
							echo $sub_head; ?>
							<?php get_template_part( 'template-parts/use-case-fade-scroller', 'none' );?>
						</div>
						<div class="col-lg-5 col-md-4 it-case-study-large after_banner_img text-center px-0">
							<?php while ( have_posts() ) : the_post(); ?>
							<?php $after_banner_image =  get_field('after_banner_image'); ?>
							<?php endwhile; ?>
							<img src="<?php echo $after_banner_image; ?>">
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php get_template_part( 'template-parts/our-approach', 'none' );?>

		<section class="getstarted pb-2">
			<div class="landing-after-banner">
				<div class="container scroller">
					<div class="row landing-row mt-lg-3 px-sm-4 px-2">
						<div class="col-lg-12">
							<?php get_template_part( 'template-parts/landing-page-boxes-tabs', 'none' );?>
						</div>
					</div>
				</div>
			</div>
		</section>



	<?php get_template_part( 'template-parts/how-we-can-support-you', 'none' );?>
	
	<?php get_template_part( 'template-parts/e-commerce-platforms', 'none' );?>

	<section class="text-center explore my-1 pb-md-3 bg-colour">
		<?php get_template_part( 'template-parts/use_cases', 'none' );?>
	</section>

	<?php get_template_part( 'template-parts/how_it_works', 'none' );?>

	<?php get_template_part( 'template-parts/home_150_logos', 'none' );?>

	<section class="text-center explore my-1 pt-md-3 ">
		<?php get_template_part( 'template-parts/case_studies', 'none' );?>
	</section>
	
	<section class="mt-5 mb-3 pb-2"  id="our_advantage">
		<div class="container">
			<div class="row pt-md-3 px-sm-0 px-2 mx-auto">
				<div class="col-md-8 pb-md-3 pb-0 explore_text text-center mx-auto advantage-heading">
					<h3>Our Advantage</h3>
				</div>
			</div>
			<br>
			<?php get_template_part( 'template-parts/our_advantage', 'none' );?>
		</div>
	</section>

	<section class="mb-5">
		<?php get_template_part( 'template-parts/landing-assivo-insights');?>
	</section>

	<?php get_template_part( 'template-parts/faqs_landing', 'none');?>

	<section class="form-section py-5" id="request_proposal">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 text">
					<h2><?php the_field('form_text_heading'); ?></h2>
					<?php the_field('form_text'); ?>
				</div>
				<span style="display: none" id="h-form-subject"><?php echo get_field('form_email_subject');?></span>
				<div class="col-lg-6">
					<?php echo do_shortcode('[contact-form-7 id="4406" title="Landing D Page Form"]'); ?>
				</div>
			</div>
		</div>
	</section>
	
    


<script>

jQuery(document).ready(function($){
	
//   if(! $('#h_casestudies').hasClass('active')){
// 	  $('#data-entry-processing_1').addClass("active");
//   }
	
  $("#wpcf7-f3947-o1 select[name=csize] option:first").text("Choose Company Size");
<?php 
	if(isset($_GET['calendy-popup'])){?>
		$("a#consultation-popup").click();
	<?php }
	?>
	
});	

</script>



<?php	get_footer();