<?php
/**
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */

get_header(); ?>

	<style>
		.nav-tabs-inner{
			padding: 0;
			margin-left: -9px;
		}
		#h_blog_slider_arrows button{
			top: 410px;
		}

	</style>



	<section class="blog-slider">
		
		<?php get_template_part( 'template-parts/blog_nav', 'none' );?>
		

	</section>

<!-- 	blog section start -->

	<section class="blog-boxes top">
		<div class="container">
			<div class="row">
        
  				<!-- Tab panes -->
  				<div class="tab-content">
					<?php
						$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
						$args = array(
							'post_type' => 'blog',
							'posts_per_page' => '9',
							'orderby' => 'title',
							'order' => 'DESC',
							'paged' => $paged,
							'tax_query' => array(
								array(
									'taxonomy' => 'blog_categories',
									'field' => 'slug',
									'terms' => 'customer-success-stories',
									'operator' => 'IN'
								)
							),
						);
						$blog_cat_loop = new WP_Query( $args );
						$h_count = 1;
						?>

<?php if ( have_posts() ) : ?>
							<div class="row blog-boxes-row">
                            <?php while ( have_posts() ) : the_post();?>
								

						  		<div class="col-md-4 b-box">
								<?php 
								$url = wp_get_attachment_image_src( get_post_thumbnail_id($page_id), 'large', false, '' );  
								?>
									<a href="<?php echo get_permalink($post->ID); ?>">
				  						<img src="<?php echo $url[0]; ?>">
									</a>
									<div class="b-box-text">
										<p>
											<?php echo get_the_date(); ?> &nbsp; | &nbsp; <?php the_field('blog_min_read'); ?> read
										</p>
<!-- 										<p class="heading heading-title-link"> -->
											<a class="heading" href="<?php echo get_permalink($post->ID); ?>">
												<?php the_title(); ?>
											</a><br><br>
<!-- 										</p> -->
										<?php
											$blog_terms = get_the_terms( $post->ID, 'blog_categories' );
											foreach($blog_terms as $blog_term) {
												$blog_term_icon = get_field('taxonomy_icon', $blog_term->taxonomy.'_'.$blog_term->term_id);
										?>
											<a class="box-taxonomy" href="<?php echo site_url($blog_term->taxonomy.'/'.$blog_term->slug); ?>">
												<?php $image = get_field('blog_icons');?>
												<img src="<?php echo $blog_term_icon; ?>">
												<?php echo $blog_term->name; ?>
											</a>
										<?php } ?>
									</div>
								</div>
								<?php
									if($h_count == 6){
										get_template_part( 'template-parts/how_it_works', 'none' );	
									}
									$h_count++;
								?>
								
								<?php $count++; ?>
								<?php endwhile; ?>
								<div class="row w-100 pagination-cls">
								<div class="col-md-12">
								<div class="text-center custom-pagination">
									<?php
									$total_pages = $blog_cat_loop->max_num_pages;
									if ($total_pages > 1) {
										$current_page = max(1, get_query_var('paged'));

										echo paginate_links(array(
											'base' => get_pagenum_link(1) . '%_%',
											'format' => '/page/%#%',
											'current' => $current_page,
											'total' => $total_pages,
											'prev_text'    => __('Prev'),
											'next_text'    => __('Next'),
										));
									}
									?>
								</div>
								</div>
								</div>
							</div>
							<?php endif; ?>
						<?php wp_reset_postdata(); ?>
							</div>
							<?php // }  ?>

</div> <!-- tab-content -->

					</div>
<!-- 
					<div class="row text-center">
						<div class="mx-auto">
							<nav aria-label="Page navigation example " class="pagenation-list">
								<ul class="pagination">
									<li class="page-item"><a class="page-link" href="#">Previous</a></li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul>
							</nav>
						</div>
					</div> -->
			
		</div>
	</section>

<!-- 	blog section end      -->

	<section class="blog-boxes" style="display:none;">
		<div class="container">
			<div class="row">
				<div class="col-md-4 b-box">
					<img src="https://assivo.com/dev/wp-content/uploads/2020/06/blog-img-1.png">
					<div class="b-box-text">
						<p>
							April 14,2020 | 5min read
						</p>
						<p class="heading">
							We are a premier partner for all of your research and data collection needs.
						</p>
						<a href="">
							<img src="https://assivo.com/dev/wp-content/uploads/2020/06/icon_03.png">
							Industry-Specific Insights
						</a>
					</div>
				</div>
				<div class="col-md-4 b-box">
					<img src="https://assivo.com/dev/wp-content/uploads/2020/06/blog-img-2.png">
					<div class="b-box-text">
						<p>
							April 14,2020 | 5min read
						</p>
						<p class="heading">
							We are a premier partner for all of your research and data collection needs.
						</p>
						<a href="">
							<img src="https://assivo.com/dev/wp-content/uploads/2020/06/icon_03.png">
							Industry-Specific Insights
						</a>
					</div>
				</div>
				<div class="col-md-4 b-box">
					<img src="https://assivo.com/dev/wp-content/uploads/2020/06/blog-img-3.png">
					<div class="b-box-text">
						<p>
							April 14,2020 | 5min read
						</p>
						<p class="heading">
							We are a premier partner for all of your research and data collection needs.
						</p>
						<a href="">
							<img src="https://assivo.com/dev/wp-content/uploads/2020/06/icon_03.png">
							Industry-Specific Insights
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>



    	<?php get_template_part( 'template-parts/request_consultation', 'none' );?>

<?php get_footer();