<?php
/**
 * 
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */
get_header(); 

 ?>

	<section class="use-case">
		<div class="container">
			<div class="row no-gutters">				
				
<!-- 				<div class="col-md-8 py-2 mt-2 mb-5 flat-box mx-auto header-short-desc">
					<p>	<?php echo get_field('header_box_description');?>
					</p>
				</div> -->
<!-- 				<div class="col-8 mx-auto text-center get-started mb-5 req-proposal">
					<a href="#request-proposal">REQUEST A PROPOSAL</a>
				</div> -->
				<div class="col-12 px-2 pb-5 mb-3">	<!--table-content-case-study-start-->
					<div class="row">
						<?php // $counter = 1; ?>
						<?php
					$i=1;
					$args = array( 'post_type' => 'case_studies','order' => 'ASC', 'posts_per_page' => 12 );
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post();
					$post_id = get_the_ID();
					$taxonomies=get_taxonomies('','names');
					$taxo=wp_get_post_terms($post_id, $taxonomies,  array("fields" => "slugs"));
// 						print_r($taxo);
					$length = count($taxo);
				?>
						<div class="co-12 col-sm-6 col-md-6 col-lg-3 pl-pr pt-3">
<!-- 							<div class="number"><?php // echo $counter; ?></div> -->
							<div class="case-study-table-content">
								
							<?php // $counter = $counter + 1; ?>
								<?php // $image = get_field('use_case_icon');?>
<!-- 							<img src="<?php // echo $image['url'];?>" alt="<?php // echo $image['alt'];?>"/> -->
								<?php the_post_thumbnail('full');?>
								<div class="table-content-detail">
									<div class="h-css-short-content">
										<h2><?php echo wp_trim_words( the_title(),3, '...' );?></h2>
										<p> <?php echo get_field('case_study_short_description'); ?></p>
										<?php
										$company_stage = get_field('company_stage');
										if( $company_stage): ?>
										<p>
											<?php foreach( $company_stage as $company_stage ): ?>
											<?php echo $company_stage; ?>
											<?php endforeach; ?>
										</p>
										<?php endif; ?>
									</div>
									<div class="learn_more text-center">
<!-- 										<a href="#case-study-<?php echo get_the_ID() ; ?>" class="case-study-link" id="case-study-btn-<?= get_the_ID() ;?>" data-target="#collapse<?php echo get_the_ID() ;?>">LEARN MORE</a> -->
										<a href="<?php echo the_field('css_to_insight'); ?>" class="case-study-link">LEARN MORE</a>
									</div>
								</div>
							</div>
						</div>
						<?php if($i == 8) get_template_part( 'template-parts/how_it_works', 'none' );?>
						<?php
						$i++;
						endwhile;
						?>
					</div>
				</div>
				<div class="col-12 mx-auto text-center">
					<div class="get-footer advantage-btn">
<!-- 						<a class="assivo-contact-us text-center text-white border-0 font-weight-bold blue-btn" href="https://calendly.com/assivo" target="_blank">Schedule A Call</a> -->
						<a class="assivo-contact-us text-center text-white border-0 font-weight-bold" href="<?php echo get_permalink('334');?>">Request A Proposal</a>
					</div>
				</div>
			</div>
		</div>
	</section>
				
	<?php // get_template_part( 'template-parts/how_it_works', 'none' );?>

	<div id="request-proposal">
	<?php get_template_part( 'template-parts/request_consultation', 'none' );?>			
	</div>


<?php get_footer();