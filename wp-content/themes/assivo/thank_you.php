<?php
if(isset($_GET['checkbox-202']) || isset($_GET['checkbox-180'])){
header('location:'.get_permalink("3955"));
//header('location:http://www.calendly.com/assivo');

}
/**
 * Template Name: Thank You
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */

get_header(); 

$h_fname = isset($_GET['first_name']) ? $_GET['first_name'] : 'Enter First Name';
$h_lname = isset($_GET['last_name']) ? $_GET['last_name'] : 'Enter Last Name';
$h_company_name = isset($_GET['cname']) ? $_GET['cname'] : 'Enter Company Name';
$h_company_size = isset($_GET['csize']) ? $_GET['csize'] : '';
$h_email = isset($_GET['email']) ? $_GET['email'] : 'Enter Email';
$h_phone_num = isset($_GET['phone']) ? $_GET['phone'] : 'Enter Telephone';
$h_req_message = isset($_GET['massage']) ? $_GET['massage'] : 'Your requirements/needs';
?>

	<section class="py-5 thank-you-info">
		<?php
// 			while ( have_posts() ) : the_post();

// 				the_content();
// 			endwhile; // End of the loop.
		?>
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center heading mb-5">
					<h4><em>"A journey of a thousand miles begins with the first step" -- Lao Tzu</em></h4>
				</div>
				<div class="col-md-5 col-12 addess d-flex">
					<img src="<?php echo site_url('wp-content'); ?>/themes/assivo/images/location-1.svg">
					<a href="https://goo.gl/maps/V3XTPLouvjnk45UTA" target="_blank" class="footer_address">444 W. Lake St. Suite 1700 Chicago, IL 60606</a>
				</div>
				<div class="col-md-4 col-12 email">
					<img src="<?php echo site_url('wp-content'); ?>/themes/assivo/images/email-1.svg">
					<a href="mailto:hello@assivo.com">hello@assivo.com</a>
				</div>
				<div class="col-md-3 col-12 mobile-number">
					<img src="<?php echo site_url('wp-content'); ?>/themes/assivo/images/call-1.svg">
					<a href="tel:+1(312) 416-8649">(312) 416-8649</a>
				</div>
			</div>
		</div>
	</section>

	<section class="contact-thanku py-3">
		<div class="container">
		<?php echo do_shortcode( '[contact-form-7 id="3824" title="Thank You Page Form"]' ); ?>
		</div>
	</section>


	<!-- Calendly inline widget begin -->
	
	
	<div class="calendly-inline-widget" data-url="https://calendly.com/assivo" style="min-width:320px;height:825px;overflow:hidden">
<!-- 		<div class="row justify-content-center mt-3">
			<div class="col-md-8 text-center">
				<h4>If you would prefer to schedule a call to get introduced and discuss your needs live, please feel free to use the scheduler below.</h4>
			</div>
		</div> -->
	</div>
	<script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
	<!-- Calendly inline widget end -->

 	<?php // get_template_part( 'template-parts/testimonials', 'none' );?> 
	
<script>
jQuery(document).ready(function($){
	var h_fname = '<?php echo $h_fname; ?>';
	var h_lname = '<?php echo $h_lname; ?>';
	var h_company_name = '<?php echo $h_company_name; ?>';
	var h_company_size = '<?php echo $h_company_size; ?>';
	var h_email = '<?php echo $h_email; ?>';
	var h_phone_num = '<?php echo $h_phone_num; ?>';
	var h_req_message = '<?php echo $h_req_message; ?>';
	$('.first-name').text(h_fname);
	$('.company-name').text(h_company_name);
	$('.contact-thanku .contact-form .first-name').val(h_fname);
	$('.contact-thanku .contact-form .last-name').val(h_lname);
	$('.contact-thanku .contact-form .cname').val(h_company_name);
	$('.contact-thanku .contact-form .fcsize').val(h_company_size);
	$('.contact-thanku .contact-form .mail').val(h_email);
	$('.contact-thanku .contact-form .phone-num').val(h_phone_num);
	$('.req-summary .project-data .req-message').val(h_req_message);
});	
</script>


<?php get_footer();