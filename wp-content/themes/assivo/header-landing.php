<?php
/**
 * TW_Assivo's functions and definitions
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-H4MD2ESNT8"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-H4MD2ESNT8');
</script>	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-128946754-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-128946754-1');
	</script>

	<script type="text/javascript"> _linkedin_partner_id = "2389290"; window._linkedin_data_partner_ids = 			window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); 		</script>
	<script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})();
	</script> 
	<noscript> 
		<img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=2389290&fmt=gif" />
	</noscript>
	<!-- Hotjar Tracking Code for www.assivo.com -->
	<script>
		(function(h,o,t,j,a,r){
			h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
			h._hjSettings={hjid:2117470,hjsv:6};
			a=o.getElementsByTagName('head')[0];
			r=o.createElement('script');r.async=1;
			r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
			a.appendChild(r);
		})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
	<!-- Facebook Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window,document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		 fbq('init', '1752767738215347'); 
		fbq('track', 'PageView');
	</script>
	<noscript>
		<img height="1" width="1" 
		src="https://www.facebook.com/tr?id=1752767738215347&ev=PageView
		&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="format-detection" content="telephone=no">

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/style.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/owlcarousel/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/owlcarousel/owl.theme.default.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/animate.css-master/animate.min.css">
	<script>
	  var pagename='';	
	</script>
    <?php 
    if((is_page(116) || is_page(3955)) || (isset($_GET['action']) && $_GET['action'] == 'track') || (isset($_GET['action']) && $_GET['action'] == 'track-bottom')){ ?>
		<!-- Global site tag (gtag.js) - Google Ads: 835531325 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-835531325"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-835531325'); </script>
	
	<!-- Event snippet for Book appointment conversion page --> <script> gtag('event', 'conversion', {'send_to': 'AW-835531325/mSUACM3b0dYBEL3ktI4D'}); </script>
    <?php } 
    
    ?>
	<?php wp_head(); ?>
	
	<?php wp_head(); ?>
</head>
<body <?php body_class();?>>

	<header class="assive-home-page nav-bg" style="background:linear-gradient(to right, #4775e5,#8e53e9) !important;">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-sm-3 col-3 mt-sm-1 mt-0 img-postion px-md-3 px-1">
					
					<a href="<?php echo  esc_url( home_url( '/' ) ); ?>" class="header-logo"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.svg"></a>
				</div>
				
				
				<div class="col-lg-10 col-sm-9 col-9 nav-position px-sm-3 p-0 pr-1 position-relative">
					<nav class="navbar navbar-expand-xl navbar-dark nav-top p-0 float-right">
						<button class="navbar-toggler close-icon mt-2" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="my-1 mx-2 close">X</span>
							<span class="navbar-toggler-icon"></span>
						</button>
						
						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							
							<?php wp_nav_menu( array( 'theme_location' => 'primary',
									'container' => false,
									'depth'     => 2,
									'items_wrap' => '<ul class="navbar-nav">%3$s</ul>',
									'depth' => 2,
                                    'fallback_cb' => 'bs4navwalker::fallback',
                                    'walker' => new bs4navwalker(),
                                    ) );							
								?>
							<div class="d-xl-none contact-info">
								<ul class="contact">
									<li><img src="<?php echo site_url('wp-content'); ?>/themes/assivo/images/call-1.svg"><a href="tel:(312) 416-8649">(312) 416-8649</a></li>
									<li><img src="<?php echo site_url('wp-content'); ?>/themes/assivo/images/email-1.svg"><a href="mailto:hello@assivo.com">hello@assivo.com</a></li>
									<li><img src="<?php echo site_url('wp-content'); ?>/themes/assivo/images/location-1.svg"><a href="https://goo.gl/maps/V3XTPLouvjnk45UTA" target="_blank" class="footer_address">444 W. Lake St. Suite 1700 Chicago, IL 60606</a></li>
								</ul>
							</div>
							<div class="get-started mt-xl-3 pt-xl-2 pt-3 pl-2 py-4 mb-1 d-xl-block d-none blue-btn">
<!-- 								<a href="https://calendly.com/assivo" target="_blank">Schedule A Call</a> -->
							</div>
							<div class="get-started mt-xl-3 pt-xl-2 pt-3 pl-2 py-4 mb-1 d-xl-block d-none">
<!-- 								<a href='<?php echo get_permalink('334');?>'>Request A Proposal</a> -->
								<a href="#request_proposal">Request A Proposal</a>
							</div>
							
							
						</div>
					</nav>
					<div class="get-started-lg d-xl-none d-block float-right py-sm-3 py-2 mt-sm-0 mt-2 pr-2 blue-btn h_n-pd">
<!-- 						<a href="https://calendly.com/assivo">Schedule A Call</a> -->
					</div>
					<div class="get-started-lg d-xl-none d-block float-right py-sm-3 py-2 mt-sm-0 mt-2 pr-2 h_n-pd">
<!-- 						<a href='<?php echo get_permalink('334');?>'>Request A Proposal</a> -->
						<a href="#request_proposal">Request A Proposal</a>
					</div>
				
				</div>
<!-- 				<div class="col-lg-10 col-sm-9 col-8 nav-position p-sm-0 p-0 pr-1 position-relative">
					<div class="get-started-lg d-block float-right py-sm-3 py-2 mt-sm-0 mt-2 pr-sm-3 pr-2">
						<a href='#request_proposal'>Request A Proposal</a>
					</div>
					<div class="get-started-lg purple_button d-md-block d-sm-none d-block d-xs-none float-right py-sm-3 py-2 mt-sm-0 mt-2 pr-lg-3 pr-2">
						<a href='#our_advantage'>OUR ADVANTAGE</a>
					</div>
					<div class="get-started-lg blue_button d-sm-block d-none float-right py-sm-3 py-2 mt-sm-0 mt-2 pr-lg-3 pr-2">
						<a href='#how_work'>HOW TO WORK WITH US</a>
					</div>
				</div> -->
			</div>
		</div>
	</header>
	