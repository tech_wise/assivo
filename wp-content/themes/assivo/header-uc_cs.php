<?php
/**
 * TW_Assivo's functions and definitions
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-H4MD2ESNT8"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-H4MD2ESNT8');
</script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-128946754-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-128946754-1');
	</script>
	
	<script type="text/javascript"> _linkedin_partner_id = "2389290"; window._linkedin_data_partner_ids = 			window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); 		</script>
	<script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})();
	</script> 
	<noscript> 
		<img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=2389290&fmt=gif" />
	</noscript>
	<!-- Hotjar Tracking Code for www.assivo.com -->
	<script>
		(function(h,o,t,j,a,r){
			h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
			h._hjSettings={hjid:2117470,hjsv:6};
			a=o.getElementsByTagName('head')[0];
			r=o.createElement('script');r.async=1;
			r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
			a.appendChild(r);
		})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
	<!-- Facebook Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window,document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		 fbq('init', '1752767738215347'); 
		fbq('track', 'PageView');
	</script>
	<noscript>
		<img height="1" width="1" 
		src="https://www.facebook.com/tr?id=1752767738215347&ev=PageView
		&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/style.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/owlcarousel/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/owlcarousel/owl.theme.default.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/animate.css-master/animate.min.css">
	<?php wp_head(); ?>
</head>
<body <?php body_class();?>>

<?php 
	$queried_object = get_queried_object();
	$term_array = get_queried_object();
	$term= $queried_object->slug;
	$term_id= $queried_object->term_id;
	$term_name= $queried_object->name;
	$term_description= $queried_object->description;
	$image_term = get_field('featured_image_for_term',$queried_object); 
	$mobile_image = get_field('mobile_banner_image_for_term',$queried_object);
	
?>
	<style>
		header.back_responsive{
			background: url('<?php echo $image_term['url'];?>') no-repeat!important;
			background-position: center;
			background-size: 100% !important;
		}
		@media (max-width:865px){
		<?php if(!empty($mobile_image['url'])){ ?>
			header, header.back_responsive{
				background: url('<?php echo $mobile_image['url'];?>') no-repeat!important;
				background-position: center;
				background-size: 100% !important;
			}
		<?php } ?>
		}
   	</style>
	<header class="assive-home-page" style="background: url('<?php echo $image_term['url'];?>') no-repeat;">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-4 mt-xl-2 mt-sm-1 mt-0 img-postion px-sm-3 px-0">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.png"></a>
				</div>
				<div class="col-lg-10 col-8 nav-position px-sm-3 p-0 pr-1 position-relative">
					<nav class="navbar navbar-expand-xl navbar-dark nav-top p-0 float-right">
						<button class="navbar-toggler mt-2" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<?php wp_nav_menu( array( 'theme_location' => 'primary',
									'container' => false,
									'depth'     => 2,
									'items_wrap' => '<ul class="navbar-nav">%3$s</ul>',
									'depth' => 2,
                                    'fallback_cb' => 'bs4navwalker::fallback',
                                    'walker' => new bs4navwalker(),
                                    ) );							
								?>
							
							<div class="get-started mt-xl-3 pt-xl-2 pt-3 pl-2 py-4 mb-1 d-xl-block d-none">
								<a href='<?php echo get_permalink('334');?>'>Request A Proposal</a>
							</div>
						</div>
					</nav>
					<div class="get-started-lg d-xl-none d-block float-sm-right float-left py-sm-3 py-2 mt-sm-0 mt-2 pr-sm-3 pr-2">
						<a href='<?php echo get_permalink('334');?>'>Request A Proposal</a>
					</div>
				
				</div>
				<div class="col-lg-8 col-md-10 assivo-banner-text desktop_view text-center">
					<h2><?php echo $term_name;?></h2>
					<p><?php echo $term_description; ?></p>
				</div>
			</div>
		</div>
	</header>
	
	<section class="mobile_view">
		<div class="container">
			<div class="row">
                <div class="col-lg-8 col-md-10 assivo-banner-text text-center">
					<h2><?php echo $term_name;?></h2>
					<p><?php echo $term_description; ?></p>
				</div>
			</div>
		</div>
	</section>
	