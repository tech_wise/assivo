<section class="step-process mb-md-5 d-none">
		<div class="container">
			<div class="row">
				<div class="col-md-12 steps-heading text-center">
					<h3><?php the_field('main_heading'); ?></h3>
				</div>
				<div class="col-md-12">
					<div class="steps-img text-center">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/Step Prosess.svg">
					</div>
					<div class="number">1</div>
					<div class="col-md-4 step-1 step-text text-right">
						<h5><?php the_field('step_1_heading'); ?></h5>
						<?php the_field('step_1_text'); ?>
					</div>
					<div class="number">2</div>
					<div class="col-md-4 step-2 step-text text-right">
						<h5><?php the_field('step_2_heading'); ?></h5>
						<?php the_field('step_2_text'); ?>
					</div>
					<div class="number">3</div>
					<div class="col-md-4 step-3 step-text text-left">
						<h5><?php the_field('step_3_heading'); ?></h5>
						<?php the_field('step_3_text'); ?>
					</div>
					<div class="number">4</div>
					<div class="col-md-4 step-4 step-text text-left">
						<h5><?php the_field('step_4_heading'); ?></h5>
						<?php the_field('step_4_text'); ?>
					</div>
					<div class="number">5</div>
					<div class="col-md-4 step-5 step-text text-center">
						<h5><?php the_field('step_5_heading'); ?></h5>
						<?php the_field('step_5_text'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>