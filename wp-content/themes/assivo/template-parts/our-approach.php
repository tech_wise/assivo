<!-- Our Approach Section Start -->
<section class="our-approach">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-12 text-center heading">
				<h1><?php the_field('our_approach_heading'); ?></h1>
				<?php the_field('our_approach_text_after_heading'); ?>
			</div>
			<div class="col-md-12 text-center">
				<div class="circle-img">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/assivo-circle.png">
				</div>
				<div class="number">1</div>
				<div class="col-md-4 text-1 approach-text text-right">
					<?php the_field('our_approach_text_1'); ?>
				</div>
				<div class="number">2</div>
				<div class="col-md-4 text-2 approach-text text-right">
					<?php the_field('our_approach_text_2'); ?>
				</div>
				<div class="number">3</div>
				<div class="col-md-4 text-3 approach-text text-left">
					<?php the_field('our_approach_text_3'); ?>
				</div>
				<div class="number">4</div>
				<div class="col-md-4 text-4 approach-text text-left">
					<?php the_field('our_approach_text_4'); ?>
				</div>
			</div>
			<div class="col-12 py-5 our-approach-img">
				<img src="<?php the_field('our_global_img');?>">
			</div>
		</div>
	</div>
</section>
<!-- Our Approach Section End -->
