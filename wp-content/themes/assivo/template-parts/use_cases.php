<?php
/**
 * Template part for displaying page content in page.php
 *
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */

	$first_active = 'active';
	$second_active = '';
	$filter_active = 'tab-pane';
	$url = $_SERVER["REQUEST_URI"];
	$keys = parse_url($url);
	$path = explode("/", $keys['path']);
	$last = $path[count($path)-2];
	$index_array = explode("-",$last);

	$selected_uc_category = get_field('select_related_uc_category');
	if(!empty($selected_uc_category)){
		$selected_uc_category = get_term( $selected_uc_category, 'uc_and_cs_categories' );
		if($index_array[0] == 'landing'){
			$second_active = 'active';
			$first_active = '';
			$filter_active = 'tab-pane';
		}	
	}

	if (isset($_GET['uc_filter'])) {
		$uc_filtered_array = $_GET['uc_filter'];
		$uc_filtered_array = explode(",",$uc_filtered_array);
// 		$uc_filtered_array = str_split($uc_filtered_array);
		$first_active = '';
		$second_active = '';
		$filter_active = 'active';
	}else{
		$uc_filtered_array = [];
	}
	

?>


<div class="container uc-temp-part" id="h_uc_multiselect" uccat="<?php echo $selected_uc_category->slug; ?>">
	<div class="row">
		<div class="col-12 explore_text mt-2"> <!-- pt-5 -->
			<h3>EXPLORE HOW WE CAN SUPPORT YOU</h3>
			<p>Search and scroll through our library of use cases, and find the right fit for your business needs. <br> Need something else? Most of our engagements are highly customized, so don't hesitate to get in touch with us and let's discuss!</p>
		</div>
	</div>

<div class="row new-usecase explore_tabs text-center mt-3">
			<div class="col-md-6 left-box">
					<?php 
						$i=1;
						$taxonomy ='uc_and_cs_categories';
						$terms = get_terms( $taxonomy, array( 'order' => 'ASC') );
							if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
								echo '<ul class="nav nav-tabs p-0 d-block m-auto w-100">';
								foreach ( $terms as $term ) {
									
								if($i==1){?>
									<li class="nav-item d-inline-block">
<!-- 									<input type="checkbox" class="select-all-cat all-filter-check"> -->
										<a href="<?php echo esc_url( home_url( '/' ) ).$get_parent;?>" class="h_uc_link nav-link  case-filter all-cat uc-cat <?php echo $first_active; ?>" data-category="" data-type="category">
											<img src="<?php echo site_url('wp-content'); ?>/uploads/2020/07/All-Use-Cases.png" class="icon-white"><img src="<?php echo site_url('wp-content'); ?>/uploads/2020/07/All-Use-Cases-1.png" class="icon-blue">
											<span class="cat-name">All Use Cases</span>
										</a>
									</li>
									<li class="nav-item d-inline-block">
									<input type="checkbox" class="filter-check filter" name="category">
										<?php	$image = get_field('icon_of_terms', $term);?>
										<a href="<?php echo get_term_link($term, $taxonomy);?>?type=<?php echo $get_parent;?>" class="h_uc_link nav-link  case-filter uc-cat <?php if($term->slug == $selected_uc_category->slug) echo $second_active; ?>" data-<?php echo $taxonomy;?>="<?php echo $term->slug; ?>" data-type="<?php echo $taxonomy;?>">
<!-- style="background: url(<?php echo $image['url'];?>) left center no-repeat;" -->
											<img src="<?php the_field('category_icon_white', $term); ?>" class="icon-white"><img src="<?php the_field('category_icon_blue', $term); ?>" class="icon-blue">
											<span class="cat-name"><?php echo $term->name;?></span>
										</a>
									</li>
								<?php }
								else{
// 									echo  $term->term_id;
// print_r($queried_object);
									if(is_page(47)){
									$post_type = "use_case";
									}
									else{
									$post_type = "case_studies";
									}

									$args = array(
									'post_type' => $post_type,
									'tax_query' => array(
										array(
										'taxonomy' => $taxonomy ,
										'field' => 'term_id',
										'terms' => $term->term_id
										 )
									  )
									);
 									$args = array( 'post_type' => 'use_case', 'posts_per_page' => -1, 'order'=> 'asc' );
									$query = new WP_Query( $args );
									if ( $query->have_posts() ){
									
					?>
									<li class="nav-item d-inline-block">
									<input type="checkbox" class="filter-check" name="category">
									<?php	$image = get_field('icon_of_terms', $term); ?>
										<a href="<?php echo get_term_link($term, $taxonomy);?>?type=<?php echo $get_parent;?>" class="h_uc_link nav-link  case-filter uc-cat <?php if($term->slug == $selected_uc_category->slug) echo $second_active; ?>" data-<?php echo $taxonomy;?>="<?php echo $term->slug; ?>" data-type="<?php echo $taxonomy;?>">
<!-- 				 style="background: url(<?php echo $image['url'];?>) left center no-repeat;" -->
											<img src="<?php the_field('category_icon_white', $term); ?>" class="icon-white"><img src="<?php the_field('category_icon_blue', $term); ?>" class="icon-blue">
											<span class="cat-name"><?php echo $term->name;?></span>
										</a>
									</li>
								<?php
										}
								}
									$i++;
								}
								echo '</ul>';
							}
					?>
			</div>
			<div class="row category-menu cat-uc-menu">
				<div class="col-10 c-name">
					<img src="<?php echo site_url('wp-content'); ?>/uploads/2020/07/All-Use-Cases.png" class="icon-white"><h5>All Use Cases</h5>
				</div>
				<div class="col-12 c-menu">
					<nav class="navbar navbar-expand-lg navbar-dark">
					  <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarNav1" aria-controls="navbarNav1" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					  </button>
					  <div class="collapse navbar-collapse" id="navbarNav1">
						<?php 
						$i=1;
						$taxonomy ='uc_and_cs_categories';
						$terms = get_terms( $taxonomy, array( 'order' => 'ASC') );
							if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
								echo '<ul class="nav nav-tabs p-0 d-block m-auto w-100">';
								foreach ( $terms as $term ) {
									
								if($i==1){?>
									<li class="nav-item d-inline-block">
<!-- 									<input type="checkbox" class="select-all-cat all-filter-check"> -->
										<a href="<?php echo esc_url( home_url( '/' ) ).$get_parent;?>" class="h_uc_link nav-link  case-filter all-cat uc-cat <?php echo $first_active; ?>" data-category="" data-type="category">
											<img src="<?php echo site_url('wp-content'); ?>/uploads/2020/07/All-Use-Cases.png" class="icon-white">
											<span class="cat-name">All Use Cases</span>
										</a>
									</li>
									<li class="nav-item d-inline-block">
									<input type="checkbox" class="filter-check filter" name="category">
										<?php	$image = get_field('icon_of_terms', $term);?>
										<a href="<?php echo get_term_link($term, $taxonomy);?>?type=<?php echo $get_parent;?>" class="h_uc_link nav-link  case-filter uc-cat <?php if($term->slug == $selected_uc_category->slug) echo $second_active; ?>" data-<?php echo $taxonomy;?>="<?php echo $term->slug; ?>" data-type="<?php echo $taxonomy;?>">
<!-- style="background: url(<?php echo $image['url'];?>) left center no-repeat;" -->
											<img src="<?php the_field('category_icon_white', $term); ?>" class="icon-white">
											<span class="cat-name"><?php echo $term->name;?></span>
										</a>
									</li>
								<?php }
								else{
// 									echo  $term->term_id;
// print_r($queried_object);
									if(is_page(47)){
									$post_type = "use_case";
									}
									else{
									$post_type = "case_studies";
									}

									$args = array(
									'post_type' => $post_type,
									'tax_query' => array(
										array(
										'taxonomy' => $taxonomy ,
										'field' => 'term_id',
										'terms' => $term->term_id
										 )
									  )
									);
 									$args = array( 'post_type' => 'use_case', 'posts_per_page' => -1, 'order'=> 'asc' );
									$query = new WP_Query( $args );
									if ( $query->have_posts() ){
									
					?>
									<li class="nav-item d-inline-block">
									<input type="checkbox" class="filter-check" name="category">
									<?php	$image = get_field('icon_of_terms', $term); ?>
										<a href="<?php echo get_term_link($term, $taxonomy);?>?type=<?php echo $get_parent;?>" class="h_uc_link nav-link case-filter uc-cat <?php if($term->slug == $selected_uc_category->slug) echo $second_active; ?>" data-<?php echo $taxonomy;?>="<?php echo $term->slug; ?>" data-type="<?php echo $taxonomy;?>">
<!-- 				 style="background: url(<?php echo $image['url'];?>) left center no-repeat;" -->
											<img src="<?php the_field('category_icon_white', $term); ?>" class="icon-white">
											<span class="cat-name"><?php echo $term->name;?></span>
										</a>
									</li>
								<?php
										}
								}
									$i++;
								}
								echo '</ul>';
							}
					?>
					  </div>
					</nav>
				</div>
			</div>



<!-- Right Side Start -->
<div class="col-md-6 right-box">
					<div class="container">
						<div class="row no-gutters uc-main-row">
							<div class="col-12" class="case-header" id="case-header">
								<input type="text" id="search-case" placeholder="Search">
							</div>
							<div class="use-case-study row <?php echo $first_active; ?>">
								<?php

								$args = array( 'post_type' => 'use_case', 'posts_per_page' => -1, 'order'=> 'asc' );
									$loop = new WP_Query( $args );?>
									
								<?php
									while ( $loop->have_posts() ) : $loop->the_post();
										$post_id = get_the_ID();
										$taxonomies=get_taxonomies('','names');
										$taxo=wp_get_post_terms($post_id, $taxonomies,  array("fields" => "slugs"));
										$length = count($taxo);
										  ?>
										<a href="<?php echo get_permalink();?>" class="col-md-12 case-box category vertical <?php for($i = 0; $i < $length; $i++){ echo $taxo[$i].' ';}?>">
	<!-- 										<div class="case_item p-3 bg-white mb-3">  -->
											<div class="case_item bg-white">
												<?php $image = get_field('use_case_icon');?>
												<img src="<?php echo $image['url'];?>" alt="" class="icon-uc-yellow"/>
											<!-- 	<img src="<?php the_field('yellow_icon'); ?>" alt="" class="icon-uc-yellow" /> -->
												<img src="<?php the_field('white_icon'); ?>" alt="" class="icon-uc-white float-left" />

												<?php the_title( '<h6>', '</h6>' ); 
													// echo '<p>'.wp_trim_words(  get_the_excerpt(),35, '...' ).'</p>';
												?>
												<img src="<?php echo site_url('wp-content'); ?>/themes/assivo/images/blue-arrow.png" alt="" class="uc-orange-arrow float-right" />	

											</div>
										</a>
								<?php
								endwhile;
								?>
							</div>
							</div>
							<div class="row text-center">
								<div class="mx-auto">
							  		<nav aria-label="Page navigation example " class="pagenation-list">
										<ul class="pagination">
											<li class="page-item"><a class="page-link" href="#">Previous</a></li>
											<li class="page-item"><a class="page-link" href="#">1</a></li>
											<li class="page-item"><a class="page-link" href="#">2</a></li>
											<li class="page-item"><a class="page-link" href="#">3</a></li>
											<li class="page-item"><a class="page-link" href="#">Next</a></li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
<!-- Right Side End -->


</div> <!-- Container End -->

	<script>
        pagename = 'usecase';
        totalcontents= 6;
    </script>