 <?php
/**
 * Template part for displaying page content in page.php
 *
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */

	$first_active = 'active';
	$second_active = '';
	$filter_active = 'tab-pane';
	$url = $_SERVER["REQUEST_URI"];
	$keys = parse_url($url);
	$path = explode("/", $keys['path']);
	$last = $path[count($path)-2];
	$index_array = explode("-",$last);

	$selected_uc_category = get_field('select_related_uc_category');
	if(!empty($selected_uc_category)){
		$selected_uc_category = get_term( $selected_uc_category, 'uc_and_cs_categories' );
		if($index_array[0] == 'landing'){
			$second_active = 'active';
			$first_active = '';
			$filter_active = 'tab-pane';
		}	
	}

	if (isset($_GET['uc_filter'])) {
		$uc_filtered_array = $_GET['uc_filter'];
		$uc_filtered_array = explode(",",$uc_filtered_array);
// 		$uc_filtered_array = str_split($uc_filtered_array);
		$first_active = '';
		$second_active = '';
		$filter_active = 'active';
	}else{
		$uc_filtered_array = [];
	}
// print_r($uc_filtered_array); die;

?>	
	<div class="container" id="h_uc_multiselect">
		<div class="row">
			<div class="col-12 explore_text mt-2"> <!-- pt-5 -->
				<h3>EXPLORE HOW WE CAN SUPPORT YOU</h3>
				<p>Search and scroll through our library of use cases, and find the right fit for your business needs. <br> Need something else? Most of our engagements are highly customized, so don't hesitate to get in touch with us and let's discuss!</p>
				<!--<div class="col-md-5 col-sm-6 col-12 text-center search-bar searchbar-home pt-4">
					<form action="#" method="get" class="searchform bg-lblue text-white py-2" role="search">
						<div class="row m-0 px-2">
							<input type="text" required class="text-white" placeholder="SEARCH" name="q">
							<button class="btn" type="submit"><i class="fa fa-search"></i></button>
						</div>
					</form>
				</div>-->
			</div>
		</div>
		<div class="row new-usecase explore_tabs text-center mt-3">
			<div class="col-md-6 left-box">
			<?php 
			$i=1;
			$taxonomy ='uc_and_cs_categories';
			$terms = get_terms( $taxonomy, array( 'order' => 'DESC') );
				if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
					echo '<ul class="nav nav-tabs p-0 d-block m-auto w-100">';
					foreach ( $terms as $term ) {
					if($i==1){?>
						<li class="nav-item d-inline-block">
<!-- 							<input type="checkbox" id="uc-0" name="uc_checkbox[]" value="all_use_cases"> -->
							<a class="h_uc_link nav-link <?php echo $first_active; ?>" data-toggle="tab" h_id="all" href="#"><img src="<?php echo site_url('wp-content'); ?>/uploads/2020/07/All-Use-Cases.png" class="icon-white"><img src="<?php echo site_url('wp-content'); ?>/uploads/2020/07/All-Use-Cases-1.png" class="icon-blue">All Use Cases</a>
						</li>
						<li class="nav-item d-inline-block">
<!-- 							<input type="checkbox" id="uc-<?php echo $i; ?>" name="uc_checkbox[]" value="<?php echo $term->slug; ?>" <?php if (in_array($term->slug, $uc_filtered_array)){ echo 'checked';} ?> > -->
							<a class="h_uc_link nav-link <?php if($term->slug == $selected_uc_category->slug) echo $second_active; ?>" data-toggle="tab" h_id="<?php echo $term->slug;?>" href="#"><img src="<?php the_field('category_icon_white', $term); ?>" class="icon-white"><img src="<?php the_field('category_icon_blue', $term); ?>" class="icon-blue"><?php echo $term->name;?></a>
						</li>
					<?php }
					else{
						?>
						<li class="nav-item d-inline-block">
<!-- 							<input type="checkbox" id="uc-<?php echo $i; ?>" name="uc_checkbox[]" value="<?php echo $term->slug; ?>" <?php if (in_array($term->slug, $uc_filtered_array)){ echo 'checked';} ?> > -->
							<a class="h_uc_link nav-link <?php if($term->slug == $selected_uc_category->slug) echo $second_active; ?>" data-toggle="tab" h_id="<?php echo $term->slug;?>" href="#"><img src="<?php the_field('category_icon_white', $term); ?>" class="icon-white"><img src="<?php the_field('category_icon_blue', $term); ?>" class="icon-blue"><?php echo $term->name;?></a>
						</li>
					<?php
					}
						$i++;
					}
					echo '</ul>';
				}
		?>
			</div>
			
			<div class="row category-menu cat-uc-menu">
				<div class="col-10 c-name">
					<img src="<?php echo site_url('wp-content'); ?>/uploads/2020/07/All-Use-Cases.png" class="icon-white"><h5>All Use Cases</h5>
				</div>
				<div class="col-12 c-menu">
					<nav class="navbar navbar-expand-lg navbar-dark">
					  <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarNav1" aria-controls="navbarNav1" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					  </button>
					  <div class="collapse navbar-collapse" id="navbarNav1">
						<?php 
						  $i=1;
						  $taxonomy ='uc_and_cs_categories';
						  $terms = get_terms( $taxonomy, array( 'order' => 'DESC') );
						  if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
							  echo '<ul class="nav nav-tabs p-0 d-block m-auto w-100">';
							  foreach ( $terms as $term ) {
								  if($i==1){?>
						  <li class="nav-item d-inline-block">
							  <a class="h_uc_link nav-link <?php echo $first_active; ?>" data-toggle="tab" h_id="all" href="#" style="padding-left: 55px;"><img src="<?php echo site_url('wp-content'); ?>/uploads/2020/07/All-Use-Cases.png" class="icon-white"><img src="<?php echo site_url('wp-content'); ?>/uploads/2020/07/All-Use-Cases-1.png" class="icon-blue">All Use Cases</a>
						  </li>
						  <li class="nav-item d-inline-block">
							  <a class="h_uc_link nav-link <?php if($term->slug == $selected_uc_category->slug) echo $second_active; ?>" data-toggle="tab" h_id="<?php echo $term->slug;?>" href="#"><img src="<?php the_field('category_icon_white', $term); ?>" class="icon-white"><img src="<?php the_field('category_icon_blue', $term); ?>" class="icon-blue"><?php echo $term->name;?></a>
						  </li>
						  <?php }
								  else{
						  ?>
						  <li class="nav-item d-inline-block">
							  <a class="h_uc_link nav-link <?php if($term->slug == $selected_uc_category->slug) echo $second_active; ?>" data-toggle="tab" h_id="<?php echo $term->slug;?>" href="#"><img src="<?php the_field('category_icon_white', $term); ?>" class="icon-white"><img src="<?php the_field('category_icon_blue', $term); ?>" class="icon-blue"><?php echo $term->name;?></a>
						  </li>
						  <?php
								  }
								  $i++;
							  }
							  echo '</ul>';
						  }
						  ?>
					  </div>
					</nav>
				</div>
			</div>
			
			
			<div id="selected-uc-category" style="display: none;"><?php echo $selected_uc_category->slug; ?></div>
			<div class="col-md-6 right-box">
				<div class="row tab-content p-sm-0 px-2 home-usecase" id="h_usecase">
					<input type="text" id="search-case-all" placeholder="Search" class="search-case">
					
					
					
					
					
					<?php 
// 						if (isset($_GET['uc_filter'])) {
// 							print_r ($uc_filtered_array);
// 						}
					?>
					
					
					<div id="uc_filtered" class="container <?php echo $filter_active; ?>">
<!-- 						<input type="text" id="search-case-select" placeholder="Search" class="search-case"> -->
<!-- 						<h3 class="uc-top-title">
							Selected Use Cases
						</h3> -->
						<?php

// 						$ids_args = [
// 							'post_type'      => 'use_case',
// 							'posts_per_page' => -1,
// 							'order'          => 'ASC',
// 							'fields'         => 'ids'
// 						];
// 						$all_posts_ids = get_posts( $ids_args );
// 						$move_to_front   = [959,999,971,2171];
// 						$post_ids_merged = array_merge( $move_to_front, $all_posts_ids );
// 						$reordered_ids   = array_unique( $post_ids_merged );
// 						$args = [
// 							'post_type'      => 'use_case',
// 							'posts_per_page' => -1,
// 							'post__in'       => $reordered_ids,
// 							'orderby'        => 'post__in',
// 							'order'          => 'DESC'
// 						];
						$args = array(
							'posts_per_page' => -1,
							'post_type' => 'use_case',
							'order'          => 'DESC',
							'tax_query' => array(
								array(
									'taxonomy' => 'uc_and_cs_categories', 
									'field' => 'slug',
									'terms' => $uc_filtered_array,
								)
							)
						);

// 						$args = array( 'post_type' => 'use_case', 'posts_per_page' => -1 );
						$selected_posts = get_posts($args);
						$total_items = count($selected_posts);
// 						$loop = new WP_Query( $args );
// 						print_r ($loop); die;
// 						$total_items = wp_count_posts( 'use_case' )->publish;
						$remaining_items = $total_items % 6;
						$total_slides = (int)($total_items / 6);
// 						echo $total_items.'--'.$total_slides.'--'.$remaining_items;
						$h_i = 0; $h_tab = 0;
						?>
						
						<?php //while ( $loop->have_posts() ) : $loop->the_post(); 
						foreach ( $selected_posts as $post ) : setup_postdata( $post ); $h_i++;
						if($h_i == 1 || $h_i % 6 == 1){ $h_tab++; ?>
						<div class="h_explore_tabs uc-select h_tab-<?php echo $h_tab; echo ' '; if($h_tab == 1){echo 'd-block n-new-uc';}else{echo 'd-none';} ?> row mx-auto">
							<div class="row text-center h-owl-uc owl-carousel owl-theme owl-nav-outer owl-dot-round mx-auto">
						<?php } ?>
								<div class="owl-item owl-box"> <!-- animate__animated animate__backInRight -->
									<a href="<?php echo get_permalink();?>" class="item">
										<div class="case_item bg-white">
											
											<?php
												$image = get_field('use_case_icon');?>
												<img src="<?php echo $image['url'];?>" alt="" class="icon-uc-yellow"/>
<!-- 											<img src="<?php the_field('yellow_icon'); ?>" alt="" class="icon-uc-yellow" /> -->
											<img src="<?php the_field('white_icon'); ?>" alt="" class="icon-uc-white float-left" />
											<?php the_title( '<h6 class="float-left">', '</h6>' ); 
											// echo $h_i; ?>
											<img src="<?php echo site_url('wp-content'); ?>/themes/assivo/images/blue-arrow.png" alt="" class="uc-orange-arrow float-right" />	
										</div>
									 </a>
								  </div>
										  <?php
								if($h_i % 6 == 0 || $h_i == $total_items){ ?>
										</div>
<!-- 										<div id="usecase_nav"  class="owl-nav customNav"></div> -->
									</div>
								<?php	
										if($h_tab == 1){
											$h_dots = '<button role="button" id="h_tab-'.$h_tab.'" class="owl-dot h-dot active new-uc"><span></span></button>';
										}else{
											$h_dots .= '<button role="button" id="h_tab-'.$h_tab.'" class="owl-dot h-dot"><span></span></button>';
										}
									}	wp_reset_postdata(); endforeach;
								if( $total_slides > 1 || ($total_slides == 1 && $remaining_items > 0)){ ?>
									<div class="h-dots h-dots-select owl-theme">
										<div class="owl-dots"> <?php echo $h_dots; ?> </div>
									</div>
									<div id="uc_arrows" class="owl-nav uc-arrows-select customNav">
										<button type="button" role="presentation" class="owl-prev">
											<img src="<?php echo site_url('wp-content'); ?>/themes/assivo/images/prev_slide.png">
										</button>
										<button type="button" role="presentation" class="owl-next">
											<img src="<?php echo site_url('wp-content'); ?>/themes/assivo/images/next_slide.png">
										</button>
									</div>
						<?php } ?>	
					</div>
					
					
					
					
					
					
					<div id="all" class="container tab-pane">
<!-- 							<input type="text" id="search-case-all" placeholder="Search" class="search-case"> -->
<!-- 						<h3 class="uc-top-title">
							All Use Cases
						</h3> -->
						<?php

						$ids_args = [
							'post_type'      => 'use_case',
							'posts_per_page' => -1,
							'order'          => 'ASC',
							'fields'         => 'ids'
						];
						$all_posts_ids = get_posts( $ids_args );
						$move_to_front   = [959,999,971,2171];
						$post_ids_merged = array_merge( $move_to_front, $all_posts_ids );
						$reordered_ids   = array_unique( $post_ids_merged );
						$args = [
							'post_type'      => 'use_case',
							'posts_per_page' => -1,
							'post__in'       => $reordered_ids,
							'orderby'        => 'post__in',
							'order'          => 'DESC'
						];

// 						$args = array( 'post_type' => 'use_case', 'posts_per_page' => -1 );
						$loop = new WP_Query( $args );
						$total_items = wp_count_posts( 'use_case' )->publish;
						$remaining_items = $total_items % 6;
						$total_slides = (int)($total_items / 6);
// 						echo $total_items.'--'.$total_slides.'--'.$remaining_items;
						$h_i = 0; $h_tab = 0;
						?>
						
						<?php while ( $loop->have_posts() ) : $loop->the_post(); $h_i++;
						if($h_i == 1 || $h_i % 6 == 1){ $h_tab++; ?>
						<div class="h_explore_tabs uc-all h_tab-<?php echo $h_tab; echo ' '; if($h_tab == 1){echo 'd-block n-new-uc';}else{echo 'd-none';} ?> row mx-auto">
							<div class="row text-center h-owl-uc owl-carousel owl-theme owl-nav-outer owl-dot-round mx-auto">
						<?php } ?>
								<div class="owl-item owl-box"> <!-- animate__animated animate__backInRight -->
									<a href="<?php echo get_permalink();?>" class="item">
										<div class="case_item bg-white">
											<?php
												$image = get_field('use_case_icon');?>
												<img src="<?php echo $image['url'];?>" alt="" class="icon-uc-yellow"/>
<!-- 											<img src="<?php the_field('yellow_icon'); ?>" alt="" class="icon-uc-yellow" /> -->
											<img src="<?php the_field('white_icon'); ?>" alt="" class="icon-uc-white float-left" />
											<?php the_title( '<h6 class="float-left">', '</h6>' ); 
											// echo $h_i; ?>
											<img src="<?php echo site_url('wp-content'); ?>/themes/assivo/images/blue-arrow.png" alt="" class="uc-orange-arrow float-right" />	
										</div>
									 </a>
								  </div>
										  <?php
								if($h_i % 6 == 0 || $h_i == $total_items){ ?>
										</div>
<!-- 										<div id="usecase_nav"  class="owl-nav customNav"></div> -->
									</div>
								<?php	
										if($h_tab == 1){
											$h_dots = '<button role="button" id="h_tab-'.$h_tab.'" class="owl-dot h-dot active new-uc"><span></span></button>';
										}else{
											$h_dots .= '<button role="button" id="h_tab-'.$h_tab.'" class="owl-dot h-dot"><span></span></button>';
										}
									}	endwhile;
								if( $total_slides > 1 || ($total_slides == 1 && $remaining_items > 0)){ ?>
									<div class="h-dots h-dots-all owl-theme">
										<div class="owl-dots"> <?php echo $h_dots; ?> </div>
									</div>
									<div id="uc_arrows" class="owl-nav uc-arrows-all customNav">
										<button type="button" role="presentation" class="owl-prev">
											<img src="<?php echo site_url('wp-content'); ?>/themes/assivo/images/prev_slide.png">
										</button>
										<button type="button" role="presentation" class="owl-next">
											<img src="<?php echo site_url('wp-content'); ?>/themes/assivo/images/next_slide.png">
										</button>
									</div>
						<?php } ?>	
					</div>
					
					
					<?php 
						$terms_array = array( 
						  'taxonomy' => 'uc_and_cs_categories', 
						  'parent'   => 0 
						);
						$j=2;
						$h_j = 0;
						$services_terms = get_terms($terms_array); 
						foreach($services_terms as $service): $h_j++; ?>

						<div id="<?php echo $service->slug;?>" class="container tab-pane">
<!-- 							<input type="text" id="search-case-inner" placeholder="Search" class="search-case"> -->
<!-- 							<h3 class="uc-top-title h-inner-h3">
								<?php echo $service->name;?>
							</h3> -->
							<?php 
							$post_args = array(
								  'posts_per_page' => -1,
								  'post_type' => 'use_case', 
								  'tax_query' => array(
									  array(
										  'taxonomy' => 'uc_and_cs_categories', 
										  'field' => 'term_id', 
										  'terms' => $service->term_id,
									  )
								  )
							);
							$myposts = get_posts($post_args);
							$total_items = count($myposts);
							$remaining_items = $total_items % 6;
							$total_slides = (int)($total_items / 6);
// 							echo $total_items.'--'.$total_slides.'--'.$remaining_items;
							$h_i = 0; $h_tab = 0;
							?>
<!-- 							<div class="explore_tabs row text-center mw-100 mx-auto position-relative">
								<div class="row text-center h-owl-uc owl-carousel owl-theme owl-nav-outer owl-dot-round mx-auto"> -->
									<?php foreach ( $myposts as $post ) : setup_postdata( $post );
									$h_i++;
									if($h_i == 1 || $h_i % 6 == 1){ $h_tab++; ?>
									<div class="h_explore_tabs uc-inner h_tab-<?php echo $h_j; echo $h_tab; echo ' '; if($h_tab == 1){echo 'd-block n-new-uc';}else{echo 'd-none';} ?> row mx-auto">
										<div class="row text-center h-owl-uc owl-carousel owl-theme owl-nav-outer owl-dot-round mx-auto">
											<?php } ?>
									<div class="owl-item owl-box"> <!-- animate__animated animate__backInRight -->
										<a href="<?php echo get_permalink();?>" class="item">
											<div class="case_item bg-white">
												<?php
												$image = get_field('use_case_icon');?>
												<img src="<?php echo $image['url'];?>" alt="" class="icon-uc-yellow"/>
<!-- 												<img src="<?php the_field('yellow_icon'); ?>" alt="" class="icon-uc-yellow" /> -->
												<img src="<?php the_field('white_icon'); ?>" alt="" class="icon-uc-white float-left" />
												<?php the_title( '<h6 class="float-left">', '</h6>' ); 
												// echo $h_i; ?>
												<img src="<?php echo site_url('wp-content'); ?>/themes/assivo/images/blue-arrow.png" alt="" class="uc-orange-arrow float-right" />	
											</div>
										</a>
									</div>
									<?php if($h_i % 6 == 0 || $h_i == $total_items){ ?>
										</div>
<!-- 										<div id="usecase_nav"  class="owl-nav customNav"></div> -->
									</div>
								<?php	
										if($h_tab == 1){
											$h_dots = '<button role="button" id="h_tab-'.$h_j.$h_tab.'" class="owl-dot h-dot active new-uc"><span></span></button>';
										}else{
											$h_dots .= '<button role="button" id="h_tab-'.$h_j.$h_tab.'" class="owl-dot h-dot"><span></span></button>';
										}
									}
									 endforeach; 
								if( $total_slides > 1 || ($total_slides == 1 && $remaining_items > 0)){ ?>
							<div id="uc_arrows" class="owl-nav uc-arrows-inner customNav">
								<button type="button" role="presentation" class="owl-prev">
									<img src="<?php echo site_url('wp-content'); ?>/themes/assivo/images/prev_slide.png">
								</button>
								<button type="button" role="presentation" class="owl-next">
									<img src="<?php echo site_url('wp-content'); ?>/themes/assivo/images/next_slide.png">
								</button>
							</div>
								<div class="h-dots h-dots-inner owl-theme">
									<div class="owl-dots"> <?php echo $h_dots; ?> </div>
								</div>
							<?php } ?>
<!-- 								</div>
								<div id="<?php echo $j;?>_nav"  class="owl-nav customNav"></div>
							</div> -->
							<?php $j= $j+2; ?>

							<?php wp_reset_postdata(); ?>
						</div>
					<?php endforeach;?>  
				</div>
			</div>
		</div>
	</div>

	