 
<?php
/**
 * Template part for displaying page content in page.php
 *
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */
$insight_posts = get_field('insight_posts');
// print_r($insight_posts);
if(empty($insight_posts)){
 	return;
}

?>
<div class="container">
	<div class="row">
		<div class="col-12 explore_text pt-sm-3 text-center"> <!-- mt-5 -->
			<h3>Our Insights</h3>
		</div>
	</div>

	<div class="row tab-content p-sm-0 px-2 home-usecase" id="h_blogs">
		<div id="all_2" class="container tab-pane active"><br>
			<div class="explore_tabs row text-center mw-100 mx-auto position-relative">
				<div class="row text-center owl-blog-insights owl-carousel owl-theme owl-nav-outer owl-dot-round mx-auto">
					<?php
					
// 					print_r($insight_posts[0]->ID); die;
// 					$args = array( 'post_type' => 'blog', 'posts_per_page' => '10', 'order' => 'DESC' );
// 					$loop = new WP_Query( $args );
// 					while ( $loop->have_posts() ) : $loop->the_post();
					foreach($insight_posts as $post){
// 					if ( get_post_meta($post->ID, 'featured_tp_posts', true)[0] == 'Featured' )  { ?>
					<div class="template-blog">
						<?php 
						$url = wp_get_attachment_image_src( get_post_thumbnail_id($page_id), 'large', false, '' );
						?>
						<a href="<?php echo get_permalink($post->ID); ?>">
							<?php the_post_thumbnail('full');?>
						</a>
						<div class="b-box-text">
							<p>
								<?php echo get_the_date(); ?> &nbsp; | &nbsp; <?php the_field('blog_min_read'); ?> read
							</p>
							<div>
								<a class="heading" href="<?php echo get_permalink($post->ID); ?>">
									<?php the_title(); ?>
								</a>
							</div><br>

							<?php
						$blog_terms = get_the_terms( $post->ID, 'blog_categories' );
						foreach($blog_terms as $blog_term) {
							$blog_term_icon = get_field('taxonomy_icon', $blog_term->taxonomy.'_'.$blog_term->term_id);
							?>
							<a class="box-taxonomy" href="<?php echo site_url($blog_term->taxonomy.'/'.$blog_term->slug); ?>">
								<?php $image = get_field('blog_icons');?>
								<img src="<?php echo $blog_term_icon; ?>">
								<?php echo $blog_term->name; ?>
							</a>
							<?php } ?>
						</div>
					</div>
					<?php }
// 					}
// 					endwhile;
					?>
				</div>
				<div id="blog_nav"  class="owl-nav customNav"></div>
			</div>
			<div class="view-all mt-md-5 pb-5 pt-3 text-center">
				<a class="assivo-contact-us text-center text-white border-0 font-weight-bold" href="<?php echo esc_url( home_url( '/' ) ); ?>insights">VIEW ALL</a>
			</div>
		</div>
	</div>
</div>

	