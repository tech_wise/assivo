 <?php
/**
 * Template part for displaying page content in page.php
 *
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */

	$first_active = 'active';
	$second_active = '';
	$url = $_SERVER["REQUEST_URI"];
	$keys = parse_url($url);
	$path = explode("/", $keys['path']);
	$last = $path[count($path)-2];
	$index_array = explode("-",$last);

	if(get_the_ID() == 4015 || get_the_ID() == 4019){
        $selected_cs_category = '';
    }else{
        $selected_cs_category = get_field('select_related_cs_category');
    }

	if(!empty($selected_cs_category)){
		$selected_cs_category = get_term( $selected_cs_category, 'uc_and_cs_categories' );
		if($index_array[0] == 'landing'){
			$second_active = 'active';
			$first_active = '';
		}
	}
?>
        <div class="container">
				<div class="row">
			 		<div class="col-12 explore_text h-css-head"> <!-- pt-sm-3 mt-5 -->
			 	   		<h3>Customer Success Stories</h3>
			    		<p>Our client engagements speak to the strength of our service offerings and the partnerships that we have formed since our inception in 2016. <br> When our clients succeed in measurable and quantifiable ways, we succeed alongside them. </p>
		     		</div>
				</div>
			<div class="row explore_tabs css-tabs text-center">
				<?php 
				$i=1;
				$taxonomy ='uc_and_cs_categories';
				$terms = get_terms( $taxonomy, array( 'order' => 'DESC') );
				if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
					echo '<ul class="col-lg-12 nav nav-tabs p-0 d-block m-auto w-100">';
					foreach ( $terms as $term ) {
// 						print_r($term);
						if($term->slug != 'data-mining' && $term->slug != 'data-transcription' ){
							if($i==1){?>
								<li class="nav-item d-inline-block">
									<a class="h_cs_link nav-link <?php echo $first_active; ?>" data-toggle="tab" h_id="all_1" href="#"><img src="<?php echo site_url('wp-content'); ?>/uploads/2020/07/All-Use-Cases.png" class="icon-white"><img src="<?php echo site_url('wp-content'); ?>/uploads/2020/07/All-Use-Cases-1.png" class="icon-blue">All Case Studies</a>		
								</li>
								<li class="nav-item d-inline-block">
									<a class="h_cs_link nav-link <?php if($term->slug == $selected_cs_category->slug) echo $second_active; ?>" data-toggle="tab" h_id="<?php echo $term->slug;?>_1" href="#"><img src="<?php the_field('category_icon_white', $term); ?>" class="icon-white"><img src="<?php the_field('category_icon_blue', $term); ?>" class="icon-blue"><?php echo $term->name;?></a>
								</li>
							<?php }
							else{
								?>
								<li class="nav-item d-inline-block">
									<a class="h_cs_link nav-link <?php if($term->slug == $selected_cs_category->slug) echo $second_active; ?>" data-toggle="tab" h_id="<?php echo $term->slug;?>_1" href="#"><img src="<?php the_field('category_icon_white', $term); ?>" class="icon-white"><img src="<?php the_field('category_icon_blue', $term); ?>" class="icon-blue"><?php echo $term->name;?></a>
								</li>
							<?php
							}
						}
						$i++;
					}
					echo '</ul>';
					}
				?>
			</div>
			
			<div class="row category-menu cat-cs-menu">
				<div class="col-10 c-name">
					<img src="<?php echo site_url('wp-content'); ?>/uploads/2020/07/All-Use-Cases.png" class="icon-white"><h5>All Case Studies</h5>
				</div>
				<div class="col-12 c-menu">
					<nav class="navbar navbar-expand-lg navbar-dark">
					  <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					  </button>
					  <div class="collapse navbar-collapse" id="navbarNav">
						<?php 
						$i=1;
						$taxonomy ='uc_and_cs_categories';
						$terms = get_terms( $taxonomy, array( 'order' => 'DESC') );
						if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
							echo '<ul class="col-lg-12 nav nav-tabs p-0 m-auto w-100">';
							foreach ( $terms as $term ) {
								if($term->slug != 'data-mining' && $term->slug != 'data-transcription' ){
									if($i==1){?>
										<li class="nav-item d-inline-block">
											<a class="h_cs_link nav-link <?php echo $first_active; ?>" data-toggle="tab" h_id="all_1" href="#"><img src="<?php echo site_url('wp-content'); ?>/uploads/2020/07/All-Use-Cases.png" class="icon-white"><img src="<?php echo site_url('wp-content'); ?>/uploads/2020/07/All-Use-Cases-1.png" class="icon-blue">All Case Studies</a>		
										</li>
										<li class="nav-item d-inline-block">
											<a class="h_cs_link nav-link <?php if($term->slug == $selected_cs_category->slug) echo $second_active; ?>" data-toggle="tab" h_id="<?php echo $term->slug;?>_1" href="#"><img src="<?php the_field('category_icon_white', $term); ?>" class="icon-white"><img src="<?php the_field('category_icon_blue', $term); ?>" class="icon-blue"><?php echo $term->name;?></a>
										</li>
									<?php }
									else{
										?>
										<li class="nav-item d-inline-block">
											<a class="h_cs_link nav-link <?php if($term->slug == $selected_cs_category->slug) echo $second_active; ?>" data-toggle="tab" h_id="<?php echo $term->slug;?>_1" href="#"><img src="<?php the_field('category_icon_white', $term); ?>" class="icon-white"><img src="<?php the_field('category_icon_blue', $term); ?>" class="icon-blue"><?php echo $term->name;?></a>
										</li>
									<?php
									}
								}
								$i++;
							}
							echo '</ul>';
							}
						?>
					  </div>
					</nav>
				</div>
			</div>
			
		<div id="selected-cs-category" style="display: none;"><?php echo $selected_cs_category->slug; ?></div>	
		<div class="row tab-content p-sm-0 px-2 home-usecase" id="h_casestudies">
			<div id="all_1" class="container tab-pane animate__animated animate__fadeIn <?php echo $first_active; ?>"><br>
				<div class="explore_tabs row text-center mw-100 mx-auto position-relative">
					<div class="row text-center h-owl-css owl-services owl-carousel owl-theme owl-nav-outer owl-dot-round mx-auto">
						<?php
							$cs_ids_args = [
								'post_type'      => 'case_studies',
								'posts_per_page' => -1,
								'order'          => 'ASC',
								'fields'         => 'ids'
							];
							$cs_all_posts_ids = get_posts( $cs_ids_args );
    						$cs_move_to_front   = [1032,1034,1031];
    						$cs_post_ids_merged = array_merge( $cs_move_to_front, $cs_all_posts_ids );
    						$cs_reordered_ids   = array_unique( $cs_post_ids_merged );
							$args = [
								'post_type'      => 'case_studies',
								'posts_per_page' => 12,
								'post__in'       => $cs_reordered_ids,
								'orderby'        => 'post__in',
								'order'          => 'ASC'
							];
// 							$args = array( 'post_type' => 'case_studies', 'posts_per_page' => -1 );
								$loop = new WP_Query( $args );
								while ( $loop->have_posts() ) : $loop->the_post();
									$h_featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
								  ?>
<!-- 						css-bg-img 
https://assivo.com/dev/wp-content/uploads/2018/10/css-bg-01.png
-->
									<div class="item it-case-study css-bg-img industry-case-study pb-3 mx-sm-1">
										<img src="<?php echo $h_featured_img_url; ?>">
										<div class="bg-image text-left">
											<?php the_title( '<h5>', '</h5>' ); ?>
											<?php $h_excerpt = get_the_excerpt(); 
											$a_excerpt = explode("/", $h_excerpt); ?>
										 	<?php echo '<p>'.wp_trim_words( $a_excerpt[0] ,30, '...' ).'<br>'.$a_excerpt[1].'</p>';?>
											<div class="learn_more text-center">
<!-- 												<a href="<?php echo get_permalink(17);?>#case-study-<?= get_the_ID();?>">LEARN MORE</a> -->
											<a href="<?php echo the_field('css_to_insight'); ?>" class="case-study-link">LEARN MORE</a>
											</div>
										</div>
									</div>
								  <?php
								endwhile;
							?>
					</div>
					<div id="services_nav"  class="owl-nav customNav"></div>
				</div>
				<div class="view-all pt-4">
					<a class="assivo-contact-us text-center text-white border-0 font-weight-bold" href="<?php echo esc_url( home_url( '/' ) ); ?>case-study">VIEW ALL</a>
				</div>
			</div>
			<?php 
				$terms_array = array( 
				  'taxonomy' => 'uc_and_cs_categories', 
				  'parent'   => 0 
				);
				$i=1;
				$services_terms = get_terms($terms_array); 
				foreach($services_terms as $service): ?>
				
				<div id="<?php echo $service->slug;?>_1" class="container tab-pane animate__animated animate__fadeIn"><br>
					<?php 
					$post_args = array(
						  'posts_per_page' => -1,
						  'post_type' => 'case_studies', 
						  'tax_query' => array(
							  array(
								  'taxonomy' => 'uc_and_cs_categories', 
								  'field' => 'term_id', 
								  'terms' => $service->term_id,
							  )
						  )
					);
					$myposts = get_posts($post_args);
					$h_posts = count($myposts);
					?>
					<div class="explore_tabs row text-center mw-100 mx-auto position-relative">
						<div class="row text-center h-owl-css owl-<?php echo $i;?> owl-carousel owl-theme owl-nav-outer owl-dot-round mx-auto">
							<?php foreach ( $myposts as $post ) : setup_postdata( $post ); 
							$h_featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
								<div class="item it-case-study css-bg-img industry-case-study pb-3 mx-1">
									<img src="<?php echo $h_featured_img_url; ?>">
									<div class="bg-image text-left">
										<?php the_title( '<h5>', '</h5>' ); ?>
									 	<?php $h_excerpt = get_the_excerpt(); 
											$a_excerpt = explode("/", $h_excerpt); ?>
										 	<?php echo '<p>'.wp_trim_words( $a_excerpt[0] ,30, '...' ).'<br>'.$a_excerpt[1].'</p>';?>
										<div class="learn_more text-center">
<!-- 											<a href="<?php echo get_permalink(17);?>#case-study-<?= get_the_ID();?>">LEARN MORE</a> -->
											<a href="<?php echo the_field('css_to_insight'); ?>" class="case-study-link">LEARN MORE</a>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
						<?php if($h_posts > 3){ ?>
							<div id="<?php echo $i;?>_nav"  class="owl-nav customNav"></div>
						<?php }else{ ?>
							<div id="<?php echo $i;?>_nav"  class="owl-nav customNav <?php if($h_posts == 3){echo 'd-lg-none d-md-block';}else if($h_posts == 2){ echo 'd-block d-sm-none';}else{echo 'd-none';} ?> "></div>
						<?php } ?>
					</div>
					<?php $i++; ?>
					<div class="view-all pt-4"> <!-- mt-md-5 pb-5 pt-3 -->
						<a class="assivo-contact-us text-center text-white border-0 font-weight-bold" href="<?php echo esc_url( home_url( '/' ) ); ?>case-study">VIEW ALL</a>
					</div>
					<?php $i++; ?>
					<?php wp_reset_postdata(); ?>
				</div>
			<?php endforeach;?>  
		</div>
		<div class="row justify-content-center">
			<div class="col-lg-3 col-md-4 col-6">
				<div class="get-footer my-5">
					<a class="assivo-contact-us text-center d-none text-white border-0 font-weight-bold" href="#request_proposal">Request A Proposal</a>
				</div>
			</div>
		</div>
	</div>

	