		<section class="container ecommerce-platforms my-md-5 d-none"> <!-- mt-md-0 mt-3 -->
    		<div class="row justify-content-center">
    			<div  class="col-lg-8 col-md-10 col-12 explore_text text-center">
    		      <h3>OUR TEAMS ARE TRAINED AND EXPERIENCED WITH ALL OF THE LEADING E-COMMERCE PLATFORMS</h3>
    		    </div>
					<?php 
						$post_args = array(
							  'posts_per_page' => -1,
							  'order' => 'ASC',
							  'post_type' => 'tools_and_softwares',
							  'tax_query' => array(
								  array(
									  'taxonomy' => 'tools_categories', 
									  'field' => 'term_id',
									  'terms' => 80,
								  )
							  )
						);
						
						$myposts = get_posts($post_args); ?>
						<ul class="software_tools">
						<?php foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
						  <li>
							<img src="<?php echo get_the_post_thumbnail_url(get_the_id(),"full"); ?>" alt="">
						  </li>
						<?php endforeach;?>
						</ul>
						<?php wp_reset_postdata(); ?>
    		</div>
    	</section>