										<div class="slider-main">
												<div class="slider-static">
													<p id="static-text">Customers use <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-white.png" class="white-logo"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-black.png" class="black-logo"> assivo to </p>
												</div>
												<div class="slider">
													<ul id="slides">
														<?php 
														$current_landpage = get_the_ID();
														
														$q_fade_slider = new WP_Query( array('post_type' => 'top_uc_slider', 'orderby'   => 'rand', 'order' => 'rand', 'posts_per_page' => -1) );
														
														while ( $q_fade_slider ->have_posts() ) : $q_fade_slider ->the_post();
														$landpage_array = get_field('landing_page');
														
														if(!empty($landpage_array ) && in_array($current_landpage, $landpage_array )) {
														echo '<li class="slide">'.get_the_title().'</li>';
														}
														endwhile;
														wp_reset_postdata();
														?>
														
														
													</ul>

												</div>
											</div>
