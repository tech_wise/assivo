 <?php
/**
 * Template part for displaying page content in page.php
 *
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */

?>

	<div id="request_proposal" class="pt-5"></div>
 	<section class="get-in-touch request-bg-img">  <!-- pt-xl-5 -->
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="row">
						<div class="col-md-8 col-12 get-touch text-center">
							<?php if(is_page('2')){?>
							<h3>GET IN TOUCH - SEND US A NOTE</h3>
							<?php }
							else{
							?>
							<h3>REQUEST A PROPOSAL TODAY</h3>
							<?php } ?>
						</div>
						
						<div class="row get-touch-form contact_form mw-100 my-0 mx-auto pb-4">
							<div class="col-md-10 offset-md-1 case-study-form request_const"> 	
								<div class="get-started-form-box">
									<?php 
									if(is_page(4713)){
									 	echo do_shortcode( '[contact-form-7 id="4755" title="Mortgage outsourcing Bottom Form"]' ); 
									}elseif(is_page(4803)){
										echo do_shortcode( '[contact-form-7 id="4800" title="Mortgage outsourcing C Bottom Form"]' );
									}else{
										echo do_shortcode( '[contact-form-7 id="4843" title="Landing B Bottom Form"]' );
									}
// 									}elseif(is_page_template( 'landing-e.php' )){
// 										echo do_shortcode( '[contact-form-7 id="4843" title="Landing B Bottom Form"]' );
// 									}else {
// 									  	echo do_shortcode( '[contact-form-7 id="1218" title="REQUEST A CONSULTATION TODAY"]' ); 
									//} ?> 	
								</div>
							</div>
						</div>
					
				   </div>

			  </div>

		   </div>
	  </div>
	</section>

	<!-- Rediction on Conusltation page after top form submtting START -->
	<input type="hidden" id="consultation_page" value="<?php the_permalink(3955); ?>" />
	<input type="hidden" id="next_step_page" value="<?php the_permalink(116); ?>" />


	<script>
	document.addEventListener( 'wpcf7mailsent', function( event ) {
	var checkedValue = document.querySelector('.check-boxes .active input:checked').value;
	var consultation = document.getElementById("consultation_page").value;
	var next_step = document.getElementById("next_step_page").value;
// 	 alert(checkBox + consultation);

	if(checkedValue != "A short-term project (4-8 weeks)" ){
		location = consultation;
	}else {
		location = next_step;
	}

	}, false );
	// Rediction on Conusltation page after top form submtting End
	
</script>