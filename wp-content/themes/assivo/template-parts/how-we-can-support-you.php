	<section class="supporting-boxes my-5 d-none">
		<div class="container">
			<div class="row text-center">
				<div class="col-md-12 support-heading">
					<h3><?php the_field('support_heading'); ?></h3>
				</div>
				<div class="col-md-3 support-box">
					<img src="<?php the_field('support_icon_1'); ?>">
					<?php the_field('support_text_1'); ?>
				</div>
				<div class="col-md-3 support-box">
					<img src="<?php the_field('support_icon_2'); ?>">
					<?php the_field('support_text_2'); ?>
				</div>
				<div class="col-md-3 support-box">
					<img src="<?php the_field('support_icon_3'); ?>">
					<?php the_field('support_text_3'); ?>
				</div>
				<div class="col-md-3 support-box">
					<img src="<?php the_field('support_icon_4'); ?>">
					<?php the_field('support_text_4'); ?>
				</div>
				<div class="col-md-3 support-box">
					<img src="<?php the_field('support_icon_5'); ?>">
					<?php the_field('support_text_5'); ?>
				</div>
				<div class="col-md-3 support-box">
					<img src="<?php the_field('support_icon_6'); ?>">
					<?php the_field('support_text_6'); ?>
				</div>
				<div class="col-md-3 support-box">
					<img src="<?php the_field('support_icon_7'); ?>">
					<?php the_field('support_text_7'); ?>
				</div>
				<div class="col-md-3 support-box">
					<img src="<?php the_field('support_icon_8'); ?>">
					<?php the_field('support_text_8'); ?>
				</div>
				<div class="col-md-3 support-box">
					<img src="<?php the_field('support_icon_9'); ?>">
					<?php the_field('support_text_9'); ?>
				</div>
				<div class="col-md-3 support-box">
					<img src="<?php the_field('support_icon_10'); ?>">
					<?php the_field('support_text_10'); ?>
				</div>
				<div class="col-md-3 support-box">
					<img src="<?php the_field('support_icon_11'); ?>">
					<?php the_field('support_text_11'); ?>
				</div>
				<div class="col-md-3 support-box">
					<img src="<?php the_field('support_icon_12'); ?>">
					<?php the_field('support_text_12'); ?>
				</div>
			</div>
		</div>
	</section>
