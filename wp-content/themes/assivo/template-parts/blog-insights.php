 <?php
/**
 * Template part for displaying page content in page.php
 *
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */
?>

        <div class="container">
			<div class="row">
			 	<div class="col-12 explore_text pt-sm-1 text-center"> <!-- mt-5 -->
			 	   	<h3>Our Insights</h3>
		     	</div>
			</div>
<!-- 		   	<div class="row explore_tabs text-center mt-4"> -->
<!-- 				<?php 
				$i=1;
				$taxonomy ='blog_categories';
				$terms = get_terms( $taxonomy, array( 'order' => 'DESC') );
					if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
						echo '<ul class="col-lg-12 nav nav-tabs p-0 d-block m-auto w-100">';
						foreach ( $terms as $term ){
								if($i==1){?>
									<li class="nav-item d-inline-block">
										<a class="h_cs_link nav-link active" data-toggle="tab" h_id="all_2" href="#">All</a>		
									</li>
									<li class="nav-item d-inline-block">
										<a class="h_cs_link nav-link" data-toggle="tab" h_id="<?php echo $term->slug;?>_2" href="#"><?php echo $term->name;?></a>
									</li>
								<?php }
								else{
									?>
									<li class="nav-item d-inline-block">
										<a class="h_cs_link nav-link" data-toggle="tab" h_id="<?php echo $term->slug;?>_2" href="#"><?php echo $term->name;?></a>
									</li>
								<?php
								
							}
							$i++;
						}
						echo '</ul>';
					}
			?> -->
<!-- 		</div> -->
			
		<div class="row tab-content p-sm-0 px-2 home-usecase" id="h_blogs">
			<div id="all_2" class="container tab-pane active"><br>
				<div class="explore_tabs row text-center mw-100 mx-auto position-relative">
					<div class="row text-center owl-blog-insights owl-carousel owl-theme owl-nav-outer owl-dot-round mx-auto">
						<?php
							
 							$args = array( 'post_type' => 'blog', 'posts_per_page' => '-1', 'order' => 'DESC' );
								$loop = new WP_Query( $args );
								while ( $loop->have_posts() ) : $loop->the_post();
								if ( get_post_meta($post->ID, 'featured_tp_posts', true)[0] == 'Featured' )  { ?>
									<div class="template-blog">
									<?php 
										$url = wp_get_attachment_image_src( get_post_thumbnail_id($page_id), 'large', false, '' );
								?>
										<a href="<?php echo get_permalink($post->ID); ?>">
											<?php the_post_thumbnail('full');?>
										</a>
										<div class="b-box-text">
											<p>
												<?php echo get_the_date(); ?> &nbsp; | &nbsp; <?php the_field('blog_min_read'); ?> read
											</p>
											<div>
												<a class="heading" href="<?php echo get_permalink($post->ID); ?>">
													<?php the_title(); ?>
												</a>
											</div><br>
											
												<?php
													$blog_terms = get_the_terms( $post->ID, 'blog_categories' );
													foreach($blog_terms as $blog_term) {
														$blog_term_icon = get_field('taxonomy_icon', $blog_term->taxonomy.'_'.$blog_term->term_id);
												?>
											<a class="box-taxonomy" href="<?php echo site_url($blog_term->taxonomy.'/'.$blog_term->slug); ?>">
												<?php $image = get_field('blog_icons');?>
												<img src="<?php echo $blog_term_icon; ?>">
												<?php echo $blog_term->name; ?>
											</a>
										<?php } ?>
									</div>
								</div>
								  <?php }
								endwhile;
							?>
					</div>
					<div id="blog_nav"  class="owl-nav customNav"></div>
				</div>
				<div class="view-all pb-5 pt-4 text-center">
					<a class="assivo-contact-us text-center text-white border-0 font-weight-bold" href="<?php echo esc_url( home_url( '/' ) ); ?>insights">VIEW ALL</a>
				</div>
			</div>
<!-- 			<?php 
				$terms_array = array( 
				  'taxonomy' => 'blog_categories', 
				  'parent'   => 0 
				);
				$i=1;
				$blog_terms = get_terms($terms_array); 
				foreach($blog_terms as $blog): ?>
			
		
			<div id="<?php echo $blog->slug;?>_2" class="container tab-pane active"><br>
				<div class="explore_tabs row text-center mw-100 mx-auto position-relative">
					<div class="row text-center owl-blog-insights owl-carousel owl-theme owl-nav-outer owl-dot-round mx-auto">
						<?php
							
 							$args = array( 'post_type' => 'blog', 'posts_per_page' => -1 );
								$loop = new WP_Query( $args );
								while ( $loop->have_posts() ) : $loop->the_post();
								  ?>
									<div>
									<?php 
										$url = wp_get_attachment_image_src( get_post_thumbnail_id($page_id), 'large', false, '' );
								?>
										<a href="<?php echo get_permalink($post->ID); ?>">
											<?php the_post_thumbnail('full');?>
										</a>
										<div class="b-box-text">
											<p>
												<?php echo get_the_date(); ?> &nbsp; | &nbsp; <?php the_field('blog_min_read'); ?> read
											</p>
											<a class="heading" href="<?php echo get_permalink($post->ID); ?>">
												<?php the_title(); ?>
											</a><br><br>
												<?php
													$blog_terms = get_the_terms( $post->ID, 'blog_categories' );
													foreach($blog_terms as $blog_term) {
														$blog_term_icon = get_field('taxonomy_icon', $blog_term->taxonomy.'_'.$blog_term->term_id);
												?>
											<a class="box-taxonomy" href="<?php echo site_url($blog_term->taxonomy.'/'.$blog_term->slug); ?>">
												<?php $image = get_field('blog_icons');?>
												<img src="<?php echo $blog_term_icon; ?>">
												<?php echo $blog_term->name; ?>
											</a>
										<?php } ?>
									</div>
								</div>
								  <?php
								endwhile;
							?>
					</div>
					<div id="blog_nav"  class="owl-nav customNav"></div>
				</div>
			<?php $i++; ?>
					<div class="view-all mt-md-5 pb-5 pt-3">
						<a class="assivo-contact-us text-center text-white border-0 font-weight-bold" href="<?php echo esc_url( home_url( '/' ) ); ?>category/<?php echo $blog->slug;?>/?type=blog">VIEW ALL</a>
					</div>
					<?php $i++; ?>
					<?php wp_reset_postdata(); ?>
				</div>
			<?php endforeach;?>  
		</div> -->
	</div>

	