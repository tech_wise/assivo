<?php 
if(is_page_template( 'landing-e.php' )){ ?>

<!-- FAQs Section Start -->
<section class="faqs-section py-md-5">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center faqs-heading mb-3">
				<h3>
					FREQUENTLY ASKED QUESTIONS
				</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 text-center">
				<?php 
					$terms_array = array( 
						'taxonomy' => 'faqs_categories', 
						'parent'   => 0 
					);
					$services_terms = get_terms($terms_array); 
					$k=1; 
				?>
				<ul class="nav nav-tabs text-center" id="faq-tabs">
					<?php foreach($services_terms as $service):  ?>
					<li class="<?php if($k == 1){ echo 'active'; }?>">
						<a data-toggle="tab" href="#faq-<?php echo $service->term_id;?>" class="tab <?php if($k == 1){ echo 'active show'; }?>">
							<img src="<?php the_field('category_icon_white', $service); ?>" class="icon-white">
							<img src="<?php the_field('category_icon_blue', $service); ?>" class="icon-blue">
							<p><?php echo $service->name ?></p>
						</a>
					</li>
					<?php $k++; endforeach; ?>
				</ul>
				<?php //wp_reset_postdata(); ?>
			</div>
		</div>


		<div class="tab-content" id="faq-tabs-content">
			<?php
				$terms_array = array( 
					'taxonomy' => 'faqs_categories', 
					'parent'   => 0 
				);
				$services_terms = get_terms($terms_array);

				$j=1;

				foreach($services_terms as $service):

					$post_args = array(
						'posts_per_page' => -1,
						'post_type' => 'frequent_asked_ques',
						'order' => 'ASC',
						'tax_query' => array(
							array(
								'taxonomy' => 'faqs_categories', 
								'field' => 'term_id', 
								'terms' => $service->term_id,
							)
						)
					);
					
					$myposts = get_posts($post_args);
			?>
			<div id="faq-<?php echo $service->term_id;?>" class="tab-pane fade <?php if($j==1){ echo 'active show'; }?>">
				<div class="row justify-content-center">
					<div class="col-md-6 questions">
						<ul class="nav nav-tabs text-center" id="question-tabs">
						<?php 
							$n=1;
							foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
							<li class="<?php if($n == 1){ echo 'active'; }?>">
								<a data-toggle="tab" href="#question-<?php echo $post->ID;?>" class="tab <?php if($n == 1){ echo 'active show'; }?>">
									<h5><?php echo $n. '. ' ; the_title();  ?></h5>
								</a>
							</li>
						
						<?php $n++; endforeach; ?>
						</ul>
					</div>

					<div class="col-md-6 answers">
						<div class="tab-content" id="faq-tabs-content">
							<?php
								$i=1; 
								foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
							<div id="question-<?php echo $post->ID;?>" class="tab-pane fade <?php if($i==1){ echo 'active show'; }?>">
								<?php the_content(); ?>
							</div>
							<?php $i++; endforeach; ?>
						</div>
					</div>
				</div>
			</div>
			<?php $j++; endforeach; ?>
			<?php wp_reset_postdata(); ?>
		</div>
	</div>
</section>


<?php } else { ?>



<section class="faqs-section py-md-5">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center faqs-heading mb-3">
				<h3>
					FREQUENTLY ASKED QUESTIONS
				</h3>
			</div>
		</div>
		<div class="row px-1 px-sm-3">
			<?php 
			$terms_array = array( 
				'taxonomy' => 'faqs_categories', 
				'parent'   => 0 
			);
			$services_terms = get_terms($terms_array); 
			$k=1; ?>				
			<div class="tab-content col-md-12 border px-0 accordion-section">
				<?php foreach($services_terms as $service):  ?>
				<div class="accordion tab-pane active h_faq_hide" role="tablist" aria-multiselectable="true">
					<div class="card">
						<div class="card-header py-4">
							<h5>
								<button class="btn btn-link faq-head faq-heading collapsed <?php // if($k==1){echo 'h-color';} else{echo 'collapsed';}?>" type="button" data-toggle="collapse" data-target="#collapse<?php echo $k;?>" aria-expanded="true" aria-controls="collapse"><?php echo $service->name; ?></button>
								<a class="float-right toggle-circle h-js-faq plus collapsed <?php // if($k==1){echo 'minus';}else{echo 'plus collapsed';}?>" href="#" data-toggle="collapse" data-target="#collapse<?php echo $k;?>" aria-expanded="true" aria-controls="collapse"></a> 
							</h5>

							<?php 
							$i=1; $j=1; ?>
							<div class="tab-content col-md-12 border px-0 accordion-section collapse <?php //if($k == 1) echo 'show'?>" id="collapse<?php echo $k;?>">
								<?php
								$post_args = array(
									'posts_per_page' => -1,
									'post_type' => 'frequent_asked_ques',
									'order' => 'ASC',
									'tax_query' => array(
										array(
											'taxonomy' => 'faqs_categories', 
											'field' => 'term_id', 
											'terms' => $service->term_id,
										)
									)
								);
								$myposts = get_posts($post_args);?>
								<div class="accordion tab-pane <?php if($j==1){echo 'active';}else{echo '';}?> h_faq_hide" id="<?php echo $k;?>-accordian" role="tablist" aria-multiselectable="true">
									<?php
									foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
									<div class="card">
										<div class="card-header py-4" id="heading<?php echo $k;?>-<?php echo $i;?>">
											<h5>
												<button class="btn btn-link faq-head h-faq-head <?php if($i==1){echo '';}else{echo 'collapsed';}?>" type="button" data-toggle="collapse" data-target="#collapse<?php echo $k;?>-<?php echo $i;?>" aria-expanded="true" aria-controls="collapse<?php echo $k;?>-<?php echo $i;?>"><?php the_title(); ?></button>
												<!-- 										<a class="float-right toggle-circle <?php if($i==1){echo 'minus';}else{echo 'plus collapsed';}?>" href="#" data-toggle="collapse" data-target="#collapse<?php echo $k;?>-<?php echo $i;?>" aria-expanded="true" aria-controls="collapse<?php echo $k;?>-<?php echo $i;?>"></a>  -->
											</h5>
										</div>
										<div id="collapse<?php echo $k;?>-<?php echo $i;?>" class="collapse show<?php // if($i==1){echo 'show';}else{echo '';}?>" aria-labelledby="heading<?php echo $k;?>-<?php echo $i;?>" data-parent="#<?php echo $k;?>-accordian">
											<div class="card-body border mx-4 p-md-4 p-2">
												<?php the_content();?>	
											</div>
										</div>
										<?php  
										$i++; ?>
									</div>
									<?php 
									endforeach;
									$i=1;
									$j++;  ?>
								</div>
								<?php 
								wp_reset_postdata(); ?>
							</div>
						</div>
					</div>
				</div>
				<?php $k++; endforeach; ?>
			</div>
		</div>
	</div>
</section>


<?php } ?>