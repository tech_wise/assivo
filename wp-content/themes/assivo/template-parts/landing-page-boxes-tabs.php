<div class="row landing-page-tabs">
	<div class="col-md-12 landing-box-tabs">
		<ul class="nav nav-tabs text-center" id="landing-boxes">
			<li class="active">
				<a data-toggle="tab" href="#box-1" class="tab">
					<img src="<?php the_field('box_1_icon');?>">
					<h4><?php the_field('box_1_heading');?></h4>
				</a>
			</li>
			<li>
				<a data-toggle="tab" href="#box-2" class="tab">
					<img src="<?php the_field('box_2_icon');?>">
					<h4><?php the_field('box_2_heading');?></h4>
				</a>
			</li>
			<li>
				<a data-toggle="tab" href="#box-3" class="tab">
					<img src="<?php the_field('box_3_icon');?>">
					<h4><?php the_field('box_3_heading');?></h4>
				</a>
			</li>
			<li>
				<a data-toggle="tab" href="#box-4" class="tab">
					<img src="<?php the_field('box_4_icon');?>">
					<h4><?php the_field('box_4_heading');?></h4>
				</a>
			</li>
			<li>
				<a data-toggle="tab" href="#box-5" class="tab">
					<img src="<?php the_field('box_5_icon');?>">
					<h4><?php the_field('box_5_heading');?></h4>
				</a>
			</li>
			<li>
				<a data-toggle="tab" href="#box-6" class="tab">
					<img src="<?php the_field('box_6_icon');?>">
					<h4><?php the_field('box_6_heading');?></h4>
				</a>
			</li>
		  </ul>
		
		<div class="tab-content">
			<div id="box-1" class="tab-pane fade show active">
			 	<div class="row">
					<div class="col-md-8 box-content">
						<h3><?php the_field('box_1_heading');?></h3>
						<?php the_field('box_1_text');?>
					</div>
					<div class="col-md-4 text-center">
						<img src="<?php the_field('box_1_image');?>">
					</div>
				</div>
			</div>
			<div id="box-2" class="tab-pane fade">
			 	<div class="row">
					<div class="col-md-8 box-content">
						<h3><?php the_field('box_2_heading');?></h3>
						<?php the_field('box_2_text');?>
					</div>
					<div class="col-md-4 text-center">
						<img src="<?php the_field('box_2_image');?>">
					</div>
				</div>
			</div>
			<div id="box-3" class="tab-pane fade">
			 	<div class="row">
					<div class="col-md-8 box-content">
						<h3><?php the_field('box_3_heading');?></h3>
						<?php the_field('box_3_text');?>
					</div>
					<div class="col-md-4 text-center">
						<img src="<?php the_field('box_3_image');?>">
					</div>
				</div>
			</div>
			<div id="box-4" class="tab-pane fade">
			 	<div class="row">
					<div class="col-md-8 box-content">
						<h3><?php the_field('box_4_heading');?></h3>
						<?php the_field('box_4_text');?>
					</div>
					<div class="col-md-4 text-center">
						<img src="<?php the_field('box_4_image');?>">
					</div>
				</div>
			</div>
			<div id="box-5" class="tab-pane fade">
			 	<div class="row">
					<div class="col-md-8 box-content">
						<h3><?php the_field('box_5_heading');?></h3>
						<?php the_field('box_5_text');?>
					</div>
					<div class="col-md-4 text-center">
						<img src="<?php the_field('box_5_image');?>">
					</div>
				</div>
			</div>
			<div id="box-6" class="tab-pane fade">
			 	<div class="row">
					<div class="col-md-8 box-content">
						<h3><?php the_field('box_6_heading');?></h3>
						<?php the_field('box_6_text');?>
					</div>
					<div class="col-md-4 text-center">
						<img src="<?php the_field('box_6_image');?>">
					</div>
				</div>
			</div>
			<ul id="landing-boxes" class="next_pre">	
				<li class="prev"><a href="#box-1" class="tab pre-box"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/prev-icon.png"></a></li>
				<li class="next"><a href="#box-2" class="tab next-box"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/next-icon.png"></a></li>
			</ul>
			
		</div>
	</div>
</div>


<script>
	jQuery(document).ready(function(){
		$('.landing-box-tabs ul li').click(function(){
			$('.landing-box-tabs ul li').removeClass('active');
			$(this).addClass('active');
		});
	});
</script>