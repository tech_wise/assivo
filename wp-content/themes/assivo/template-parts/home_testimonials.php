<style>
		.assivo-help img.testimonial-image{
			width: 20% !important;
		}
		.assivo-help img.company-logo{
			width: 28% !important;
		}
		.bg-content-service.quote blockquote{
			min-height: 355px;
		}
		.testimonial-bottom {
			padding-bottom: 10px;
		}
		#h_testimonial_nav button{
			bottom: 3.5% !important;
		}
		#h_testimonial_nav .owl-next {
			right: 41%;
		}
		#h_testimonial_nav .owl-prev {
			left: 41%;
		}
		
		@media (max-width: 1024px){
			#h_testimonial_nav button{
				bottom: 3% !important;
			}
			.bg-content-service.quote blockquote {
				min-height: 470px;
			}
			#h_testimonial_nav .owl-next {
				right: 39%;
			}
			#h_testimonial_nav .owl-prev {
				left: 39%;
			}
		}
		@media (max-width: 768px){
			#h_testimonial_nav button{
				bottom: 50% !important;
			}
			#h_testimonial_nav .owl-prev {
				left: -5%;
			}
			#h_testimonial_nav .owl-next {
				right: -5%;
			}
			.bg-content-service.quote blockquote {
				min-height: 340px;
			}
			.testimonial-bottom {
				padding-bottom: 35px;
				width: 80%;
				margin: 0 auto;
			}
			.assivo-help img.testimonial-image {
				width: 17% !important;
			}
		}
		@media (max-width: 576px){
			.testimonial-bottom {
    			padding-bottom: 20px;
			}
		}
		@media (max-width: 575px){
			.testimonial-bottom {
				width: 100%;
			}
			.assivo-help img.testimonial-image{
				width: 23% !important;
			}
			#h_testimonial_nav .owl-prev {
				left: -4%;
			}
			#h_testimonial_nav .owl-next {
				right: -4%;
			}
		}
		@media (max-width: 430px){
			.owl-h-testimonial .nav-link{
				min-height: 440px !important;
			}
			.bg-content-service.quote blockquote {
				min-height: 330px;
			}
			.testimonial-bottom {
				padding-bottom: 0px;
			}
		}
		
	</style>

	<section class="use-case mt-2 assivo-help">
		<div class="container">
				<div class="row px-sm-0 px-2 mx-auto">
					<div class="col-md-10 pb-3 pb-0 explore_text lower-heading text-center mx-auto advantage-heading">
						<h3>CUSTOMER TESTIMONIALS</h3>
					</div>
				</div>
			<div class="explore_tabs row text-center mw-100 mx-auto position-relative">
				<div class="row pb-md-4 data-entry-tabs owl-h-testimonial owl-carousel owl-theme owl-nav-outer owl-dot-round mx-auto">
					<?php
					$ids_args = [
						'post_type' => 'post',
						'post_status' => 'publish',
						'category_name' => 'testimonials',
						'posts_per_page' => -1,
						'paged' => $paged,
						'order' => 'ASC',
						'fields' => 'ids',
					];
					$all_posts_ids = get_posts( $ids_args );
					$testimonial_args = array(
						'post_type' => 'post',
						'post_status' => 'publish',
						'category_name' => 'testimonials',
						'posts_per_page' => -1,
						'paged' => $paged,
						'order' => 'ASC',
					);
					$myposts = get_posts($testimonial_args); $h_limit = count($all_posts_ids); 
					$h_last_id = $all_posts_ids[$h_limit - 1];
						foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
							<div class="nav-link px-2" data-toggle="tab">
								<div class="bg-img-service">
									<div class="bg-content-service quote">
										<blockquote><?php the_content(); ?></blockquote>
										<div class="testimonial-bottom">																							<img src="<?php the_field('testimonial_image'); ?>" class="testimonial-image">
											<img src="<?php the_field('company_logo'); ?>" class="company-logo">
											<?php $designation = get_field('designation');?>
											<h4>
												<?php the_title(); ?>
												<br>
												<span class="designation"><?php echo $designation; ?></span>
											</h4>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				<?php wp_reset_postdata(); ?>
					<div id="h_testimonial_nav"  class="owl-nav customNav"></div>
			</div>
			<div class="row justify-content-center">
				<div class="col-lg-3 col-md-4 col-6">
					<div class="get-footer my-5">
						<a class="assivo-contact-us text-center d-none text-white border-0 font-weight-bold" href="#request_proposal">Request A Proposal</a>
					</div>
				</div>
			</div>
		</div>
	</section>