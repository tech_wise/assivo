<div class="row landing-page-boxes">
	<div class="col-md-6 landing-box">
		<div class="row">
			<div class="col-2 landing-box-img">
				<img src="<?php the_field('box_1_icon');?>">
			</div>
			<div class="col-10 landing-box-text">
				<h4><?php the_field('box_1_heading');?></h4>
				<?php the_field('box_1_text');?>
			</div>
		</div>
	</div>
	<div class="col-md-6 landing-box">
		<div class="row">
			<div class="col-2 landing-box-img">
				<img src="<?php the_field('box_2_icon');?>">
			</div>
			<div class="col-10 landing-box-text">
				<h4><?php the_field('box_2_heading');?></h4>
				<?php the_field('box_2_text');?>
			</div>
		</div>
	</div>
	<div class="col-md-6 landing-box">
		<div class="row">
			<div class="col-2 landing-box-img">
				<img src="<?php the_field('box_3_icon');?>">
			</div>
			<div class="col-10 landing-box-text">
				<h4><?php the_field('box_3_heading');?></h4>
				<?php the_field('box_3_text');?>
			</div>
		</div>
	</div>
	<div class="col-md-6 landing-box">
		<div class="row">
			<div class="col-2 landing-box-img">
				<img src="<?php the_field('box_4_icon');?>">
			</div>
			<div class="col-10 landing-box-text">
				<h4><?php the_field('box_4_heading');?></h4>
				<?php the_field('box_4_text');?>
			</div>
		</div>
	</div>
	<div class="col-md-6 landing-box">
		<div class="row">
			<div class="col-2 landing-box-img">
				<img src="<?php the_field('box_5_icon');?>">
			</div>
			<div class="col-10 landing-box-text">
				<h4><?php the_field('box_5_heading');?></h4>
				<?php the_field('box_5_text');?>
			</div>
		</div>
	</div>
	<div class="col-md-6 landing-box">
		<div class="row">
			<div class="col-2 landing-box-img">
				<img src="<?php the_field('box_6_icon');?>">
			</div>
			<div class="col-10 landing-box-text">
				<h4><?php the_field('box_6_heading');?></h4>
				<?php the_field('box_6_text');?>
			</div>
		</div>
	</div>
</div>