<?php
/**
 * Template Name: Landing Mortgage Outsourcing Page
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */

get_header('landing'); 
   	 global $post;
    	$post_slug=$post->post_name;
	$file_name = explode('-', $post_slug, 2);
	$service_type = $file_name[1];
	$template_fname= "template-parts/".$service_type;	
?>
<br><br>


	<section class="getstarted pb-2">
		<div class="landing-banner-text desktop_fade_slider">
			<div class="container">
				<div class="row landing-service">
					<div class="col-lg-7 col-md-8 under_line my-2" id="request_proposal">
						<?php $sub_head = get_field('sub_heading'); 
						echo $sub_head; ?>
					</div>
					<div class="col-lg-5 col-md-4 it-case-study-large after_banner_img text-center px-0">
						<?php while ( have_posts() ) : the_post(); ?>
						<?php $after_banner_image =  get_field('after_banner_image'); ?>
						<?php endwhile; ?>
						<img src="<?php echo $after_banner_image; ?>">
					</div>
				</div>
			</div>
		</div>
		<div class="landing-after-banner">
				<div class="container scroller">
					<div class="row landing-row mt-lg-3 px-sm-4 px-2">
						<span style="display: none" id="h-form-subject"><?php echo get_field('form_email_subject');?></span>
						<div class="col-lg-12 flat-box get-started-box px-0 mt-lg-0 mt-3">
							<div class="form-column pb-0 pt-xl-2 pt-3 px-xl-4 px-md-3 px-2">
								<div class="get-started-form-box landing py-2">
                                <?php 
                        			if(isset($_GET['action']) && $_GET['action'] == 'track'){?>

                            		<p>Thank you for submitting your information. One of our representatives will be in touch with you within 48 hours to discuss your needs further</p>

                        			<?php }elseif(is_page(4713)){ ?>
                        
                            		<h6 class="mb-3 py-3 under_line"><?php the_field('form_heading'); ?></h6>
									<?php echo do_shortcode( '[contact-form-7 id="4716" title="Mortgage outsourcing Top Form"]'); ?>
                        			<?php } elseif(is_page(4803)){ ?>
                        
                            		<h6 class="mb-3 py-3 under_line"><?php the_field('form_heading'); ?></h6>
									<?php echo do_shortcode( '[contact-form-7 id="4799" title="Mortgage outsourcing C Top Form"]'); ?>
                        			<?php } ?>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

<!-- What We Do Section Start -->
	<section class="what-we-do">
		<div class="container">
			<div class="row justify-content-center top-row">
				<div class="col-md-7 text">
					<?php the_field('what_we_do_text'); ?>
				</div>
				<div class="col-md-5 text-center image">
					<img src="<?php the_field('what_we_do_img'); ?>">
				</div>
			</div>
			<div class="row bottom-row justify-content-between">
				<div class="col-md-12 text-center">
					<h2><?php the_field('pie_chart_section_heading'); ?></h2>
				</div>
				<div class="col-lg-6 chart">
					<h4><?php the_field('pie_chart_label'); ?></h4>
					<img src="https://assivo.com/dev/wp-content/uploads/2021/02/pie-chart.png">
					<div class="values value-1">
						<img src="https://assivo.com/dev/wp-content/uploads/2021/02/line-1.png">
						<?php the_field('pie_chart_onshore_team'); ?>
					</div>
					<div class="values value-2">
						<img src="https://assivo.com/dev/wp-content/uploads/2021/02/line-2.png">
						<?php the_field('pie_chart_offshore_team'); ?>
					</div>
				</div>
				<div class="col-lg-5 results">
					<div class="row">
						<div class="col-lg-12 col-sm-6 result result-1 text-center">
							<?php the_field('pie_chart_result_1'); ?>
						</div>
						<div class="col-lg-12 col-sm-6 result result-2 text-center">
							<?php the_field('pie_chart_result_2'); ?>
						</div>
					</div>
				</div>
				<div class="arrow-img">
					<img src="https://assivo.com/dev/wp-content/uploads/2021/02/arrow.png">	
				</div>
			</div>
		</div>
	</section>
<!-- What We Do Section End -->

<!-- Benefits of Global Operations Section Start -->
	<section class="benefits">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-12 global-benefits text-center">
					<h2><?php the_field('benefit_heading'); ?></h2>
				</div>
				<div class="box col-lg-4">
					<?php the_field('benefit_1'); ?>
				</div>
				<div class="box col-lg-4">
					<?php the_field('benefit_2'); ?>
				</div>
				<div class="box col-lg-4">
					<?php the_field('benefit_3'); ?>
				</div>
				<div class="box col-lg-4">
					<?php the_field('benefit_4'); ?>
				</div>
				<div class="box col-lg-4">
					<?php the_field('benefit_5'); ?>
				</div>
			</div>
		</div>
	</section>
<!-- Benefits of Global Operations Section End -->


<!-- Assivo Solutions Section Start -->
	<section class="assivo-solutions">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center problems">
					<?php the_field('problems'); ?>
				</div>
				<div class="col-md-12 text-center arrow-img">
					<img src="https://assivo.com/dev/wp-content/uploads/2021/02/arrow.png">	
				</div>
			</div>
			<div class="row outcomes">
				<div class="col-md-12 text-center solutions">
					<?php the_field('assivo_outcomes_heading'); ?>
					<ul class="equation">
						<li>
							<?php the_field('outcome_1'); ?>
						</li>
						<div class="plus"><img src="https://assivo.com/dev/wp-content/uploads/2020/12/plus.png"></div>
						<li>
							<?php the_field('outcome_2'); ?>
						</li>
						<div class="plus"><img src="https://assivo.com/dev/wp-content/uploads/2020/12/plus.png"></div>
						<li>
							<?php the_field('outcome_3'); ?>
						</li>
						<div class="plus"><img src="https://assivo.com/dev/wp-content/uploads/2020/12/equal.png"></div>
						<li>
							<?php the_field('outcome_4'); ?>
						</li>
					</ul>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-lg-3 col-md-4 col-6">
					<div class="get-footer my-5 mortgage">
						<a class="assivo-contact-us text-center text-white border-0 font-weight-bold" href="#request_proposal">Request A Proposal</a>
					</div>
				</div>
			</div>
		</div>
	</section>
<!-- Assivo Solutions Section End -->

<!-- 	Labor Stack Section Start -->
	<section class="labor-stack">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center heading">
					<?php the_field('top_heading'); ?>
				</div>
				<div class="col-md-6 text-center team onshore">
					<?php the_field('onshore_team'); ?>
				</div>
				<div class="col-md-6 text-center team offshore">
					<?php the_field('offshore_team'); ?>
				</div>
			</div>
		</div>
	</section>
<!-- 	Labor Stack Section End -->

<!-- Assivo Services Section Start -->
	<section class="assivo-services">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6 heading">
					<?php the_field('services_heading'); ?>
				</div>
				<div class="col-md-6 text-center">
					<img src="<?php the_field('services_image'); ?> ">
				</div>
				<div class="col-lg-3 col-sm-6 services">
					<?php the_field('services_1'); ?>
				</div>
				<div class="col-lg-3 col-sm-6 services">
					<?php the_field('services_2'); ?>
				</div>
				<div class="col-lg-3 col-sm-6 services">
					<?php the_field('services_3'); ?>
				</div>
				<div class="col-lg-3 col-sm-6 services">
					<?php the_field('services_4'); ?>
				</div>
				<div class="col-lg-3 col-sm-4 services">
					<?php the_field('services_5'); ?>
				</div>
				<div class="col-lg-3 col-sm-4 services">
					<?php the_field('services_6'); ?>
				</div>
				<div class="col-lg-3 col-sm-4 services">
					<?php the_field('services_7'); ?>
				</div>
			</div>
		</div>
	</section>
<!-- Assivo Services Section End -->

<!-- Collapse Section Start -->
	<section class="services-collapse">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-12">
					<div class="row justify-content-center">
						<div class="col-xl-8 col-lg-10 text-center">
							<ul class="nav nav-tabs text-center" id="services-tabs">
								<li class="active">
									<a data-toggle="tab" href="#service-1" class="tab">
										<img src="<?php the_field('service_tab_1_icon_white'); ?>" class="white">
										<img src="<?php the_field('service_tab_1_icon_blue'); ?>" class="blue">
										<p><?php the_field('collapse_1_heading'); ?></p>
									</a>
								</li>
								<li>
									<a data-toggle="tab" href="#service-5" class="tab">
										<img src="<?php the_field('service_tab_5_icon_white'); ?>" class="white">
										<img src="<?php the_field('service_tab_5_icon_blue'); ?>" class="blue">
										<p><?php the_field('collapse_5_heading'); ?></p>
									</a>
								</li>
								<li>
									<a data-toggle="tab" href="#service-3" class="tab">
										<img src="<?php the_field('service_tab_3_icon_white'); ?>" class="white">
										<img src="<?php the_field('service_tab_3_icon_blue'); ?>" class="blue">
										<p><?php the_field('collapse_3_heading'); ?></p>
									</a>
								</li>
								<li>
									<a data-toggle="tab" href="#service-4" class="tab">
										<img src="<?php the_field('service_tab_4_icon_white'); ?>" class="white">
										<img src="<?php the_field('service_tab_4_icon_blue'); ?>" class="blue">
										<p><?php the_field('collapse_4_heading'); ?></p>
									</a>
								</li>
								<li>
									<a data-toggle="tab" href="#service-2" class="tab">
										<img src="<?php the_field('service_tab_2_icon_white'); ?>" class="white">
										<img src="<?php the_field('service_tab_2_icon_blue'); ?>" class="blue">
										<p><?php the_field('collapse_2_heading'); ?></p>
									</a>
								</li>
								
								<li>
									<a data-toggle="tab" href="#service-6" class="tab">
										<img src="<?php the_field('service_tab_6_icon_white'); ?>" class="white">
										<img src="<?php the_field('service_tab_6_icon_blue'); ?>" class="blue">
										<p><?php the_field('collapse_6_heading'); ?></p>
									</a>
								</li>
							</ul>
						</div>
					</div>

					<div class="tab-content" id="services-tabs-content">
						<div id="service-1" class="tab-pane fade show active">
							<div class="row justify-content-center">
								<div class="col-lg-11 heading">
									<h4><?php the_field('collapse_1_heading'); ?></h4>
									<div class="row">
										<div class="col-md-6 left-side">
											<?php the_field('collapse_1_left_side'); ?>
										</div>
										<div class="col-md-6 right-side">
											<?php the_field('collapse_1_right_side'); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="service-2" class="tab-pane fade">
							<div class="row justify-content-center">
								<div class="col-lg-11 heading">
									<h4><?php the_field('collapse_2_heading'); ?></h4>
									<div class="row">
										<div class="col-lg-4 col-sm-6">
											<div class="loan-pro-box">
												<?php the_field('collapse_2_list_1'); ?>
											</div>
										</div>
										<div class="col-lg-4 col-sm-6">
											<div class="loan-pro-box">
												<?php the_field('collapse_2_list_2'); ?>
											</div>
										</div>
										<div class="col-lg-4 col-sm-6">
											<div class="loan-pro-box">
												<?php the_field('collapse_2_list_3'); ?>
											</div>
										</div>
										<div class="col-lg-4 col-sm-6">
											<div class="loan-pro-box">
												<?php the_field('collapse_2_list_4'); ?>
											</div>
										</div>
										<div class="col-lg-4 col-sm-6">
											<div class="loan-pro-box">
												<?php the_field('collapse_2_list_5'); ?>
											</div>
										</div>
										<div class="col-lg-4 col-sm-6">
											<div class="loan-pro-box">
												<?php the_field('collapse_2_list_6'); ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="service-3" class="tab-pane fade">
							<div class="row justify-content-center">
								<div class="col-lg-11 heading">
									<h4><?php the_field('collapse_3_heading'); ?></h4>
									<div class="row">
										<div class="col-md-4 header top box-1">
											<img src="https://assivo.com/dev/wp-content/uploads/2020/12/loan-1-1.png">
											<h5>Quality</h5>
										</div>
										<div class="col-md-4 header top box-2">
											<img src="https://assivo.com/dev/wp-content/uploads/2020/12/loan-1-2.png">
											<h5>Efficiency</h5>
										</div>
										<div class="col-md-4 header top box-3">
											<img src="https://assivo.com/dev/wp-content/uploads/2020/12/loan-1-3.png">
											<h5>Accountability</h5>
										</div>
										<div class="col-lg-3 col-sm-6 underwrite-box">
											<?php the_field('collapse_3_list_1'); ?>
										</div>
										<div class="col-lg-3 col-sm-6 underwrite-box">
											<?php the_field('collapse_3_list_2'); ?>
										</div>
										<div class="col-lg-3 col-sm-6 underwrite-box">
											<?php the_field('collapse_3_list_3'); ?>
										</div>
										<div class="col-lg-3 col-sm-6 underwrite-box">
											<?php the_field('collapse_3_list_4'); ?>
										</div>
										<div class="col-lg-3 col-sm-6 underwrite-box">
											<?php the_field('collapse_3_list_5'); ?>
										</div>
										<div class="col-lg-3 col-sm-6 underwrite-box">
											<?php the_field('collapse_3_list_6'); ?>
										</div>
										<div class="col-lg-3 col-sm-6 underwrite-box">
											<?php the_field('collapse_3_list_7'); ?>
										</div>
										<div class="col-lg-3 col-sm-6 underwrite-box">
											<?php the_field('collapse_3_list_8'); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="service-4" class="tab-pane fade">
							<div class="row justify-content-center">
								<div class="col-lg-11 heading">
									<h4><?php the_field('collapse_4_heading'); ?></h4>
									<div class="row">
										<div class="col-md-12">
											<img src="<?php the_field('collapse_4_image'); ?> ">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="service-5" class="tab-pane fade">
							<div class="row justify-content-center">
								<div class="col-lg-11 heading">
									<h4><?php the_field('collapse_5_heading'); ?></h4>
									<div class="row">
										<div class="col-md-12">
											<img src="<?php the_field('collapse_5_image'); ?> ">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="service-6" class="tab-pane fade">
							<div class="row justify-content-center">
								<div class="col-lg-11 heading">
									<h4><?php the_field('collapse_6_heading'); ?></h4>
									<div class="row">
										<div class="col-md-4 others-box text-center">
											<?php the_field('collapse_6_box_1'); ?>
										</div>
										<div class="col-md-4 others-box text-center">
											<?php the_field('collapse_6_box_2'); ?>
										</div>
										<div class="col-md-4 others-box text-center">
											<?php the_field('collapse_6_box_3'); ?>
										</div>
										<div class="col-md-4 others-box text-center">
											<?php the_field('collapse_6_box_4'); ?>
										</div>
										<div class="col-md-4 others-box text-center">
											<?php the_field('collapse_6_box_5'); ?>
										</div>
										<div class="col-md-4 others-box text-center">
											<?php the_field('collapse_6_box_6'); ?>
										</div>
										<div class="col-md-4 others-box text-center">
											<?php the_field('collapse_6_box_7'); ?>
										</div>
										<div class="col-md-4 others-box text-center">
											<?php the_field('collapse_6_box_8'); ?>
										</div>
										<div class="col-md-4 others-box text-center">
											<?php the_field('collapse_6_box_9'); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-lg-3 col-md-4 col-6">
					<div class="get-footer my-5 mortgage">
						<a class="assivo-contact-us text-center text-white border-0 font-weight-bold" href="#request_proposal">Request A Proposal</a>
					</div>
				</div>
			</div>
		</div>
	</section>
<!-- Collapse Section End -->

<!-- Logos Section Start -->
	<section class="mortgage-logos">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-9 text-center">
					<?php the_field('logos_heading'); ?>
				</div>
				<div class="col-md-12 list-logos text-center">
					<?php the_field('logo_images'); ?>
				</div>
			</div>
		</div>
	</section>
<!-- Logos Section End -->

<!-- Optimized Loan Section Start -->
	<section class="optimized-loan">
		<div class="process-arrow">
			<img src="https://assivo.com/dev/wp-content/uploads/2021/01/process-arrow.png">
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-9 heading">
					<?php the_field('loan_heading'); ?>
				</div>
				<div class="col-md-3 btn-sign">
					<span class="assivo"><img src="https://assivo.com/dev/wp-content/themes/assivo/images/logo.svg"></span>
					<span class="client">Client</span>
				</div>
				<div class="col-md-4 loan-process client">
					<p>Client</p>
					<div class="client-assivo-box">
						<div class="process-num">1</div>
						<?php the_field('process_1'); ?>
					</div>
				</div>
				<div class="col-md-4 loan-process assivo">
					<p><img src="https://assivo.com/dev/wp-content/uploads/2020/11/blue-logo-assivo.svg"> assivo</p>
					<div class="client-assivo-box">
						<div class="process-num">2</div>
						<?php the_field('process_2'); ?>
					</div>
				</div>
				<div class="col-md-4 loan-process client">
					<p>Client</p>
					<div class="client-assivo-box">
						<div class="process-num">3</div>
						<?php the_field('process_3'); ?>
					</div>
				</div>
				<div class="col-md-4 loan-process assivo">
					<p><img src="https://assivo.com/dev/wp-content/uploads/2020/11/blue-logo-assivo.svg"> assivo</p>
					<div class="client-assivo-box">
						<div class="process-num">4</div>
						<?php the_field('process_4'); ?>
					</div>
				</div>
				<div class="col-md-4 loan-process client">
					<p>Client</p>
					<div class="client-assivo-box">
						<div class="process-num">5</div>
						<?php the_field('process_5'); ?>
					</div>
				</div>
				<div class="col-md-4 loan-process assivo">
					<p><img src="https://assivo.com/dev/wp-content/uploads/2020/11/blue-logo-assivo.svg"> assivo</p>
					<div class="client-assivo-box">
						<div class="process-num">6</div>
						<?php the_field('process_6'); ?>
					</div>
				</div>
				<div class="col-md-4 loan-process assivo">
					<p><img src="https://assivo.com/dev/wp-content/uploads/2020/11/blue-logo-assivo.svg"> assivo</p>
					<div class="client-assivo-box">
						<div class="process-num">7</div>
						<?php the_field('process_7'); ?>
					</div>
				</div>
				<div class="col-md-4 loan-process client">
					<p>Client</p>
					<div class="client-assivo-box">
						<div class="process-num">8</div>
						<?php the_field('process_8'); ?>
					</div>
				</div>
				<div class="col-md-4 loan-process assivo">
					<p><img src="https://assivo.com/dev/wp-content/uploads/2020/11/blue-logo-assivo.svg"> assivo</p>
					<div class="client-assivo-box">
						<div class="process-num">9</div>
						<?php the_field('process_9'); ?>
					</div>
				</div>
				<div class="col-md-4 loan-process client">
					<p>Client</p>
					<div class="client-assivo-box">
						<div class="process-num">10</div>
						<?php the_field('process_10'); ?>
					</div>
				</div>
				<div class="col-md-4 loan-process assivo">
					<p><img src="https://assivo.com/dev/wp-content/uploads/2020/11/blue-logo-assivo.svg"> assivo</p>
					<div class="client-assivo-box">
						<div class="process-num">11</div>
						<?php the_field('process_11'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<!-- Optimized Loan Section Start -->

	<section class="form-section py-5" id="request_proposal">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 text">
					<h2><?php the_field('form_text_heading'); ?></h2>
					<?php the_field('form_text'); ?>
				</div>
				<span style="display: none" id="h-form-subject"><?php echo get_field('form_email_subject');?></span>
				<div class="col-lg-6">
					<?php echo do_shortcode('[contact-form-7 id="4406" title="Landing D Page Form"]'); ?>
				</div>
			</div>
		</div>
	</section>

	<?php
		if(isset($_GET['action']) && $_GET['action'] == 'track-bottom'){ ?>
		<div class="container">
			<div class="row justify-content-center">
				<div id="thank-you" class="col-md-10 text-center">
					<p>Thank you for submitting your information. One of our representatives will be in touch with you within 48 hours to discuss your needs further</p>
				</div>
			</div>
		</div>
     <?php }
		else {get_template_part( 'template-parts/request_consultation', 'none' ); } ?>
    
<!-- Rediction on Conusltation page after top form submtting START -->
<input type="hidden" id="consultation_page" value="<?php the_permalink(3955); ?>" />
<input type="hidden" id="reload_page" value="<?php the_permalink(4713); ?>?action=track" />

<!--  For Bottom Form-->
<input type="hidden" id="reload_page_bottom" value="<?php the_permalink(4713); ?>?action=track-bottom#thank-you" />


<script>
// 	Top Form

document.addEventListener( 'wpcf7mailsent', function( event ) {
var loan = document.getElementById("lsize").value;
var consultation = document.getElementById("consultation_page").value;
var reload = document.getElementById("reload_page").value;
  //alert(loan + consultation);

if((loan == "Sole Proprietor" ) || (loan == "Confidential or N/A" )){
	location = reload;	
}else {
	location = consultation;
}

}, false );
// Rediction on Conusltation page after top form submtting End

	
// 	Bottom Form
document.addEventListener( 'wpcf7mailsent', function( event ) {
var loan_bottom = document.getElementById("lsize-bottom").value;
var consultation_bottom = document.getElementById("consultation_page").value;
var reload_bottom = document.getElementById("reload_page_bottom").value;
//  alert(loan_bottom + consultation_bottom);

if((loan_bottom == "Sole Proprietor" )|| (loan_bottom == "Confidential or N/A" )){
	location = reload_bottom;
}else {
	location = consultation_bottom;
}

}, false );
// Rediction on Conusltation page after top form submtting End
	
	
jQuery(document).ready(function($){
	
  $("#wpcf7-f3947-o1 select[name=csize] option:first").text("Choose Company Size");
<?php 
	if(isset($_GET['calendy-popup'])){?>
		$("a#consultation-popup").click();
	<?php }
	?>
	
});	

</script>



<?php	get_footer();