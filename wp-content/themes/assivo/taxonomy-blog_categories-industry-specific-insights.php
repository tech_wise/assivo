<?php
/**
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */

get_header(); ?>

	<style>
		.nav-tabs-inner{
			padding: 0;
			margin-left: -9px;
		}
		#h_blog_slider_arrows button{
			top: 410px;
		}

	</style>



	<section class="blog-slider">
		
		<?php get_template_part( 'template-parts/blog_nav', 'none' );?>
		

	</section>



	<section class="under-construction my-3">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8 explore_text text-center">
					<h3>
						under construction
					</h3>
					<p>
						We are currently authoring Industry-Specific Insights, and they will be ready shortly. Thank you for your patience!
					</p>
				</div>
				<div class="col-md-12 text-center">
					
					<img src="https://assivo.com/dev/wp-content/uploads/2020/07/under_construction.jpg">
				</div>
			</div>
		</div>
	</section>


    	<?php get_template_part( 'template-parts/request_consultation', 'none' );?>

<?php get_footer();