<?php
/**
 * Template Name: Second Landing Page
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */

get_header('landing'); 
   	 global $post;
    	$post_slug=$post->post_name;
	$file_name = explode('-', $post_slug, 2);
	$service_type = $file_name[1];
	$template_fname= "template-parts/".$service_type;	
?>
<br><br>
		<section class="getstarted pb-2">
			<div class="banner-img">
				<?php the_post_thumbnail('full');?>
			</div>
			<div class="landing-banner-text">
				<div class="container-fluid">
					<div class="row landing-row mt-lg-3 px-sm-4 px-2">
						<div class="col-lg-9 landing-service pl-lg-0">
							<div class="row">
								<div class="col-md-12 col-12 landing-top-text">
									<div class="row">
										<div class="col-md-8 under_line text-center my-2 pr-md-4">
											<?php $sub_head = get_field('sub_heading');
											echo $sub_head; ?>
										</div>
										<div class="col-md-4 it-case-study-large after_banner_img text-center px-0">
											<?php while ( have_posts() ) : the_post(); ?>
											<?php $after_banner_image =  get_field('after_banner_image'); ?>
											<?php endwhile; ?>
											<img src="<?php echo $after_banner_image; ?>">
										</div>
									</div>
									
									<div class="row px-lg-3 px-0">
										<div class="col-sm-12 it-case-study-large px-0">
											<div class="landing-service px-1">
												<h3> <?php the_field('after_banner_text_heading'); ?>
													<span class="blue-logo d-none"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/blue-logo-assivo.svg"> assivo</span>
												</h3>
												<?php the_field('after_banner_text'); ?>
											</div>
										</div>
										
										
										<?php get_template_part( 'template-parts/landing-page-boxes', 'none' );?>
								</div>
							</div>
						</div>
					</div>
					<span style="display: none" id="h-form-subject"><?php echo get_field('form_email_subject');?></span>
					<div class="col-lg-3 flat-box get-started-box px-0 mt-lg-0 mt-3" id="request_proposal">
						<div class="form-column pb-0 pt-xl-2 pt-3 px-xl-4 px-md-3 px-2">
							<div class="get-started-form-box landing">
								<h6 class="mb-3 under_line"><?php the_field('form_heading'); ?></h6>
								<?php echo do_shortcode( '[contact-form-7 id="1219" title="Get Started Form"]'); ?>
								<div class="row text-left form-footer">
									<div class="col-2 form-footer-img">
										<img src="https://assivo.com/dev/wp-content/uploads/2020/06/icon-01-7.svg">
									</div>
									<div class="col-10 form-footer-text">
										<p>444 W. Lake St. Suite 1700 Chicago, IL 60606</p>
									</div>
								</div>
								<div class="row text-left">
									<div class="col-2 form-footer-img">
										<img src="https://assivo.com/dev/wp-content/uploads/2020/06/icon-01-8.svg">
									</div>
									<div class="col-10 form-footer-text">
										<a href="mailto:hello@assivo.com">hello@assivo.com</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/5-step-process', 'none' );?>
	
	<?php get_template_part( 'template-parts/testimonials', 'none' );?>

	<section class="text-center explore my-1 pb-md-3 bg-colour">
		<?php get_template_part( 'template-parts/use_cases', 'none' );?>
	</section>
	
	<section class="text-center explore my-1 pt-md-3 ">
		<?php get_template_part( 'template-parts/case_studies', 'none' );?>
	</section>
	
	<section class="mt-5 mb-3 pb-2"  id="our_advantage">
		<div class="container">
			<div class="row pt-md-3 px-sm-0 px-2 mx-auto">
				<div class="col-md-8 pb-md-3 pb-0 explore_text text-center mx-auto advantage-heading">
					<h3>Our Advantage</h3>
				</div>
			</div>
			<br>
			<?php get_template_part( 'template-parts/our_advantage', 'none' );?>
		</div>
	</section>
      
	<?php get_template_part( 'template-parts/faqs_landing', 'none');?>

	<section>
		<?php get_template_part( 'template-parts/landing-assivo-insights');?>
	</section>

<script>

jQuery(document).ready(function($){
	
//   if(! $('#h_casestudies').hasClass('active')){
// 	  $('#data-entry-processing_1').addClass("active");
//   }
	
  $("#wpcf7-f1219-o1 select[name=csize] option:first").text("Choose Company Size");
	
});	

</script>



<?php	get_footer();