<?php
/**
 * Template Name: Get Started Now
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */

get_header('get'); ?>

	<section class="getstarted pb-2 request-bg-img">
			<div class="container">
				<div class="row d-flex flex-row-reverse mt-lg-3 mt-md-0 mt-3 px-4">
					<div class="col-lg-5 flat-box get-started-box px-0">
						<div class="form-column pb-0 pt-xl-4 pt-3 px-xl-4 px-md-3 px-2">
							<div class="get-started-form-box">
								<?php //echo do_shortcode( '[contact-form-7 id="1219" title="Get Started Form"]'); ?>
								<?php echo do_shortcode( '[contact-form-7 id="4830" title="Landing B Top form"]'); ?>
							</div>
						</div>
					</div>
					<span style="display: none" id="h-form-subject"><?php echo get_field('form_email_subject');?></span>
					<div class="col-lg-7 get-started-contect pt-md-5">
						<div class="row mt-lg-3">
							<div class="col-12 mt-4 pt-lg-3">
							<h3><?php echo get_field('heading_check_marks');?></h3>
								<?php
									while ( have_posts() ) : the_post();
						
										the_content();
									endwhile; 
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
	</section>

	<!-- Rediction on Conusltation page after top form submtting START -->
	<input type="hidden" id="consultation_page" value="<?php the_permalink(3955); ?>" />
	<input type="hidden" id="next_step_page" value="<?php the_permalink(116); ?>" />


	<script>
	document.addEventListener( 'wpcf7mailsent', function( event ) {
	var checkedValue = document.querySelector('.check-boxes .active input:checked').value;
	var consultation = document.getElementById("consultation_page").value;
	var next_step = document.getElementById("next_step_page").value;
// 	 alert(checkBox + consultation);

	if(checkedValue != "A short-term project (4-8 weeks)" ){
		location = consultation;
	}else {
		location = next_step;
	}

	}, false );
	// Rediction on Conusltation page after top form submtting End
	
</script>
	
<?php	get_footer();