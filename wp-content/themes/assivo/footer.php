<?php
/**
 * The template for displaying the footer
 *
 * @package TW_Assivo
 * @since TW_Assivo 1.0
 */
 
if (!is_page_template( 'get-started.php')){ ?> 

	<footer>
		<div class="container mt-5">
			<div class="row mb-2 mx-auto">
				<?php if(is_page_template( 'landing-c.php' ) || is_page_template( 'landing-mortgage-outsourcing.php' )) { ?>
				<div class="col-lg-4 col-sm-6 footer-section d-flex pb-lg-1 pb-sm-4 px-md-0 px-2">
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-blue.svg" class="footer-logo">
				</div>
				<div class="col-lg-4 col-sm-6 col-12 pt-lg-4 pt-3 pl-lg-5 pb-sm-5 pb-lg-1  px-2 contact-info">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Content") ) : ?>
					<?php endif;?>
				</div>
				<div class="col-lg-4 contact-us-footer col-sm-12 col-12 pt-lg-4 pt-3 pl-xl-4 px-md-0 px-sm-0 px-2 py-3">
					<div class="get-footer">
						<a class="assivo-contact-us text-center text-white border-0 font-weight-bold" href="#request_proposal">Request A Proposal</a>
					</div>
					<div class="get-footer second-btn">
						<link href="https://assets.calendly.com/assets/external/widget.css" rel="stylesheet">
<script src="https://assets.calendly.com/assets/external/widget.js" type="text/javascript"></script>
						<a class="assivo-contact-us text-center text-white border-0 font-weight-bold blue-btn" href="" onclick="Calendly.initPopupWidget({url: 'https://calendly.com/assivo'});return false;" id="consultation-popup">Schedule A Consultation</a>
					</div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/soc2.png" class="soc-logo">
				</div>
				<?php }else { ?>
				<div class="col-lg-3 col-sm-7 footer-section pb-lg-1 pb-4 px-md-0 px-2">
						<a href="<?php echo  esc_url( home_url( '/' ) ); ?>" class="footer-logo"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-blue.svg"></a>					
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Content") ) : ?>
					<?php endif;?>
				</div>
				
				<div class="col-lg-2 col-sm-5 col-5 pt-lg-4 pt-3 pl-lg-5 pb-sm-5 pb-lg-1  px-2">
					<?php wp_nav_menu( array( 'theme_location' => 'secondary' ) ); ?>
				</div>
				<div class="col-lg-3 col-sm-7 col-7 pt-lg-4 pt-3 pl-xl-4 px-md-0 pb-lg-1 px-2">
					<?php wp_nav_menu( array( 'theme_location' => 'services_Menu' ) ); ?>
				</div>
				
				<div class="col-lg-4 contact-us-footer col-sm-5 col-12 pt-lg-4 pt-3 pl-xl-4 px-md-0 px-sm-0 px-2 py-3">
					
					<div class="get-footer">
						<?php
							if(is_page_template( 'landing.php' ) || is_page_template( 'landing-3.php' ) || is_page_template( 'landing-4.php' ) || is_page_template( 'landing-a.php' ) || is_page_template( 'landing-b.php' ) || is_page_template( 'landing-c.php' ) || is_page_template( 'landing-d.php' )){?>
					<a class="assivo-contact-us text-center text-white border-0 font-weight-bold" href="#request_proposal">Request A Proposal</a>
					
					<?php }else if( is_page('1322')){?>
						<a class="assivo-contact-us text-center text-white border-0 font-weight-bold" href="<?php echo get_permalink('334');?>">Request A Proposal</a>
					
						<div class="row mt-3">
							<div class="col-sm-6 col-6 certification-col text-center">
								<img src="<?php the_field('certification_icon_1'); ?>">
							</div>
							<div class="col-sm-6 col-6 certification-col text-center">
								<img src="<?php the_field('certification_icon_2'); ?>">
							</div>
							<div class="col-sm-6 col-6 certification-col text-center">
								<img src="<?php the_field('certification_icon_3'); ?>">
							</div>
							<div class="col-sm-6 col-6 certification-col text-center">
								<img src="<?php the_field('certification_icon_4'); ?>">
							</div>
						</div>
						
					<?php }else{?>
						<a class="assivo-contact-us text-center text-white border-0 font-weight-bold" href="<?php echo get_permalink('334');?>">Request A Proposal</a>
					
					<?php }	?>
<!-- 						<a class="assivo-contact-us text-center text-white border-0 font-weight-bold" href="<?php echo get_permalink('334');?>">Request A Proposal</a> -->
					</div>
					<div class="get-footer second-btn">
						
						<a class="assivo-contact-us text-center text-white border-0 font-weight-bold blue-btn" href="<?php the_permalink('3955'); ?>" id="consultation-popup">Schedule A Consultation</a>

<!-- 						<link href="https://assets.calendly.com/assets/external/widget.css" rel="stylesheet">
<script src="https://assets.calendly.com/assets/external/widget.js" type="text/javascript"></script>
						<a class="assivo-contact-us text-center text-white border-0 font-weight-bold blue-btn" href="" onclick="Calendly.initPopupWidget({url: 'https://calendly.com/assivo'});return false;" id="consultation-popup">Schedule A Consultation</a> -->
					</div>
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/soc2.png" class="soc-logo">
				</div>
				<?php } ?>
				
			</div>
			<div class="row copyrights pt-2">
				<div class="col-sm-3 col-12 copyrights-date px-0">
					<p>&copy; <?php echo date("Y"); ?> Assivo, Inc. </p>
				</div>
				<div class="col-sm-9 col-12 p-0 m-0 pr-xl-1 copyrights-terms">
					<p>All Rights Reserved &nbsp;&nbsp;|&nbsp;&nbsp;
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>information-and-data-security">Information &amp; Data Security</a>
					<?php if(!is_page_template( 'landing-c.php')) { ?> &nbsp;&nbsp; |&nbsp;&nbsp; <?php } ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>privacy-policy">Privacy Policy </a> 
					<?php if(!is_page_template( 'landing-c.php' )) { ?> &nbsp;&nbsp; |&nbsp;&nbsp; <?php } ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>terms-conditions">Terms &amp; Conditions</a>
					
					</p>
				</div>
			</div>
		</div>
	</footer>
<?php } 
if (is_page_template( 'get-started.php')){ ?> 
	<script>
		var url_string = window.location.href;
		var url = new URL(url_string);
		var email = url.searchParams.get("email");
		document.getElementById('email_custom').value = email;
	</script>
<?php }
else { ?>
	<script>
		var url_string = window.location.href;
		var url = new URL(url_string);
		var email = url.searchParams.get("email");
		document.getElementById('email').value = email;
		if (typeof email!= 'undefined' && email) {
			document.getElementById('request_proposal').scrollIntoView(true);
		}
	</script>
<?php } ?>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.min.js"></script>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/bootstrap.min.js"></script>

<?php
// 	Make URL active/deactive	Nayyer									   
	if(is_page_template( 'landing-c.php' )) { ?>
	<script>
		$('.uc-temp-part a.case-box').each(function(){
			$(this).removeAttr("href");
		});
		$('header .header-logo').each(function(){
			$(this).removeAttr("href");
		});
	</script>
	<?php } ?>

	<script>
		var base_url='<?php echo esc_url( get_template_directory_uri() )?>';
	</script>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/smooth_scroll.js"></script>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/custom.js"></script>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/slider_owl.js"></script>
<!-- 	<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js"></script> -->
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/owlcarousel/owl.carousel.min.js"></script>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/dot-nav.js"></script>
	<script type="text/javascript"> _linkedin_partner_id = "2389290"; window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=2389290&fmt=gif" /> </noscript>
	<?php wp_footer(); ?>
</body>
</html>
